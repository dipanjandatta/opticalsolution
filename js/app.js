function paymentcheck(val){
    console.log(val.checked);
    if(val.checked == true){
        $('#prty_name_shw').show();
    }
    else{
        $('#prty_name_shw').hide();

    }
}


function returnval() {

    var item_id= $("#item_ids_get").val();
    var prty_name= $("#hid_prty_name").val();
    var tot_amnt= $("#tot_amount").val();
    var amount = $("#amount").val();
    var serv_tax = $("#serv_tax").val();
    var due_amount = $("#due_amount").val();
    var payed = $("#paid_amount").val();
    var tot_due_amnt = $("#tot_due_amnt").val();

    console.log(tot_due_amnt);

     $("#btnprint").attr('disabled' , false);

    $.ajax({
        type:'POST',
        data:{item_id:item_id,prty_name:prty_name,tot_amnt:tot_amnt,amount:amount,serv_tax:serv_tax,due_amount:due_amount,payed_amnt:payed,tot_due_amnt:tot_due_amnt},
        beforeSend: function(){

            $('#myDiv').show();
        },
        url:window.location.origin+'/opticalsolution/wholesale/payment_update',
        success:function(result){
            console.log(result);
            //$('#result').val(result);
            $('#myDiv').hide();

            $("#notification").fadeIn("slow").delay(3000).fadeOut("slow");
        }
    });

}

function savebtn(val,val1,val2,val3,val4,val5,val6,val7) {
    //console.log(val);
    //console.log(val1);
    //console.log(val2);
    //console.log(val3);
    //console.log(val4);
    //console.log(val5);
    //console.log(val6);
    //console.log(val7);

    var ord_stat = $("#order_status1"+val).val();
    window.location.href = window.location.origin + "/opticalsolution/wholesale/purchase_update?id=" + val + "&type=" + val1 + "&stat=" + ord_stat+"&prod_name="+val2+"&sph="+val3+"&cyl="+val4+"&axis="+val5+"&addition="+val6+"&quant="+val7;;
}

function savebtnrcv(val,val1,val2,val3,val4,val5,val6,val7){

    var ord_stat = $("#order_status1"+val).val();
    window.location.href = window.location.origin + "/opticalsolution/wholesale/received_update?id=" + val + "&type=" + val1 + "&stat=" + ord_stat+"&prod_name="+val2+"&sph="+val3+"&cyl="+val4+"&axis="+val5+"&addition="+val6+"&quant="+val7;;
}


function savedelivery(val,val1,val2,val3,val4,val5,val6,val7) {

    var ord_stat = $("#order_status1"+val).val();
    window.location.href = window.location.origin + "/opticalsolution/wholesale/delivery_update?id=" + val + "&type=" + val1 + "&stat=" + ord_stat+"&prod_name="+val2+"&sph="+val3+"&cyl="+val4+"&axis="+val5+"&addition="+val6+"&quant="+val7;;
}


var arr=[];//globally declared array

function getvalue(val){
    document.getElementById("myDiv").style.display="block";
    setTimeout("hide()", 3000);
    window.location.href = window.location.origin+"/opticalsolution/wholesale/orderlist/"+val;
    return false;
}

function addprod(val,val1,val2){
    window.location.href = window.location.origin+"/opticalsolution/wholesale/sale/"+val+"/"+val1+"/"+val2;
}


function test(val){


    var element = $(this);
    var uniq_id = element.attr("id");

    var quant = $("#quantity"+val).val();
    var unit_cost = $("#unit_cost").val();

    var tot_cost=parseInt(unit_cost*quant);

    var amount=$("#amount").val();

    var tot_amnt=amount +tot_cost

    $("#total_cost").val(tot_cost);
    $("#amount").val(tot_amnt);
}

function service(){

    console.log("Hello");

    var amount=parseFloat($('#amount').val());
    var serv_tax=parseFloat($('#serv_tax').val());

    var tot_amount=amount + serv_tax;
    if(isNaN(tot_amount)){
        tot_amount=0;
        $('#tot_amount').val(tot_amount);
    }else{
        $('#tot_amount').val(tot_amount);
    }

}

function paid_amount(){

    var tot_amount=parseInt($('#tot_amount').val());
    var paid_amount=parseInt($('#paid_amount').val());

    var due_amount=tot_amount - paid_amount;

    if(isNaN(due_amount)){
        due_amount=0;
        $('#due_amount').val(tot_amount);
    }else{
        $('#due_amount').val(due_amount);
    }
}


$(document).ready(function(){

    $('#delivcheckboxs').click(function(){
        console.log("Something has happened");
    });

    if($('#order_status').val() == "Received"){
        $("#date").prop("disabled", true);
        $("#sph").prop("disabled", true);
        $("#cyl").prop("disabled", true);
        $("#axis").prop("disabled", true);
        $("#addition").prop("disabled", true);
        $("#dia").prop("disabled", true);
        $("#base").prop("disabled", true);
        $("#side").prop("disabled", true);
        $("#quantity").prop("disabled", true);
        $("#save_orderlist").prop("disabled", true);
    }

    $("#hidden_status").change(function(){
        alert("The text has been changed.");
    });


    $('#btnclick').click(function(){

        var prod_type=$('#prod_type').val();
        var prod_name=$('#prod_name').val();
        var comp_name=$('#company_name').val();
        var specification=$('#specific').val();
        var sph=$('#sph').val();
        var cyl=$('#cyl').val();
        var axis=$('#axis').val();
        var addition=$('#addition').val();
        var dia=$('#dia').val();
        var base=$('#base').val();
        var side=$('#side').val();
        var remarks=$('#remarks_dup').val();
        var frame=$('#frame_dup').val();
        var lens=$('#lens_dup').val();
        var add_type=$('#add_type').val();
        var supplier_name="";
        var pending_qty=0;

        console.log(comp_name);
        console.log(specification);

        if(prod_type == "FITTING" || prod_type == "SOLUTION" || prod_type == "ACCESSORY"){
            var quant=$('#quant2').val();
        }else{
            var quant=$('#quant1').val();
        }

        var z={prod_type:prod_type,prod_name:prod_name,comp_name:comp_name,add_type:add_type,specification:specification,sph:sph,cyl:cyl,axis:axis,addition:addition,dia:dia,base:base,side:side,remarks:remarks,frame:frame,lens:lens,quantity:quant,supplier_name:supplier_name,pending_qty:pending_qty}
        arr.push(z);
        var jsonString = JSON.stringify(arr);

        $('#testing_data').val(jsonString);
        console.log(jsonString);

        $('#editTable').append('<tr><td>'+prod_type+'</td><td>'+quant+'</td><td><button class="glyphicon glyphicon-pencil btnclickedit" id="btnclickedit"></button></td></tr>');


        $.ajax({
            type:'POST',
            data:{data:jsonString},
            url:window.location.origin+'/opticalsolution/wholesale/welltest',
//                cache: false,
            success:function(result){
                console.log("Inside Success");
                console.log(result);
                $('#hidden_val_json').val(result);
                $('#prod_type').val("");
                $('#company_name').val("");
                $('#prod_name').val("");
                $('#sph').val("");
                $('#cyl').val("");
                $('#axis').val("");
                $('#addition').val("");
                $('#dia').val("");
                $('#base').val("");
                $('#side').val("");
                $('#remarks_dup').val("");
                $('#quant1').val("");
                $('#quant2').val("");
                $('#comp_name').val("");
                $('#frame_dup').val("");
                $('#lens_dup').val("");
                $("#btnclick").prop("disabled", true);
            }
        });

    });

    $('#btnclick2').click(function(){
        var fullname=$('#fullname').val();
        var z={fullname:fullname}
        arr.push(z);
        console.log(arr);
        var jsonString = JSON.stringify(arr);
        $.ajax({
            type:'POST',
            data:{data:jsonString},
            url:window.location.origin+'/opticalsolution/wholesale/demo_save',
            success:function(result){
                console.log(result);
                $('#result').val(result);
            }
        });
    });



    //Datatable for purchase order page
    $('#purchaseorderdatatable').DataTable(
        {
            "aaSorting": [],
            "scrollY": "300px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },

            "dom": '<"top"f>rt<"clear">',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );

    $('#violators_tbl').dataTable({
        "aoColumnDefs": [
            { "sClass": "my_class", "aTargets": [ 0 ] }
        ]
    } );


    //Datatable for receive order page
    $('#receiveorderdatatable').DataTable(
        {
            "aaSorting": [],
            "scrollY": "300px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },
            //"lengthMenu": [[50, 100, 200, -1],[50, 100, 200]],
            "sDom": 'ft',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );

    //Datatable for delivery list page
    $('#deliverylistdatatable').DataTable(
        {
            "aaSorting": [],
            "scrollY": "300px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },
            //"lengthMenu": [[50, 100, 200, -1],[50, 100, 200]],
            "sDom": 'ft',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );

    //Datatable for delivery list page
    $('#orderlistdatatable').DataTable(
        {
            "aaSorting": [],
            "scrollY": "300px",

            //"sDom": 'Bf',

            dom: 'Bf',
            buttons: [
                'print'
            ]
        }
    );

    $(".totalprice").keyup(function()
    {
        var sum = 0.0;
        $('.totalprice').each(function()
        {
            var first=$('.totalprice:eq(0)').val();
            var second=$('.totalprice:eq(1)').val();

            if(second == ""){
                $('#amount').val(first);
            }else{
                sum += parseFloat($(this).val());
                console.log(sum);
                $('#amount').val(sum);
            }
        });
    });


    $('#stocklistdatatable').DataTable(
        {
            "aaSorting": [],
            "scrollY": "200px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },
            "lengthMenu": [[200, 100, 200, -1],[200, 100, 200]],
            "sDom": 'pft',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );

});

