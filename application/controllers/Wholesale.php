<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wholesale extends CI_Controller
{

    function __construct(){

        parent::__construct(); // needed when adding a constructor to a controller
        $this->load->model('Postmodel');
    }


    function purchase(){

        $data['getdet']=$this->Postmodel->get_country_list();

        $this->load->view('app/wholesale/purchase');
    }

    function sale(){

        $data['prod_name'] = $this->config->item('prod_name');
        $data['cont_prod_name'] = $this->config->item('cont_prod_name');
        $data['cont_spec'] = $this->config->item('cont_spec');
        $data['soln_prod_name'] = $this->config->item('soln_prod_name');
        $data['grind_prod_name'] = $this->config->item('grind_prod_name');

        $prod_name=json_encode($data['prod_name']);
        $cont_prod_name=json_encode($data['cont_prod_name']);
        $cont_spec=json_encode($data['cont_spec']);
        $soln_prod_name=json_encode($data['soln_prod_name']);
        $grind_prod_name=json_encode($data['grind_prod_name']);

        $data["product_name"]=$prod_name;
        $data["cont_prod_name"]=$cont_prod_name;
        $data["cont_spec"]=$cont_spec;
        $data["soln_prod_name"]=$soln_prod_name;
        $data["grind_prod_name"]=$grind_prod_name;

        $this->load->view('app/wholesale/sale',$data);
    }

    function sale_save(){

        $datalist=array(
            'order_id'=>$this->input->post('order_id'),
            'bill_no'=>$this->input->post('bill_no'),
            'date'=>$this->input->post('date'),
            'party_name'=>$this->input->post('party_name'),
            'order_status'=>'Not Ordered',
        );


        $data['savesales'] = $this->Postmodel->save_update_sales_order($datalist);
        @$this->db->free_db_resource();

        $var=$data['savesales'][0]->order_id;
        $json=$this->input->post('hidden_val_json');
        $obj=json_decode($json);

        for($i=0;$i<sizeof($obj);$i++){

            $datalist1=array(
                'item_id'=>0,
                'order_id'=>$var,
                'prod_type'=>$obj[$i]->prod_type,
                'prod_name'=>$obj[$i]->prod_name,
                'quant'=>$obj[$i]->quantity,
                'comp_name'=>$obj[$i]->comp_name,
                'add_type'=>$obj[$i]->add_type,
                'specification'=>$obj[$i]->specification,
                'sph'=>$obj[$i]->sph,
                'cyl'=>$obj[$i]->cyl,
                'axis'=>$obj[$i]->axis,
                'addition'=>$obj[$i]->addition,
                'dia'=>$obj[$i]->dia,
                'base'=>$obj[$i]->base,
                'side'=>$obj[$i]->side,
                'remarks'=>$obj[$i]->remarks,
                'order_status'=>'Not Ordered',
                'frame'=>$obj[$i]->frame,
                'lens'=>$obj[$i]->lens,
                'supplier_name'=>$obj[$i]->supplier_name,
                'pending_quant'=>$obj[$i]->pending_qty,
            );

            $data['savesalesitem'] = $this->Postmodel->save_update_sales_order_item($datalist1);
        }
        $this->session->set_flashdata('messageSuccess', 'Data Saved Successfully');


        redirect('wholesale/sale');
    }

    function orderlist(){

        if($this->uri->segment(3) != ""){

            $datalist = array(
                'sales_order_id' => $this->uri->segment(3),
                'order_status' =>'Not Ordered',
                'offset' => 0,
                'limit' => 100,
            );

            $data['orderlistdata'] = $this->Postmodel->get_sales_order_item($datalist);
            @$this->db->free_db_resource();

//            print_r($data['orderlistdata']);exit();

            $phpdate=$data['orderlistdata'][0]->order_date;
            $mysqldate = date('Y-m-d', strtotime( $phpdate ));
            $data['date']=$mysqldate;
            $data['id']=$data['orderlistdata'][0]->id;
            $data['sales_order_id']=$data['orderlistdata'][0]->sales_order_id;
            $data['bill_no']=$data['orderlistdata'][0]->bill_no;
            $data['party_name']=$data['orderlistdata'][0]->party_name;
            $data['item_id']=$data['orderlistdata'][0]->item_id;
            $data['product_type']=$data['orderlistdata'][0]->product_type;
            $data['product_name']=$data['orderlistdata'][0]->product_name;
            $data['company_name']=$data['orderlistdata'][0]->company_name;
            $data['specification']=$data['orderlistdata'][0]->specification;
            $data['sph']=$data['orderlistdata'][0]->sph;
            $data['cyl']=$data['orderlistdata'][0]->cyl;
            $data['axis']=$data['orderlistdata'][0]->axis;
            $data['addition']=$data['orderlistdata'][0]->addition;
            $data['diameter']=$data['orderlistdata'][0]->diameter;
            $data['base']=$data['orderlistdata'][0]->base;
            $data['side']=$data['orderlistdata'][0]->side;
            $data['quantity']=$data['orderlistdata'][0]->quantity;
            $data['remarks']=$data['orderlistdata'][0]->remarks;
            $data['status']=$data['orderlistdata'][0]->status;
            $data['frame']=$data['orderlistdata'][0]->frame;
            $data['lens']=$data['orderlistdata'][0]->lens;
            $data['pending_quantity']=$data['orderlistdata'][0]->pending_quantity;
            $data['supplier_name']=$data['orderlistdata'][0]->supplier_name;
            $data['add_type']=$data['orderlistdata'][0]->add_type;
        }

        $datalist=array(
            'sales_order_id'=>0,
            'order_status' =>'Not Ordered',
            'offset'=>0,
            'limit'=>100,
        );

        $data['orderlistdata'] = $this->Postmodel->get_sales_order_item1($datalist);
        @$this->db->free_db_resource();

//        print_r($data['orderlistdata'] );exit();

        $this->load->view('app/wholesale/orderlist',$data);
    }

    function orderlist_save(){

        //Checking order number
        if($this->input->post('order_no') != ""){
            $ord_id=$this->input->post('order_no');
        }else{
            redirect('wholesale/orderlist');
        }

        $datalist=array(
            'order_id'=>$ord_id,
            'bill_no'=>$this->input->post('bill_no'),
            'date'=>$this->input->post('date'),
            'party_name'=>$this->input->post('party_name'),
            'order_status'=>$this->input->post('order_status'),
        );

        $data['orderlistupdate'] = $this->Postmodel->save_update_sales_order($datalist);
        $ord_id=$data['orderlistupdate'][0]->order_id;
        @$this->db->free_db_resource();

        $datalist1=array(
            'item_id'=>$this->input->post('item_id'),
            'order_id'=>$ord_id,
            'prod_type'=>$this->input->post('prod_type'),
            'prod_name'=>$this->input->post('prod_name'),
            'quant'=>$this->input->post('quantity'),
            'comp_name'=>$this->input->post('comp_name'),
            'add_type'=>$this->input->post('add_type'),
            'specification'=>$this->input->post('specification'),
            'sph'=>$this->input->post('sph'),
            'cyl'=>$this->input->post('cyl'),
            'axis'=>$this->input->post('axis'),
            'addition'=>$this->input->post('addition'),
            'dia'=>$this->input->post('dia'),
            'base'=>$this->input->post('base'),
            'side'=>$this->input->post('side'),
            'remarks'=>$this->input->post('remarks_dup'),
            'order_status'=>$this->input->post('order_status'),
            'frame'=>$this->input->post('frame_dup'),
            'lens'=>$this->input->post('lens_dup'),
            'supplier_name'=>$this->input->post('supplier_name'),
        );

//        print_r($datalist1);exit();

        $data['saveorderlistitem'] = $this->Postmodel->save_update_sales_order_item($datalist1);
        @$this->db->free_db_resource();

        if($this->input->post('prod_type') == "READY" && $this->input->post('order_status') == "Received"){

            $data['stockadd'] = $this->Postmodel->add_to_stock($datalist1);
        }
        if($this->input->post('prod_type') == "READY" && $this->input->post('order_status') == "Delivered To Counter"){

            $data['stockremove'] = $this->Postmodel->remove_from_stock($datalist1);
        }

        $this->session->set_flashdata('messageSuccess', 'Data Updated Successfully');

        redirect('wholesale/orderlist');

    }

    function demo(){

        $data['msg'] = $this->config->item('test');

        $json=json_encode($data['msg']);

        $data["testing_data"]=$json;

//        print_r($json);exit;

        $this->load->view('app/demo1',$data);
    }

    function demo_save(){
        print_r($_POST['data']);exit();
        $fullname=$this->input->post('fullname');
        $abc=$this->input->post('abc');
        $datalist=array(
            'fullname'=>$fullname,
            'abc'=>$abc,
        );
        array_push($data,$datalist);
        $ayz[]=$data;

        print_r($ayz);exit();
    }


    function welltest(){
        print_r($this->input->post('data'));exit();

    }

    function test_data(){
//        print_r($this->input->post('tags'));exit();

        $json=$this->input->post('Welltest');
        $obj=json_decode($json);
        print_r($obj);exit();
    }

    function stocklist(){

        $data['stocklist'] = $this->Postmodel->get_stock_list();
//        print_r($data['stocklist']);exit();
        @$this->db->free_db_resource();

        $this->load->view('app/wholesale/stocklist',$data);
    }


    function outsource_list(){

        $datalist=array(
            'sales_order_id'=>0,
            'order_status' =>'Out Source',
            'offset'=>0,
            'limit'=>100,
        );

        $data['outsourcelist']=$this->Postmodel->get_sales_order_item($datalist);

//        print_r($data['orderedlist']);exit();

        $this->load->view('app/wholesale/out_source',$data);
    }

    function outsource_update(){

        $datalist=array(
            'order_id'=>$this->input->post('id'),
            'bill_no'=>$this->input->post('bill_no'),
            'date'=>$this->input->post('order_date'),
            'party_name'=>$this->input->post('party_name'),
            'order_status'=>$this->input->post('order_status'),
        );

        $data['orderlistupdate'] = $this->Postmodel->save_update_sales_order($datalist);
        @$this->db->free_db_resource();

        $order_id=$data['orderlistupdate'][0]->order_id;

        $datalist1=array(
            'item_id'=>$this->input->post('item_id'),
            'order_id'=>$order_id,
            'prod_type'=>$this->input->post('product_type'),
            'prod_name'=>$this->input->post('product_name'),
            'quant'=>$this->input->post('quantity'),
            'comp_name'=>$this->input->post('company_name'),
            'specification'=>$this->input->post('specification'),
            'sph'=>$this->input->post('sph'),
            'cyl'=>$this->input->post('cyl'),
            'axis'=>$this->input->post('axis'),
            'addition'=>$this->input->post('addition'),
            'dia'=>$this->input->post('dia'),
            'base'=>$this->input->post('base'),
            'side'=>$this->input->post('side'),
            'remarks'=>$this->input->post('remarks'),
            'order_status'=>$this->input->post('order_status'),
            'frame'=>$this->input->post('frame_dup'),
            'lens'=>$this->input->post('lens_dup'),
        );

//        print_r($datalist1);exit();

        $data['returnid'] = $this->Postmodel->save_update_sales_order_item($datalist1);
        @$this->db->free_db_resource();
    }

    function purchase_orderlist(){

        $datalist=array(
            'sales_order_id'=>0,
            'order_status' =>'Ordered',
            'offset'=>0,
            'limit'=>100,
        );

        $data['orderedlist']=$this->Postmodel->get_sales_order_item($datalist);

        $this->load->view('app/wholesale/purchase_orderlist',$data);
    }

    function purchase_update(){

        $prod_name=str_replace('%20', '', $_GET['prod_name']);
        $sph=str_replace('%20', '', $_GET['sph']);
        $cyl=str_replace('%20', '', $_GET['cyl']);
        $axis=str_replace('%20', '', $_GET['axis']);
        $addition=str_replace('%20', '', $_GET['addition']);

        $data['orderlistupdate'] = $this->Postmodel->update_stat($_GET['id'],$_GET['stat']);
        @$this->db->free_db_resource();

        if($_GET['type'] == "READY" && $_GET['stat']== "Received"){

            $datalist2=array(
                'prod_name'=>$prod_name,
                'sph'=>$sph,
                'cyl'=>$cyl,
                'axis'=>$axis,
                'addition'=>$addition,
                'quant'=>$_GET['quant'],
            );

//            print_r($datalist2);exit();

            $data['returnstock'] = $this->Postmodel->add_to_stock($datalist2);
        }
        redirect('wholesale/purchase_orderlist');
    }


    function receivedlist(){

        $datalist=array(
            'sales_order_id'=>0,
            'order_status' =>'Received',
            'offset'=>0,
            'limit'=>100,
        );

        $data['orderedlist']=$this->Postmodel->get_sales_order_item($datalist);
        @$this->db->free_db_resource();

        $this->load->view('app/wholesale/received_orderlist',$data);
    }

    function received_update(){

        $prod_name=str_replace('%20', '', $_GET['prod_name']);
        $sph=str_replace('%20', '', $_GET['sph']);
        $cyl=str_replace('%20', '', $_GET['cyl']);
        $axis=str_replace('%20', '', $_GET['axis']);
        $addition=str_replace('%20', '', $_GET['addition']);

        $data['orderlistupdate'] = $this->Postmodel->update_stat($_GET['id'],$_GET['stat']);
        @$this->db->free_db_resource();

        if($_GET['type'] == "READY" && $_GET['stat']== "Delivered To Counter"){

            $datalist2=array(
                'prod_name'=>$prod_name,
                'sph'=>$sph,
                'cyl'=>$cyl,
                'axis'=>$axis,
                'addition'=>$addition,
                'quant'=>$_GET['quant'],
            );

            $data['returnstock'] = $this->Postmodel->remove_from_stock($datalist2);
        }

        redirect('wholesale/receivedlist');
    }

    function deliverylist(){

        $datalist=array(
            'sales_order_id'=>0,
            'order_status' =>'Delivered To Counter',
            'offset'=>0,
            'limit'=>100,
        );

        $data['orderedlist']=$this->Postmodel->get_sales_order_item($datalist);
        @$this->db->free_db_resource();

        $this->load->view('app/wholesale/delivery_list',$data);
    }

    function delivery_update(){

        $prod_name=str_replace('%20', '', $_GET['prod_name']);
        $sph=str_replace('%20', '', $_GET['sph']);
        $cyl=str_replace('%20', '', $_GET['cyl']);
        $axis=str_replace('%20', '', $_GET['axis']);
        $addition=str_replace('%20', '', $_GET['addition']);

        $data['orderlistupdate'] = $this->Postmodel->update_stat($_GET['id'],$_GET['stat']);
        @$this->db->free_db_resource();



        redirect('wholesale/deliverylist');
    }

    function deliv_save(){

        $inv_no=$this->input->post('inv_no');
        $abc=json_encode($inv_no);

        print_r($abc);exit();
    }

    function payment(){

        $data['inv_no']=$this->input->post('invoice_no');


        //checking value of checkbox
        if($this->input->post('payment')){

            $prty_nm=$this->input->post('party_name_prov');

            $data['checkprtyname'] = $this->Postmodel->check_name(strtoupper($prty_nm));
            @$this->db->free_db_resource();

//            print_r($data['checkprtyname']);exit();

            if($prty_nm == "" || $data['checkprtyname'] == ""){

                $this->session->set_flashdata('messageError', 'please provide valid party name');
                redirect($this->agent->referrer());
            }else{

                if($data['checkprtyname'][0]->due_amount == "" || $data['checkprtyname'][0]->due_amount == 0){

                    $tot_amt=0;
                    $pd_amt=0;
                    for($i=0;$i<count($data['checkprtyname']);$i++){
                        $tot_amt=$tot_amt + $data['checkprtyname'][$i]->total_amount;
                        $pd_amt=$pd_amt + $data['checkprtyname'][$i]->paid_amount;
                    }
                    $due_amt=$tot_amt-$pd_amt;

                    $data['due_amt']=$due_amt;

                }else{
                    $data['due_amt']=$data['checkprtyname'][0]->due_amount;
                }




                $this->load->view('app/wholesale/onlypayment',$data);
            }
        }else{

            $checkboxval=$this->input->post('options');

            $exp = implode(',', $checkboxval);

            $data['itemdetails'] = $this->Postmodel->get_sales_order_item_payment($exp);
            @$this->db->free_db_resource();

            $data['checkprtyname'] = $this->Postmodel->check_name(strtoupper($data['itemdetails'][0]->party_name));
            @$this->db->free_db_resource();

            //checking if due amount of a party is zero or not and implementing proper calculation of due amount
            if($data['checkprtyname'][0]->due_amount == "" || $data['checkprtyname'][0]->due_amount == 0){

                $tot_amt=0;
                $pd_amt=0;
                for($i=0;$i<count($data['checkprtyname']);$i++){
                    $tot_amt=$tot_amt + $data['checkprtyname'][$i]->total_amount;
                    $pd_amt=$pd_amt + $data['checkprtyname'][$i]->paid_amount;
                }
                $due_amt=$tot_amt-$pd_amt;

                $data['due_amt']=$due_amt;

            }else{
                $data['due_amt']=$data['checkprtyname'][0]->due_amount;
            }

            $data['item_ids']=$exp;
            $data['address']=$data['checkprtyname'][0]->address;


            $this->load->view('app/wholesale/payment',$data);
        }

    }

    function amount_test(){

        $amount=$this->input->post('amount');

        $data['amount']=$amount;


        $this->load->views('ajaxcontent/test_demo',$data);

//        print_r($data['amount']);exit();
    }

    function  delivery_history(){

        $datalist=array(
            'sales_order_id'=>0,
            'order_status' =>'Delivered',
            'offset'=>0,
            'limit'=>100,
        );

        $data['delivlist']=$this->Postmodel->get_sales_order_item($datalist);
        @$this->db->free_db_resource();

//    print_r($data['delivlist']);exit();

        $this->load->view('app/wholesale/delivery_history',$data);
    }

    //ajax call
    function payment_update(){

        $itemids=$this->input->post('item_id');
        $prtyname=$this->input->post('prty_name');
        $amount=$this->input->post('amount');
        $ser_tax=$this->input->post('serv_tax');
        $payed=$this->input->post('payed_amnt');
        $due_amount=$this->input->post('due_amount');
        $tot_amnt=$this->input->post('tot_amnt');
        $tot_due_amnt=$this->input->post('tot_due_amnt');

        $datalist=array(
            'paymnt_wholesale_id'=>0,
            'item_id'=>$itemids,
            'prty_name'=>$prtyname,
            'amount'=>$amount,
            'ser_tax'=>$ser_tax,
            'tot_amnt'=>$tot_amnt,
            'payed'=>$payed,
            'due_amount'=>$due_amount,
            'updated_by'=>1,
        );

//        print_r($datalist);exit();

        $data['savepayment']=$this->Postmodel->save_payment($datalist);
        @$this->db->free_db_resource();

        $updatedblnce= $tot_due_amnt + $due_amount;

        $data['savepayment']=$this->Postmodel->update_prty_due_blnce($prtyname,$updatedblnce);
        @$this->db->free_db_resource();
    }
}