<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

class Welcome extends CI_Controller {
    var $data;

    function __construct(){
        parent::__construct(); // needed when adding a constructor to a controller
        $this->data = array(
            'layoutmode' => $this->config->item('layoutconfigdev')
        );
        // $this->data can be accessed from anywhere in the controller.
        $this->load->model('Postmodel');
    }
    public function index()
    {
        $this->load->view('app/login');
    }

    function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if($username == "" && $password == ""){
            $this->session->set_flashdata('messageError', 'Please Provide Username & Password');
            redirect($this->agent->referrer());
        }

        $data['logincred'] = $this->Postmodel->login_cred($username, $password);
        @$this->db->free_db_resource();

        if ($username == $data['logincred'][0]->user_id && $password == $data['logincred'][0]->password) {
            if ($data['logincred'][0]->user_type == 1) {
                redirect("dashboard/counter");
            } else {
                redirect("wholesale/sale");
            }
        }else {
            $this->session->set_flashdata('messageError', 'Please Provide Valid Username & Password');
            redirect($this->agent->referrer());
        }
    }

    function signup(){
        try{
        }
        catch (Exception $ex){
        }
    }
        
    function logout(){
        redirect("welcome/index");
    }
}
?>
