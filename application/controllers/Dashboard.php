<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    function login(){
        $this->load->view('app/dologin');
    }

    function counter()
    {


        $this->load->model('Postmodel');
        if($this->uri->segment(3) != ""){
            $patient_id=$this->uri->segment(3);
        }
        else{
            $patient_id="";
        }
        $off_set = 0;
        $limit = 20;

        $datalist = array(
            'patient_id' => $patient_id,
            'off_set' => $off_set,
            'limit' => $limit,
        );

        $data['getinfo'] = $this->Postmodel->get_patient_details($datalist);
        @$this->db->free_db_resource();
//        print_r($data['getinfo']);exit();

        //specific patient details
        $data['patient_id']=$data['getinfo'][0]->patient_id;
        $data['full_name']=$data['getinfo'][0]->full_name;
        $data['dob']=$data['getinfo'][0]->dob;
        $data['age']=$data['getinfo'][0]->age;
        $data['gender']=$data['getinfo'][0]->gender;
        $data['mobile_no']=$data['getinfo'][0]->mobile_no;
        $data['mobile_no_alt']=$data['getinfo'][0]->mobile_no_alt;
        $data['address']=$data['getinfo'][0]->address;
        $data['patient_type']=$data['getinfo'][0]->patient_type;
        $data['delivery_status']=$data['getinfo'][0]->delivery_status;
        $data['back_office_status']=$data['getinfo'][0]->back_office_status;
        $data['pic_url']=$data['getinfo'][0]->pic_url;
        $data['created_by']=$data['getinfo'][0]->created_by;
        $data['doctor_id']=$data['getinfo'][0]->doctor_id;


        $data['getinfo1'] = $this->Postmodel->get_patient_details1();
        @$this->db->free_db_resource();

//        print_r($data['getinfo1']);exit();

        $data['drlist'] = $this->Postmodel->get_all_doctor_name();
        @$this->db->free_db_resource();

//        print_r($data['drlist']);exit();
        $this->load->view('app/retail/counter', $data);
    }

    function countersave()
    {
        $this->load->model('Postmodel');

        $patient_id = '';
        $appointment_id="";
        $appointment_on='';
        $patient_name = $this->input->post('patient_name');
        $dob = $this->input->post('dob');
        $cur_age = $this->input->post('cur_age');
        $sex = $this->input->post('sex');
        $mobile_numb = $this->input->post('mobile_numb');
        $alt_mobile_numb = $this->input->post('alt_mobile_numb');
        $patient_status = $this->input->post('patient_status');
        $address = $this->input->post('address');
        $deliv_status = $this->input->post('deliv_status');
        $back_ofc_status = $this->input->post('back_ofc_status');
        $dr_in = $this->input->post('dr_appointmnt');
        $pic_url = "234";
        $created_by = 1;

        $datalist = array(
            'patient_id' => $patient_id,
            'patient_name' => $patient_name,
            'dob' => $dob,
            'cur_age' => $cur_age,
            'sex' => $sex,
            'mobile_numb' => $mobile_numb,
            'alt_mobile_numb' => $alt_mobile_numb,
            'address' => $address,
            'patient_status' => $patient_status,
            'deliv_status' => $deliv_status,
            'back_ofc_status' => $back_ofc_status,
            'pic_url' => $pic_url,
            'created_by' => $created_by,
            'doctor_in' => $dr_in,
        );


        $data['savecounter'] = $this->Postmodel->save_update_patient_info($datalist);
        @$this->db->free_db_resource();

//        print_r($data['savecounter'][0]->patient_id_in);exit();

        $patid=$data['savecounter'][0]->patient_id_in;

        $datalist1 = array(
            'appointment_id' => $appointment_id,
            'doctor_in' => $dr_in,
            'patient_id' => $patid,
            'created_by' => $created_by,
            'appointment_on' => $appointment_on,
        );
//        print_r($datalist1);exit();

        $data['drapntmnt'] = $this->Postmodel->save_appointment_info($datalist1);
        redirect('dashboard/counter');
    }


    function dr_details()
    {
        $this->load->model('Postmodel');

//        print_r("Hello");exit();

//       if($this->uri->segment(4) != "" && $this->uri->segment(3) != ""){
//
//           print_r("If Part");
//           print_r($this->uri->segment(4));
//           print_r($this->uri->segment(3));exit();
//           $patient_id=$this->uri->segment(3);
//           $prescription_id=$this->uri->segment(4);
//           $offset=0;
//           $limit=20;
//
//           $datalist = array(
//               'patient_id' => $patient_id,
//               'prescription_id' => $prescription_id,
//               'offset' => $offset,
//               'limit' => $limit,
//           );
//       }
//        elseif($this->uri->segment(3) != ""){
////            print_r("Else if part");
////            print_r($this->uri->segment(3));exit();
//
//            $patient_id=$this->uri->segment(3);
//            $prescription_id="";
//            $offset=0;
//            $limit=20;
//
//            $datalist = array(
//                'patient_id' => $patient_id,
//                'prescription_id' => $prescription_id,
//                'offset' => $offset,
//                'limit' => $limit,
//            );
//        }
//
//        else{
////            print_r("Else Part");exit();
//        }
//
//        $data['specificpatientlist']= $this->Postmodel->get_prescription_info($datalist);
//        @$this->db->free_db_resource();

//        print_r($data['specificpatientlist']);exit();
//
//
// //specific patient details
//        $data['prescription_id']=$data['specificpatientlist'][0]->prescription_id;
//        $data['patient_id']=$data['specificpatientlist'][0]->patient_id;
//        $data['full_name']=$data['specificpatientlist'][0]->full_name;
//        $data['dob']=$data['specificpatientlist'][0]->dob;
//        $data['age']=$data['specificpatientlist'][0]->age;
//        $data['gender']=$data['specificpatientlist'][0]->gender;
//        $data['mobile_no']=$data['specificpatientlist'][0]->mobile_no;
//        $data['mobile_no_alt']=$data['specificpatientlist'][0]->mobile_no_alt;
//        $data['address']=$data['specificpatientlist'][0]->address;
//        $data['patient_type']=$data['specificpatientlist'][0]->patient_type;
//        $data['delivery_status']=$data['specificpatientlist'][0]->delivery_status;
//        $data['back_office_status']=$data['specificpatientlist'][0]->back_office_status;
//        $data['pic_url']=$data['specificpatientlist'][0]->pic_url;
//        $data['doctor_id']=$data['specificpatientlist'][0]->doctor_id;
//        $data['prescription_id']=$data['specificpatientlist'][0]->prescription_id;
//        $data['prescribed_by']=$data['specificpatientlist'][0]->prescribed_by;
//        $data['other_info']=$data['specificpatientlist'][0]->other_info;
////        print_r($data['other_info']);exit();
//        $data['doctor_name']=$data['specificpatientlist'][0]->doctor_name;
//
//        $data['specificlensinfo']= $this->Postmodel->get_patient_lens_info($datalist);
//        @$this->db->free_db_resource();
//
//        $data['rightdistance_eye']=$data['specificlensinfo'][0];
//        $data['rightnear_eye']=$data['specificlensinfo'][1];
//        $data['leftdistance_eye']=$data['specificlensinfo'][2];
//        $data['leftnear_eye']=$data['specificlensinfo'][3];
////        print_r($data['rightdistance_eye']);exit();
//
//        //data for right eye distance
//        $data['r_d_sph']=$data['rightdistance_eye']->sph;
//        $data['r_d_cyl']=$data['rightdistance_eye']->cyl;
//        $data['r_d_axis']=$data['rightdistance_eye']->axis;
//        $data['r_d_va']=$data['rightdistance_eye']->v_a;
//
//        //data for right eye near
//        $data['r_n_sph']=$data['rightnear_eye']->sph;
//        $data['r_n_cyl']=$data['rightnear_eye']->cyl;
//        $data['r_n_axis']=$data['rightnear_eye']->axis;
//        $data['r_n_va']=$data['rightnear_eye']->v_a;
//
//        //data for left eye distance
//        $data['l_d_sph']=$data['leftdistance_eye']->sph;
//        $data['l_d_cyl']=$data['leftdistance_eye']->cyl;
//        $data['l_d_axis']=$data['leftdistance_eye']->axis;
//        $data['l_d_va']=$data['leftdistance_eye']->v_a;
//
//        //data for left eye near
//        $data['l_n_sph']=$data['leftnear_eye']->sph;
//        $data['l_n_cyl']=$data['leftnear_eye']->cyl;
//        $data['l_n_axis']=$data['leftnear_eye']->axis;
//        $data['l_n_va']=$data['leftnear_eye']->v_a;

//        print_r($data['patient_id']);exit();
        $data['allpatientlist']= $this->Postmodel->get_prescription_info1();
//        print_r($data['allpatientlist']);exit();

        $this->load->view('app/retail/dr_details',$data);
    }

    function drdetailssave(){

        $this->load->model('Postmodel');

        $prescription_lens_id="";
        $patient_id=$this->input->post('patientid');
        $prescribed_by="dipanjan";
        $prescription_id=$this->input->post('prescription_id');
        $other_info=$this->input->post('otherinfo');
//        print_r($other_info);exit();

        $r_d_sph = $this->input->post('r_d_sph');
        $r_d_cyl = $this->input->post('r_d_cyl');
        $r_d_axis = $this->input->post('r_d_axis');
        $r_d_va = $this->input->post('r_d_va');
        $r_n_sph = $this->input->post('r_n_sph');
        $r_n_cyl = $this->input->post('r_n_cyl');
        $r_n_axis = $this->input->post('r_n_axis');
        $r_n_va = $this->input->post('r_n_va');
        $l_d_sph = $this->input->post('l_d_sph');
        $l_d_cyl = $this->input->post('l_d_cyl');
        $l_d_axis = $this->input->post('l_d_axis');
        $l_d_va = $this->input->post('l_d_va');
        $l_n_sph = $this->input->post('l_n_sph');
        $l_n_cyl = $this->input->post('l_n_cyl');
        $l_n_axis = $this->input->post('l_n_axis');
        $l_n_va = $this->input->post('l_n_va');



        $datalist = array(
            'prescribed_by' => $prescribed_by,
            'patient_id' => $patient_id,
            'prescription_id' => $prescription_id,
            'other_info' => $other_info,
        );
//        print_r($datalist);exit();

        $data['getpres']=$this->Postmodel->save_update_prescription($datalist);
        @$this->db->free_db_resource();

        $presreturn=$data['getpres'][0]->prescription_id_in;

//        print_r($presreturn);exit();

        $datalist1 = array(
            'prescription_lens_id'=>$prescription_lens_id,
            'prescription_id'=>$presreturn,
            'eye_type'=>'R',
            'measurement_type'=>'D',
            'r_d_sph' => $r_d_sph,
            'r_d_cyl' => $r_d_cyl,
            'r_d_axis' => $r_d_axis,
            'r_d_va' => $r_d_va,
            'created_by'=>1,

        );

//        print_r($datalist1);exit();
        $datalist2 = array(
            'prescription_lens_id'=>$prescription_lens_id,
            'prescription_id'=>$presreturn,
            'eye_type'=>'R',
            'measurement_type'=>'N',
            'r_n_sph' => $r_n_sph,
            'r_n_cyl' => $r_n_cyl,
            'r_n_axis' => $r_n_axis,
            'r_n_va' => $r_n_va,
            'created_by'=>1,
        );

        $datalist3 = array(
            'prescription_lens_id'=>$prescription_lens_id,
            'prescription_id'=>$presreturn,
            'eye_type'=>'L',
            'measurement_type'=>'D',
            'l_d_sph' => $l_d_sph,
            'l_d_cyl' => $l_d_cyl,
            'l_d_axis' => $l_d_axis,
            'l_d_va' => $l_d_va,
            'created_by'=>1,
        );

        $datalist4 = array(
            'prescription_lens_id'=>$prescription_lens_id,
            'prescription_id'=>$presreturn,
            'eye_type'=>'L',
            'measurement_type'=>'N',
            'l_n_sph' => $l_n_sph,
            'l_n_cyl' => $l_n_cyl,
            'l_n_axis' => $l_n_axis,
            'l_n_va' => $l_n_va,
            'created_by'=>1,
        );

//        print_r($datalist2);exit();

        $data['getpreslensid1']=$this->Postmodel->save_update_patient_lense_info1($datalist1);
        @$this->db->free_db_resource();

        $data['getpreslensid2']=$this->Postmodel->save_update_patient_lense_info2($datalist2);
        @$this->db->free_db_resource();

        $data['getpreslensid3']=$this->Postmodel->save_update_patient_lense_info3($datalist3);
        @$this->db->free_db_resource();

        $data['getpreslensid4']=$this->Postmodel->save_update_patient_lense_info4($datalist4);
        @$this->db->free_db_resource();

        redirect('dashboard/dr_details');
    }

    function sales_staff()
    {

        $this->load->view('app/retail/sales_staff');
    }

    function payment()
    {

        $this->load->view('app/retail/payment');
    }

    function back_office()
    {

        $this->load->view('app/retail/back_office');
    }

    function purchase_order()
    {


        $this->load->view('app/retail/purchase_order');
    }

}
?>

