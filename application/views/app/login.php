<?php $this->load->view('assets/css'); ?>
<div style="position: fixed; width: 100%; height: 100%; background: #428bca"  >
    <section>

        <div class="panel panel-signin">
            <div class="panel-body">
                <div style=" margin-left: 32px; ">
                    <i style="font-size: 100px;margin-left: 25%;" class="fa fa-eye fa-6"></i>
                </div>
                <h4 class="text-center mb5"></h4>
                <br/>

                <?php
                if($this->session->flashdata('messageError') != "")
                {
                    ?>

                    <div class = "alert alert-warning" id="error-alert"><?php echo $this->session->flashdata('messageError'); ?></div>

                    <?php
                }
                ?>

                <?php
                echo form_open('welcome/login');?>
                    <div class="input-group mb15">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input  class="form-control" name="username" class="LoginInputs" placeholder="User Name" type="text"  ng-trim="true">
                    </div>
                    <div class="input-group mb15">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input name="password" class="form-control" placeholder="Password" class="LoginInputs" type="password"  ng-model="user_data.password" ng-trim="true">
                    </div>
                    <div class="clearfix">
                        <div class="pull-right">
                            <button id="LogIn" type="submit" onclick="hello()" class="btn btn-success">Sign In <i class="fa fa-angle-right ml5"></i></button>
                        </div>
                    </div>
                <?php form_close();?>
                <p style="margin-top: 15px; color: #428bca;font-size: 12px; text-align: center">Powered By: Oum Technology Solutions Pvt Ltd.</p>
                <p style=" color: #428bca;text-align: center;"> Build Version: 1.0.0</p>
            </div>
            <div class="panel-footer">
            </div>
        </div>
    </section>
</div>


