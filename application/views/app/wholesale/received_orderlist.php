<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"><img id = "myImage" ></div>
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">RECEIVED ORDERLIST</a></li>
                        </ul>
                        <h4>RECEIVED ORDERLIST
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>

            <div class="col-lg-12" >
                <div class="medium no-padding" id="gridscroll">
                    <div style="height: 450px;">
                        <div class="table-responsive ">
                            <table class="table testing" id="purchaseorderdatatable">
                                <thead>
                                <tr>
                                    <th class="hath" style="font-size: 11px">ORDER NO</th>
                                    <th class="hath" style="font-size: 11px">BILL NO</th>
                                    <th class="hath" style="font-size: 11px">PARTY NAME</th>
                                    <th class="hath" style="font-size: 11px">DATE</th>
                                    <th class="hath" style="font-size: 11px">PRODUCT TYPE</th>
                                    <th class="hath" style="font-size: 11px">PRODUCT NAME</th>
                                    <th class="hath" style="font-size: 11px">ITEM DETAILS</th>
                                    <th class="hath" style="font-size: 11px" >QUANTITY</th>
                                    <th class="hath" style="font-size: 11px" >ORDER STATUS</th>
                                    <th class="hath" style="font-size: 11px" >SAVE STATUS</th>

                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach($orderedlist as $val):?>

                                    <tr style="font-size: 10px;">
                                        <td><?php echo $val->sales_order_id;?></td>
                                        <td><?php echo $val->bill_no;?></td>
                                        <td><?php echo $val->party_name;?></td>
                                        <td><?php echo date('d M y', strtotime($val->order_date));?></td>
                                        <td><?php echo $val->product_type?></td>
                                        <td><?php echo $val->product_name?></td>
                                        <?php if($val->product_type == "READY"){ ?>
                                            <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?>&nbsp;<b>side:</b><?php echo $val->side;?><br><b>remarks:</b><?php echo $val->remarks;?></td>
                                        <?php }else if($val->product_type == "RX"){ ?>
                                            <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?>&nbsp;<b>side:</b><?php echo $val->side;?><br><b>spec:</b><?php echo $val->specification;?><br><b>remarks:</b><?php echo $val->remarks;?></td>
                                        <?php }else if($val->product_type == "GRINDING"){ ?>
                                            <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?><br><b>side:</b><?php echo $val->side;?>&nbsp;<b>dia:</b><?php echo $val->diameter;?>&nbsp;<b>base:</b><?php echo $val->base;?><br><?php $val->remarks; ?></td>
                                        <?php }else if($val->product_type == "CONTACT_LENS"){ ?>
                                            <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?>&nbsp;<b>side:</b><?php echo $val->side;?><br><b>spec:</b><?php echo $val->specification;?><br><b>remarks:</b><?php echo $val->remarks;?></td>
                                        <?php }else if($val->product_type == "SOLUTION"){ ?>
                                            <td><b>remarks:</b><?php $val->remarks; ?></td>
                                        <?php }else if($val->product_type == "ACCESSORY"){ ?>
                                            <td><b>remarks:</b><?php $val->remarks; ?></td>
                                        <?php }else if($val->product_type == "FITTING"){ ?>
                                            <td><b>frame:</b><?php $val->frame; ?>&nbsp;<b>lens:</b><?php $val->lens; ?>&nbsp;<b>remarks:</b><?php $val->remarks; ?></td>
                                        <?php }else{ ?>
                                        <?php } ?>
                                        <td><?php echo $val->quantity?></td>
                                        <td>
                                            <select name="order_status1" id="order_status1<?php echo $val->item_id;?>">
                                                <option value ="Received">RECEIVED</option>
                                                <option value="Delivered To Counter">Delivered To Counter</option>
                                            </select>
                                        </td>
                                        <td><input type="button" onclick="savebtnrcv(<?php echo $val->item_id;?>,'<?php echo $val->product_type;?>','<?php echo $val->product_name;?>','<?php echo $val->sph;?>','<?php echo $val->cyl;?>','<?php echo $val->axis;?>','<?php echo $val->addition;?>','<?php echo $val->quantity;?>')" value="save"></td>
                                    </tr>

                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <?php $this->load->view('layouts/footer'); ?>

</section>