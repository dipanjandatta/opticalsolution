<html>
<body onload="myFunction()" style="margin:0;">

<div id="loader"></div>

<div style="display:none;" id="myDiv" class="animate-bottom">


</div>

<script>
    var myr;

    function myFunction() {
        myVar = setTimeout(showPage, 2000);
    }

    function showPage() {
        document.getElementById("loader").style.display = "none";
        document.getElementById("myDiv").style.display = "block";
    }
</script>
<?php $this->load->view('assets/css'); ?>
<?php $this->load->view('assets/js'); ?>
<head>
    <style>
        a:hover {
            background-color: yellow;
        }
    </style>
</head>

<?php $this->load->view('layouts/header'); ?>
<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">BULK PURCHASE</a></li>
                        </ul>
                        <h4>BULK PURCHASE
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>
            <div class="row">
                <?php
                echo form_open('dashboard/countersave');?>

                <div class="col-lg-6">
                    <div class="form-horizontal">

                        <div class="form-group ">
                            <label class="col-sm-4 control-label">ORDER NO</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="order_no" type="text" placeholder="ORDER NO" disabled>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-sm-4 control-label">REFERENCE NO</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="reference_no" type="text" placeholder="REFERENCE NO" disabled>

                            </div>
                        </div>

                        <div class="form-group ">
                            <label class="col-sm-4 control-label">DATE</label>
                            <div class="col-sm-8">
                                <input name="date" class="form-control" type="date" placeholder="DATE" maxlength="30"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label" >PARTY NAME</label>
                            <div class="col-sm-8">
                                <input name="party_name" class="form-control" type="text" id="CMCompanyName" placeholder="PARTY NAME" maxlength="30"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label" >ITEMS</label>
                            <div class="col-sm-8">
                                <select name="sex" class="form-control" id="ddlTrackCycle" title="Select Track Cycle" ">
                                <option value="" selected="selected">SELECT ITEMS</option>
                                <option value="GKB" >GKB</option>
                                <option value="MCG" >MCG</option>
                                <option value="FCA" >FCA</option>
                                </select>
                            </div>
                        </div>


                        <div class="col-sm-12" id="ready" style="margin-left: 16px;">
                            <div class="panel panel-default panelFixer">
                                <div class="panel-heading panelBlue" style="text-align: center;">
                                    ------ITEM DETAILS------
                                </div>
                                <div class="panel-body formPadder">

                                    <div class="control-group form-group formFix" id="sph_cyl" style="display:block">
                                        <div class="col-md-6 ">
                                            <div class="col-md-6">
                                                SPH
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                                CYL
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix" id="axis_add" style="display:block">
                                        <div class="col-md-6 ">
                                            <div class="col-md-6">
                                                AXIS
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                                ADDITION
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix" id="axis_add" style="display:block">
                                        <div class="col-md-6 ">
                                            <div class="col-md-6">
                                                QUANTITY
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                    </div>

                                    <!--                                        <div class="control-group form-group formFix" style="margin-top: 15px;">-->
                                    <!--                                            <div class="col-md-3 padRgtFix padLeftFiver">-->
                                    <!--                                            </div>-->
                                    <!--                                            <div class="col-md-3 padRgtFix padLeftFiver">-->
                                    <!--                                                <input type="submit" name="new" value="NEW" class="eilmbutton" />-->
                                    <!--                                            </div>-->
                                    <!--                                            <div class="col-md-3 padRgtFix padLeftFiver">-->
                                    <!--                                                <input type="submit" name="cancel" value="CANCEL" class="eilmbutton" />-->
                                    <!--                                            </div>-->
                                    <!--                                            <div class="col-md-3 padRgtFix padLeftFiver">-->
                                    <!--                                                <input type="submit" name="save" value="SAVE" class="eilmbutton" />-->
                                    <!--                                            </div>-->
                                    <!--                                        </div>-->
                                </div>
                            </div>
                        </div>

                        <div class="pull-right" style="margin-bottom: 68px;">
                            <input type="submit" name="save" class="btn btn-primary" id="btnsave1" value="Save" />
                            <input type="submit" class="btn btn-primary" id="btnCancelComp" value="Cancel" />
                        </div>
                    </div>
                </div>

                <div class="col-lg-6" >
                    <div class="input-group">
                        <input type="text" class="form-control"  placeholder="Search" >
                        <span class="input-group-addon" style="cursor: pointer">
                            <i class="fa fa-search" title="Search"></i>
                        </span>
                    </div>
                    <div class="medium no-padding" id="gridscroll" style="height: 425px" >
                        <div style="height: 250px; overflow: scroll;">
                            <div class="table-responsive ">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th >&nbsp;</th>
                                        <th class="hath" >ORDER NO</th>
                                        <th class="hath">REFERENCE NO</th>
                                        <th class="hath">DATE</th>
                                        <th class="hath">PARTY NAME</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr >
                                        <td class="text-center"><input type="radio" name="id" ></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php form_close();?>

            </div>

            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">ITEM DETAILS</h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel-body">
                                <label class="col-sm-2 control-label" for="lg">SPH</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" id="lg" ng-model="itemdetails.sph" ng-disabled="false" placeholder="SPH">
                                </div>

                                <label class="col-sm-2 control-label" for="lg">CYL</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" id="lg" ng-model="itemdetails.cyl" ng-disabled="false" placeholder="CYL">
                                </div>

                                <label class="col-sm-2 control-label" for="lg">AXIS</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" id="lg" ng-model="itemdetails.axis" ng-disabled="false" placeholder="AXIS">
                                </div>

                                <label class="col-sm-2 control-label" for="lg">ADDITION</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" id="lg" ng-model="itemdetails.addition" ng-disabled="false" placeholder="ADDITION">
                                </div>

                                <label class="col-sm-2 control-label">SIDE</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" id="lg" ng-model="itemdetails.side" ng-disabled="false" placeholder="SIDE">
                                </div>

                                <label class="col-sm-2 control-label" for="lg">QTY ORDER</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" id="lg" ng-model="itemdetails.qty" ng-disabled="false" placeholder="QTY ORDER">
                                </div>

                                <div class="form-group ">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-8">
                                        <button class="btn btn-sm btn-info margin7 ng-scope" ng-click="save_item_data()">Save Details</button>
                                        <button class="btn btn-sm btn-info margin7 ng-scope"   ng-click="clear_data();">Edit Details</button>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

    <?php $this->load->view('layouts/footer'); ?>
    </div>
</section>