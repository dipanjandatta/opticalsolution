<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"><img id = "myImage" ></div>
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">OUT SOURCE LIST</a></li>
                        </ul>
                        <h4>OUT SOURCE LIST
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>


            <div class="col-lg-12" >

                <div class="medium no-padding" id="gridscroll">
                    <div style="height: 450px;">
                        <div class="table-responsive ">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="hath" style="font-size: 11px">ORDER NO</th>
                                    <th class="hath" style="font-size: 11px">BILL NO</th>
                                    <th class="hath" style="font-size: 11px">PARTY NAME</th>
                                    <th class="hath" style="font-size: 11px">DATE</th>
                                    <th class="hath" style="font-size: 11px">PRODUCT TYPE</th>
                                    <th class="hath" style="font-size: 11px">PRODUCT NAME</th>
                                    <th class="hath" style="font-size: 11px" >QUANTITY</th>
                                    <th class="hath" style="font-size: 11px" >ORDER STATUS</th>
                                    <th class="hath" style="font-size: 11px" >SAVE STATUS</th>

                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach($outsourcelist as $val):?>
                                    <?php echo form_open('wholesale/outsource_update')?>

                                    <tr style="font-size: 10px;">

                                        <input type="hidden" value="<?php echo $val->id;?>" name="id"/>
                                        <input type="hidden" value="<?php echo $val->item_id;?>" name="item_id"/>
                                        <input type="hidden" value="<?php echo $val->sales_order_id;?>" name="order_id"/>
                                        <input type="hidden" value="<?php echo $val->bill_no;?>" name="bill_no"/>
                                        <input type="hidden" value="<?php echo $val->party_name;?>" name="party_name"/>
                                        <input type="hidden" value="<?php echo date('Y-m-d', strtotime($val->order_date));?>" name="order_date"/>
                                        <input type="hidden" value="<?php echo $val->product_type;?>" name="product_type"/>
                                        <input type="hidden" value="<?php echo $val->product_name;?>" name="product_name"/>
                                        <input type="hidden" value="<?php echo $val->company_name;?>" name="company_name"/>
                                        <input type="hidden" value="<?php echo $val->specification;?>" name="specification"/>
                                        <input type="hidden" value="<?php echo $val->sph;?>" name="sph"/>
                                        <input type="hidden" value="<?php echo $val->cyl;?>" name="cyl"/>
                                        <input type="hidden" value="<?php echo $val->axis;?>" name="axis"/>
                                        <input type="hidden" value="<?php echo $val->addition;?>" name="addition"/>
                                        <input type="hidden" value="<?php echo $val->diameter;?>" name="diameter"/>
                                        <input type="hidden" value="<?php echo $val->base;?>" name="base"/>
                                        <input type="hidden" value="<?php echo $val->side;?>" name="side"/>
                                        <input type="hidden" value="<?php echo $val->quantity;?>" name="quantity"/>
                                        <input type="hidden" value="<?php echo $val->remarks;?>" name="remarks"/>
                                        <input type="hidden" value="<?php echo $val->frame;?>" name="frame_dup"/>
                                        <input type="hidden" value="<?php echo $val->lens;?>" name="lens_dup"/>

                                        <td><?php echo $val->sales_order_id;?></td>
                                        <td><?php echo $val->bill_no;?></td>
                                        <td><?php echo $val->party_name;?></td>
                                        <td><?php echo date('Y-m-d', strtotime($val->order_date));?></td>
                                        <td><?php echo $val->product_type?></td>
                                        <td><?php echo $val->product_name?></td>
                                        <td><?php echo $val->quantity?></td>
                                        <td>
                                            <select name="order_status" id="order_status">
                                                <option value="Out Source"<?php if($val->status == "Out Source"){?> selected="selected" <?php }?>>OUT SOURCE</option>
                                                <option value ="Received"<?php if($val->status == "Received"){?> selected="selected" <?php }?>>RECEIVED</option>
                                            </select>
                                        </td>
                                        <td><input type="submit" value="save"> </td>

                                    </tr>

                                    <?php echo form_close();?>


                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('layouts/footer'); ?>

</section>