<html>
<body onload="myFunction()" style="margin:0;">

<div id="loader"></div>

<div style="display:none;" id="myDiv" class="animate-bottom">


</div>

<script>
    var myr;

    function myFunction() {
        myVar = setTimeout(showPage, 2000);
    }

    function showPage() {
        document.getElementById("loader").style.display = "none";
        document.getElementById("myDiv").style.display = "block";
    }
</script>
<?php $this->load->view('assets/css'); ?>
<?php $this->load->view('assets/js'); ?>
<head>
    <style>
        a:hover {
            background-color: yellow;
        }
    </style>
</head>

<?php $this->load->view('layouts/header'); ?>
<section>
    <div class="mainwrapper" style="margin-bottom: 63px;">
        <div class="leftpanel">
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">SALE</a></li>
                        </ul>
                        <h4>SALE
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>
            <div class="row">
                <?php
                echo form_open('dashboard/countersave');?>

                <div class="col-lg-6">
                    <div ng-hide="loading" class="widget-content"  >
                        <div class=" ng-scope form-horizontal">
                            <div class="form-group ">
                                <label class="col-sm-4 control-label" for="lg">ORDER NO</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" id="lg" ng-model="sale_data.order_id" ng-disabled="true" placeholder="ORDER NO">
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-4 control-label" for="lg">REFERENCE NO</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" id="lg" ng-model="sale_data.reference_no" placeholder="REFERENCE NO">
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-4 control-label" for="lg">DATE</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="date" id="lg" ng-model="sale_data.order_date" placeholder="DATE">
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-4 control-label" for="lg">PARTY NAME</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text"  ng-model="sale_data.party_name" placeholder="PARTY NAME">
                                </div>
                            </div>

                            <!--item details-->
                            <div class="col-sm-12" id="ready" style="margin-left: 16px;display: block">
                                <div class="panel panel-default panelFixer">
                                    <div class="panel-heading panelBlue" style="text-align: center;">
                                        ------ITEM DETAILS------
                                    </div>
                                    <div class="panel-body formPadder">
                                        <div class="control-group form-group formFix">
                                            <div class="col-md-12 ">
                                                <div class="col-md-3">
                                                    PRODUCT TYPE
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="form-control" onchange="showText(this)">
                                                        <option value="">SELECT TYPE</option>
                                                        <option value="READY">READY</option>
                                                        <option value ="RX">RX</option>
                                                        <option value ="GRINDING">GRINDING</option>
                                                        <option value ="CONTACT_LENS">CONTACT LEN</option>
                                                        <option value ="SOLUTION">SOLUTION</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="control-group form-group formFix" id="product_name" style="display: none">
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    PRODUCT NAME
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix" id="quantity" style="display: none">
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    QUANTITY
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix" id="comp_name" style="display: none">
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    COMPANY NAME
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix" id="specification" style="display: none">
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    SPECIFICATION
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="control-group form-group formFix" id="sph_cyl" style="display:block">
                                            <div class="col-md-6 ">
                                                <div class="col-md-6">
                                                    SPH
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    CYL
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix" id="axis_add" style="display:block">
                                            <div class="col-md-6 ">
                                                <div class="col-md-6">
                                                    AXIS
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    ADDITION
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix" id="dia_base" style="display:none">
                                            <div class="col-md-6 ">
                                                <div class="col-md-6">
                                                    DIAMETER
                                                </div>
                                                <div class="col-md-6">
                                                    <select class="form-control">
                                                        <option value="">SELECT</option>
                                                        <option value="">60 X 3 TO 8</option>
                                                        <option value ="">65 X 3 TO 8</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    BASE
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix" id="side_qty" style="display:block">
                                            <div class="col-md-6 ">

                                                <div class="col-md-6">
                                                    SIDE
                                                </div>
                                                <div class="col-md-6">
                                                    <select class="form-control">
                                                        <option value="">SELECT SIDE</option>
                                                        <option value="RE">RE</option>
                                                        <option value="LE">LE</option>
                                                        <option value="BOTH">BOTH</option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    QUANTITY
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix" id="remarks" style="display: none">
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    REMARKS
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>

                                        <!--                                        <div class="control-group form-group formFix" style="margin-top: 15px;">-->
                                        <!--                                            <div class="col-md-3 padRgtFix padLeftFiver">-->
                                        <!--                                            </div>-->
                                        <!--                                            <div class="col-md-3 padRgtFix padLeftFiver">-->
                                        <!--                                                <input type="submit" name="new" value="NEW" class="eilmbutton" />-->
                                        <!--                                            </div>-->
                                        <!--                                            <div class="col-md-3 padRgtFix padLeftFiver">-->
                                        <!--                                                <input type="submit" name="cancel" value="CANCEL" class="eilmbutton" />-->
                                        <!--                                            </div>-->
                                        <!--                                            <div class="col-md-3 padRgtFix padLeftFiver">-->
                                        <!--                                                <input type="submit" name="save" value="SAVE" class="eilmbutton" />-->
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <srd-widget>
                        <div class="widget" >
                            <div class="widget" >
                                <srd-widget-header icon="fa-tasks" title="Servers" class="ng-scope ng-isolate-scope">
                                    <!--<hr/>-->
                                </srd-widget-header>
                            </div>
                            <srd-widget-header icon="fa-tasks" title="Striped Servers" class="ng-scope ng-isolate-scope">
                                <div class="widget-header ng-binding">
                                    <div class="input-group">
                                        <input type="text" class="form-control" ng-model="searchBy" placeholder="Search" >
                    <span class="input-group-addon" style="cursor: pointer">
                        <i class="fa fa-search" title="Search"></i>
                    </span>
                                    </div>
                                </div>
                            </srd-widget-header>

                            <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope" >
                                <div class="widget-body medium no-padding" id="gridscroll" style="height: 250px" ng-class="classes">

                                    <div ng-hide="loading" class="widget-content" >
                                        <div class="table-responsive ng-scope fixed-panel" style="height: 208px !important;">
                                            <table class="table fixed-panel" ><thead>
                                                <tr>
                                                    <th >&nbsp;</th>
                                                    <th class="hath" ng-click="predicate = 'order_no'; reverse=!reverse">ORDER NO</th>
                                                    <th class="hath" ng-click="predicate = 'ref_no'; reverse=!reverse">REFERENCE NO</th>
                                                    <th class="hath" ng-click="predicate = 'date'; reverse=!reverse">DATE</th>
                                                    <th class="hath" ng-click="predicate = 'customer_name'; reverse=!reverse">PARTY NAME</th>
                                                    <th class="hath" ng-click="predicate = 'order_details'; reverse=!reverse">ITEM NAME</th>
                                                </tr>
                                                </thead>
                                                <tbody >
                                                <tr ng-repeat="list in datalist | filter: searchBy | orderBy:predicate:reverse">
                                                    <td class="text-center"><input type="radio" name="id"  ng-checked="checking" ng-click="pop_sale_data(list)"></td>
                                                    <!--<td>TEST LENCE</td>-->
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </srd-widget-body>
                        </div>
                    </srd-widget>
                </div>
                <?php form_close();?>

            </div>

        </div>

    </div>

    <?php $this->load->view('layouts/footer'); ?>
    </div>
</section>

<script>

    function showText(elem){


        if(elem.value == 'RX'){
            document.getElementById('product_name').style.display = "block";
            document.getElementById('comp_name').style.display = "block";
            document.getElementById('sph_cyl').style.display = "block";
            document.getElementById('axis_add').style.display = "block";
            document.getElementById('side_qty').style.display = "block";
            document.getElementById('specification').style.display = "block";
            document.getElementById('dia_base').style.display = "none";

        }
        if(elem.value == 'READY'){
            document.getElementById('product_name').style.display = "none";
            document.getElementById('comp_name').style.display = "none";
            document.getElementById('specification').style.display = "none";
            document.getElementById('quantity').style.display = "none";
            document.getElementById('sph_cyl').style.display = "block";
            document.getElementById('axis_add').style.display = "block";
            document.getElementById('side_qty').style.display = "block";
            document.getElementById('dia_base').style.display = "none";

        }
        if(elem.value == 'CONTACT_LENS'){
            document.getElementById('product_name').style.display = "block";
            document.getElementById('comp_name').style.display = "block";
            document.getElementById('specification').style.display = "block";
            document.getElementById('quantity').style.display = "none";
            document.getElementById('sph_cyl').style.display = "block";
            document.getElementById('axis_add').style.display = "block";
            document.getElementById('side_qty').style.display = "block";
            document.getElementById('dia_base').style.display = "none";

        }
        if(elem.value == 'SOLUTION'){
            document.getElementById('product_name').style.display = "block";
            document.getElementById('comp_name').style.display = "none";
            document.getElementById('specification').style.display = "none";
            document.getElementById('quantity').style.display = "block";
            document.getElementById('sph_cyl').style.display = "none";
            document.getElementById('axis_add').style.display = "none";
            document.getElementById('side_qty').style.display = "none";
            document.getElementById('dia_base').style.display = "none";

        }
        if(elem.value == 'GRINDING'){
            document.getElementById('product_name').style.display = "none";
            document.getElementById('comp_name').style.display = "none";
            document.getElementById('specification').style.display = "none";
            document.getElementById('quantity').style.display = "none";
            document.getElementById('sph_cyl').style.display = "block";
            document.getElementById('axis_add').style.display = "block";
            document.getElementById('side_qty').style.display = "block";
            document.getElementById('dia_base').style.display = "block";
            document.getElementById('remarks').style.display = "block";
        }

    }

</script>