<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"><img id = "myImage" ></div>
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">STOCKLIST</a></li>
                        </ul>
                        <h4>STOCKLIST
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>


            <div class="col-lg-12" style="height: 450px;">
                <div class="medium no-padding" id="gridscroll">
                    <table class="table" id="stocklistdatatable">
                        <thead>
                        <tr>
                            <th class="hath" style="font-size: 11px">PRODUCT NAME</th>
                            <th class="hath" style="font-size: 11px">SPH</th>
                            <th class="hath" style="font-size: 11px">CYL</th>
                            <th class="hath" style="font-size: 11px">AXIS</th>
                            <th class="hath" style="font-size: 11px">ADDITION</th>
                            <th class="hath" style="font-size: 11px" >QUANTITY</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($stocklist as $val):?>
                            <tr style="font-size: 10px;">
                                <td><?php echo $val->product_name?></td>
                                <td><?php echo $val->sph?></td>
                                <td><?php echo $val->cyl?></td>
                                <td><?php echo $val->axis?></td>
                                <td><?php echo $val->addition?></td>
                                <td><?php echo $val->quantity?></td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



</section>

<?php $this->load->view('layouts/footer'); ?>