<?php $this->load->view('assets/css'); ?>
<?php $this->load->view('assets/js'); ?>

<?php $this->load->view('layouts/header'); ?>


<section>
    <div class="mainwrapper" style="margin-bottom: 63px;">
        <div class="leftpanel">
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->
        <?php
        if($this->session->flashdata('messageSuccess'))
        {
            ?>
            <script>
                notif({
                    type: "success",
                    msg: '<?php echo $this->session->flashdata('messageSuccess'); ?>',
                    position: "right",
                    width: 520,
                    height: 60,
                    autohide: true,
                });
            </script>
            <?php
        }
        ?>



        <div id="printableArea" style="display: none">
            <div style="float: left; width: 20%;">
                <img src="<?php echo base_url(); ?>images/logo1.png" style="max-width: 80%;margin-top: 11px;" />
            </div>
            <div style="float: left; width: 52%;font-size: 41px;margin-top: 21px;">
                Optical Solution
            </div>
            <div style="float:left;margin-top: 17px;margin-left: 10px">

                <div>
                    <strong style="padding:0px 5px 0px 0px">Order No:</strong>
                </div>
                <div id="order_no_print">

                </div>&nbsp;&nbsp;

                <div>
                    <strong style="padding:0px 5px 0px 0px">D.C No:</strong>
                </div>
                <div id="bill_no_print">

                </div>&nbsp;&nbsp;

                <div>
                    <strong style="padding:0px 5px 0px 0px">Party Name</strong>
                </div>
                <div id="party_name_print">

                </div>&nbsp;&nbsp;

                <div>
                    <strong style="padding:0px 5px 0px 0px">Date</strong>
                </div>
                <div id="date_print">

                </div>&nbsp;&nbsp;

            </div>

            <div class="preshrview" style="border: 2px solid green; margin: 15px 0px; float: left; width: 100%;"></div>

            <div style="float: left; width: 100%;">

                <table style="width: 100%;     border: 1px solid #D8D5D5;">
                    <tr>
                        <th style="background: #CECDCD; text-align: center;">PROD TYPE</th>
                        <th style="background: #CECDCD; text-align: center;">PROD NAME</th>
                        <th style="background: #CECDCD; text-align: center;">ITEM DETAILS</th>
                        <th style="background: #CECDCD; text-align: center;">QTY</th>
                    </tr>

                    <tr>
                        <td id="grid_product_type"></td>
                        <td id="grid_product_name"></td>
                        <td><span id="grid_sph">sph:</span >&nbsp;cyl:<span id="grid_cyl"></span>&nbsp;axis:<span id="grid_axis"></span>&nbsp;add:<span id="grid_add"></span></td>
                        <td></td>
                    </tr>

                </table>

            </div>
            <div>
                <!--            <div style="display:inline-block;margin-top: 10px;margin-left: 50px"><strong>Staff Id:</strong>  --><?php //echo $this->session->user_type_id; ?><!--</div>-->
                <div style="float:right;display:inline-block;margin-right: 200px;margin-top: 10px"><strong>Signature: </strong></div>

            </div>
        </div>



        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">SALE</a></li>
                        </ul>
                        <h4>SALE
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>
            <div class="row">

                <div class="col-lg-6">
                    <div class=" ng-scope form-horizontal">

                        <?php echo form_open('wholesale/sale_save'); ?>

                        <div class="form-group ">
                            <label class="col-sm-4 control-label" for="lg">ORDER NO</label>

                            <?php if($this->uri->segment(3) != ""){?>

                            <div class="col-sm-8">
                                <input class="form-control" type="text" id="ord_no"  value="<?php echo $this->uri->segment(3); ?>" placeholder="ORDER NO" disabled>
                                <input class="form-control" type="hidden" id="ord_id_hidden" name="order_id" value="<?php echo $this->uri->segment(5); ?>">
                            </div>
                                
                            <?php }else{ ?>

                                <div class="col-sm-8">
                                    <input class="form-control" type="text" id="ord_id_hidden" name="order_no"  placeholder="ORDER NO" disabled>
                                </div>

                            <?php } ?>

                        </div>

                        <div class="form-group ">
                            <label class="col-sm-4 control-label" for="lg">BILL NO</label>

                            <?php if($this->uri->segment(4) != ""){ ?>

                            <div class="col-sm-8">
                                <input class="form-control" type="number" id="bill_no" name="bill_no" value="<?php echo $this->uri->segment(4);?>" placeholder="BILL NO">
                            </div>

                            <?php }else{ ?>

                                <div class="col-sm-8">
                                    <input class="form-control" type="number" id="bill_no" name="bill_no" placeholder="BILL NO">
                                </div>

                            <?php } ?>

                        </div>

                        <input class="form-control" type="hidden" id="hidden_val_json" name="hidden_val_json" />

                        <div class="form-group ">
                            <label class="col-sm-4 control-label" for="lg">DATE</label>
                            <div class="col-sm-8">
                                <input class="form-control" type="date" id="date" value="<?php echo date("Y-m-d")?>" name="date" placeholder="DATE">
                            </div>
                        </div>

                        <div class="form-group ">
                            <label class="col-sm-4 control-label" for="lg">PARTY NAME</label>
                            <div class="col-sm-8">
                                <input class="form-control" type="text" id="party_name" name="party_name" placeholder="PARTY NAME">
                            </div>
                        </div>

                        <!--item details-->
                        <div class="col-sm-12 fetch_results"  style="margin-left: 16px;display: block">
                            <div class="panel panel-default panelFixer">
                                <div class="panel-heading panelBlue" style="text-align: center;">
                                    ------ITEM DETAILS------
                                </div>
                                <div class="panel-body formPadder">
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-12 ">
                                            <div class="col-md-3">
                                                PRODUCT TYPE
                                            </div>
                                            <div class="col-md-5">
                                                <select class="form-control" name="prod_type" id="prod_type" onchange="showText(this)">
                                                    <option value="">SELECT TYPE</option>
                                                    <option value="READY">READY</option>
                                                    <option value ="RX">RX</option>
                                                    <option value ="GRINDING">GRINDING</option>
                                                    <option value ="CONTACT_LENS">CONTACT LENS</option>
                                                    <option value ="SOLUTION">SOLUTION</option>
                                                    <option value ="ACCESSORY">ACCESSORY</option>
                                                    <option value ="FITTING">FITTING</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="control-group form-group formFix" id="product_name" style="display: none">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                PRODUCT NAME
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" name="prod_name" id="prod_name" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group form-group formFix" id="frame" style="display: none">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                FRAME
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" name="frame_dup" id="frame_dup" class="form-control eilmlitecontrols more">
                                                <input type="hidden" name="testing_data" id="testing_data" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group form-group formFix" id="lens" style="display: none">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                LENS
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" name="lens_dup" id="lens_dup" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group form-group formFix" id="quantity" style="display: none">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                QUANTITY
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" name="quant2" id="quant2" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group form-group formFix" id="comp_name" style="display: none">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                COMPANY NAME
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" name="company_name" id="company_name" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix" id="specification" style="display: none">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                SPECIFICATION
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" name="specific" id="specific" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group form-group formFix" id="sph_cyl" style="display:block">
                                        <div class="col-md-6 ">
                                            <div class="col-md-6">
                                                SPH
                                            </div>
                                            <div class="col-md-6">
                                                <input type="number" name="sph" id="sph" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                                CYL
                                            </div>
                                            <div class="col-md-6">
                                                <input type="number" name="cyl" id="cyl" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix" id="axis_add" style="display:block">
                                        <div class="col-md-6 ">
                                            <div class="col-md-6">
                                                AXIS
                                            </div>
                                            <div class="col-md-6">
                                                <input type="number" name="axis" id="axis" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                                <select class="form-control" name="add_type" id="add_type">
                                                    <option value="">SELECT</option>
                                                    <option value="NEAR ADD">NEAR ADD</option>
                                                    <option value ="NEAR POWER">NEAR POWER</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="number" name="addition" id="addition" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix" id="dia_base" style="display:none">
                                        <div class="col-md-6 ">
                                            <div class="col-md-6">
                                                DIAMETER
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control" name="dia" id="dia">
                                                    <option value="">SELECT</option>
                                                    <option value="60">60 X 3 TO 8</option>
                                                    <option value ="65">65 X 3 TO 8</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                                BASE
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control" name="base" id="base">
                                                    <option value="">SELECT</option>
                                                    <option value="3">3</option>
                                                    <option value ="4">4</option>
                                                    <option value ="5">5</option>
                                                    <option value ="6">6</option>
                                                    <option value ="8">8</option>
                                                    <option value ="10">10</option>
                                                    <option value ="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix" id="side_qty" style="display:block">
                                        <div class="col-md-6 ">

                                            <div class="col-md-6">
                                                SIDE
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control" name="side" id="side" >
                                                    <option value="">SELECT SIDE</option>
                                                    <option value="RE">RE</option>
                                                    <option value="LE">LE</option>
                                                    <option value="BE">BE</option>
                                                    <option value="NA">N/A</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                                QUANTITY
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="quant1" id="quant1" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix" id="remarks" style="display: none">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                REMARKS
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" name="remarks_dup" id="remarks_dup" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                    </div>



                                    <div class="control-group form-group formFix" style="margin-top: 15px;">
                                        <div class="col-md-3 padRgtFix padLeftFiver">
                                        </div>
                                        <div class="col-md-3 padRgtFix padLeftFiver">
                                            <input type="button" name="add" id="btnclick" class="glyphicon glyphicon-plus" value="+" style="font-size: 27px;background: #428bca;color: whitesmoke;border: white;" class="eilmbutton" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5">
                    <div class="table-responsive fixed-panel" style="height: 442px;border: #b5a6a6 solid 1px;border-bottom-color: #b5a6a6;">
                        <table class="table fixed-panel " id="editTable">
                            <thead>
                            <tr>
                                <th class="hath">PRODUCT TYPE</th>
                                <th class="hath">QUANTITY</th>
                                <th class="hath">EDIT/DELETE</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="table-responsive fixed-panel" style="border: #b5a6a6 solid 1px;height: 67px;border-top-color: white;">
                        <table class="table fixed-panel" id="editTable">

<!--                            <div class="col-md-6 padRgtFix padLeftFiver" style="text-align:left;margin-top:17px" >-->
<!--                                <input type="button" name="save" value="PRINT" onclick="printDiv('printableArea')" class="eilmbutton" style="color: white;background: #428bca;border: white;font-size: 14px;"/>-->
<!--                            </div>-->

                            <div class="col-md-6 padRgtFix padLeftFiver" style="text-align:right;margin-top:17px" >
                                <input type="submit" name="save" value="SAVE" class="eilmbutton" style="color: white;background: #428bca;border: white;font-size: 14px;"/>
                            </div>
                        </table>
                    </div>

                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <?php $this->load->view('layouts/footer'); ?>
</section>

<script>

    function printDiv(divName) {

        var testdata=document.getElementById('hidden_val_json').value;

        var jsonparse=$.parseJSON(testdata);

       console.log(jsonparse);


        var grp= "";
        var grp1 = "";
        var grp2 = "";
        var grp3 = "";
        var grp4 = "";
        var grp5 = "";
        var grp6 = "";
        var grp7 = "";
        var grp8 = "";
        for(var i=0; i<jsonparse.length;i++){

            grp+=jsonparse[i].prod_type +"<br>";
            grp1+=jsonparse[i].prod_name +"<br>";
            grp2+=jsonparse[i].sph +"<br>";
            grp3+=jsonparse[i].cyl +"<br>";
            grp4+=jsonparse[i].axis +"<br>";
            grp5+=jsonparse[i].addition +"<br>";
//            document.getElementById('bill_no_print').innerHTML = text1;
//            document.getElementById('party_name_print').innerHTML = text2;
//            document.getElementById('date_print').innerHTML = text3;

        }
        document.getElementById('grid_product_name').innerHTML = grp;
        document.getElementById('grid_product_type').innerHTML = grp1;
        document.getElementById('grid_sph').innerHTML = grp2;
        document.getElementById('grid_cyl').innerHTML = grp3;
        document.getElementById('grid_axis').innerHTML = grp4;
        document.getElementById('grid_add').innerHTML = grp5;



        var originalContents = document.body.innerHTML;
//        var text = document.getElementById('ord_id_hidden').value;
//        var text1 = document.getElementById('bill_no').value;
//        var text2 = document.getElementById('party_name').value;
//        var text3 = document.getElementById('date').value;
//
//        document.getElementById('order_no_print').innerHTML = text;
//        document.getElementById('bill_no_print').innerHTML = text1;
//        document.getElementById('party_name_print').innerHTML = text2;
//        document.getElementById('date_print').innerHTML = text3;
//
        document.getElementById('printableArea').style.visibility = 'visible';

        var printContents = document.getElementById(divName).innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }



    $( function() {

        $( "#prod_type" ).change(function() {

            var prod_type=$('#prod_type').val();

            if(prod_type == "CONTACT_LENS"){
                var test=<?php echo $cont_prod_name;?>;
                var spec=<?php echo $cont_spec;?>;
            }else if(prod_type == "SOLUTION"){
                var test=<?php echo $soln_prod_name;?>;
            }else if(prod_type == "GRINDING"){
                var test=<?php echo $grind_prod_name;?>;
            }else{
                var test=<?php echo $product_name;?>;
            }

            console.log(prod_type);

            $( "#prod_name" ).autocomplete({
                source: test,
                minLength: 1,
                search: function(oEvent, oUi) {
                    // get current input value
                    var sValue = $(oEvent.target).val().toUpperCase();
                    console.log(sValue);
                    // init new search array
                    var aSearch = [];
                    // for each element in the main array ...
                    $(test).each(function(iIndex, sElement) {
                        // ... if element starts with input value
                        if (sElement.substr(0, sValue.length) == sValue) {
                            // add element
                            aSearch.push(sElement);
                        }
                    });
                    // change search array
                    $(this).autocomplete('option', 'source', aSearch);
                }
            });

            $( "#specific" ).autocomplete({
                source: spec,
                minLength: 1,
                search: function(oEvent, oUi) {
                    // get current input value
                    var sValue = $(oEvent.target).val().toUpperCase();
                    console.log(sValue);
                    // init new search array
                    var aSearch = [];
                    // for each element in the main array ...
                    $(spec).each(function(iIndex, sElement) {
                        // ... if element starts with input value
                        if (sElement.substr(0, sValue.length) == sValue) {
                            // add element
                            aSearch.push(sElement);
                        }
                    });
                    // change search array
                    $(this).autocomplete('option', 'source', aSearch);
                }
            });

        });


    } );


    var prod_type= document.getElementById('prod_type').value;
    var btnclick= document.getElementById('btnclick').value;

    if(prod_type == "") {
        document.getElementById('btnclick').disabled = true;
    }

    function showText(elem){
        if(elem.value == 'RX'){
            document.getElementById('product_name').style.display = "block";
            document.getElementById('comp_name').style.display = "block";
            document.getElementById('sph_cyl').style.display = "block";
            document.getElementById('axis_add').style.display = "block";
            document.getElementById('side_qty').style.display = "block";
            document.getElementById('specification').style.display = "block";
            document.getElementById('dia_base').style.display = "none";
            document.getElementById('lens').style.display = "none";
            document.getElementById('frame').style.display = "none";
            document.getElementById('btnclick').disabled = false;




        }
        if(elem.value == 'READY'){
            document.getElementById('product_name').style.display = "block";
            document.getElementById('comp_name').style.display = "none";
            document.getElementById('specification').style.display = "none";
            document.getElementById('quantity').style.display = "none";
            document.getElementById('sph_cyl').style.display = "block";
            document.getElementById('axis_add').style.display = "block";
            document.getElementById('side_qty').style.display = "block";
            document.getElementById('dia_base').style.display = "none";
            document.getElementById('lens').style.display = "none";
            document.getElementById('frame').style.display = "none";
            document.getElementById('btnclick').disabled = false;


        }
        if(elem.value == 'CONTACT_LENS'){
            document.getElementById('product_name').style.display = "block";
            document.getElementById('comp_name').style.display = "block";
            document.getElementById('specification').style.display = "block";
            document.getElementById('quantity').style.display = "none";
            document.getElementById('sph_cyl').style.display = "block";
            document.getElementById('axis_add').style.display = "block";
            document.getElementById('side_qty').style.display = "block";
            document.getElementById('dia_base').style.display = "none";
            document.getElementById('lens').style.display = "none";
            document.getElementById('frame').style.display = "none";
            document.getElementById('btnclick').disabled = false;


        }
        if(elem.value == 'SOLUTION'){
            document.getElementById('product_name').style.display = "block";
            document.getElementById('comp_name').style.display = "none";
            document.getElementById('specification').style.display = "none";
            document.getElementById('quantity').style.display = "block";
            document.getElementById('sph_cyl').style.display = "none";
            document.getElementById('axis_add').style.display = "none";
            document.getElementById('side_qty').style.display = "none";
            document.getElementById('dia_base').style.display = "none";
            document.getElementById('lens').style.display = "none";
            document.getElementById('frame').style.display = "none";
            document.getElementById('btnclick').disabled = false;


        }
        if(elem.value == 'GRINDING'){
            document.getElementById('product_name').style.display = "block";
            document.getElementById('comp_name').style.display = "none";
            document.getElementById('specification').style.display = "none";
            document.getElementById('quantity').style.display = "none";
            document.getElementById('sph_cyl').style.display = "block";
            document.getElementById('axis_add').style.display = "block";
            document.getElementById('side_qty').style.display = "block";
            document.getElementById('dia_base').style.display = "block";
            document.getElementById('remarks').style.display = "block";
            document.getElementById('lens').style.display = "none";
            document.getElementById('frame').style.display = "none";
            document.getElementById('btnclick').disabled = false;

        }
        if(elem.value == 'ACCESSORY'){
            document.getElementById('product_name').style.display = "block";
            document.getElementById('comp_name').style.display = "none";
            document.getElementById('specification').style.display = "none";
            document.getElementById('quantity').style.display = "block";
            document.getElementById('sph_cyl').style.display = "none";
            document.getElementById('axis_add').style.display = "none";
            document.getElementById('side_qty').style.display = "none";
            document.getElementById('dia_base').style.display = "none";
            document.getElementById('remarks').style.display = "block";
            document.getElementById('lens').style.display = "none";
            document.getElementById('frame').style.display = "none";
            document.getElementById('btnclick').disabled = false;
        }
        if(elem.value == 'FITTING'){
            document.getElementById('product_name').style.display = "none";
            document.getElementById('comp_name').style.display = "none";
            document.getElementById('specification').style.display = "none";
            document.getElementById('quantity').style.display = "block";
            document.getElementById('sph_cyl').style.display = "none";
            document.getElementById('axis_add').style.display = "none";
            document.getElementById('side_qty').style.display = "none";
            document.getElementById('dia_base').style.display = "none";
            document.getElementById('remarks').style.display = "none";
            document.getElementById('lens').style.display = "block";
            document.getElementById('frame').style.display = "block";
            document.getElementById('btnclick').disabled = false;
        }




        if(elem.value == ''){
            document.getElementById('btnclick').disabled = true;

        }

    }
</script>

