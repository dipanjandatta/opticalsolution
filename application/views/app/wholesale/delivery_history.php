<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"><img id = "myImage" ></div>
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">

            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">DELIVERY HISTORY</a></li>
                        </ul>
                        <h4>DELIVERY HISTORY
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>


            <div class="col-lg-12" >
                <div class="medium no-padding" id="gridscroll">
                    <div style="height: 450px;">
                        <div class="table-responsive ">
                            <table class="table testing" id="purchaseorderdatatable">
                                <thead>
                                <tr>
                                    <th class="hath" style="font-size: 11px">ORDER NO</th>
                                    <th class="hath" style="font-size: 11px">BILL NO</th>
                                    <th class="hath" style="font-size: 11px">PARTY NAME</th>
                                    <th class="hath" style="font-size: 11px">DATE</th>
                                    <th class="hath" style="font-size: 11px">PRODUCT TYPE</th>
                                    <th class="hath" style="font-size: 11px">PRODUCT NAME</th>
                                    <th class="hath" style="font-size: 11px">ITEM DETAILS</th>
                                    <th class="hath" style="font-size: 11px" >QUANTITY</th>
                                    <th class="hath" style="font-size: 11px" >ORDER STATUS</th>


                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach($delivlist as $val):?>

                                    <?php echo form_open('wholesale/purchase_update')?>

                                    <tr style="font-size: 10px;">

                                        <input type="hidden" value="<?php echo $val->id;?>" name="id" />
                                        <input type="hidden" value="<?php echo $val->sales_order_id;?>" name="order_id"/>
                                        <input type="hidden" value="<?php echo $val->bill_no;?>" name="bill_no"/>
                                        <input type="hidden" value="<?php echo $val->party_name;?>" name="party_name"/>
                                        <input type="hidden" value="<?php echo date('Y-m-d', strtotime($val->order_date));?>" name="order_date"/>
                                        <input type="hidden" value="<?php echo $val->product_type;?>" name="product_type"/>
                                        <input type="hidden" value="<?php echo $val->product_name;?>" name="product_name"/>
                                        <input type="hidden" value="<?php echo $val->company_name;?>" name="company_name"/>
                                        <input type="hidden" value="<?php echo $val->specification;?>" name="specification"/>
                                        <input type="hidden" value="<?php echo $val->sph;?>" name="sph"/>
                                        <input type="hidden" value="<?php echo $val->cyl;?>" name="cyl"/>
                                        <input type="hidden" value="<?php echo $val->axis;?>" name="axis"/>
                                        <input type="hidden" value="<?php echo $val->addition;?>" name="addition"/>
                                        <input type="hidden" value="<?php echo $val->diameter;?>" name="diameter"/>
                                        <input type="hidden" value="<?php echo $val->base;?>" name="base"/>
                                        <input type="hidden" value="<?php echo $val->side;?>" name="side"/>
                                        <input type="hidden" value="<?php echo $val->quantity;?>" name="quantity"/>
                                        <input type="hidden" value="<?php echo $val->remarks;?>" name="remarks"/>
                                        <input type="hidden" value="<?php echo $val->frame;?>" name="frame_dup"/>
                                        <input type="hidden" value="<?php echo $val->lens;?>" name="lens_dup"/>

                                        <td><?php echo $val->sales_order_id;?></td>
                                        <td><?php echo $val->bill_no;?></td>
                                        <td><?php echo $val->party_name;?></td>
                                        <td><?php echo date('d M y', strtotime($val->order_date));?></td>
                                        <td><?php echo $val->product_type?></td>
                                        <td><?php echo $val->product_name?></td>
                                        <?php if($val->product_type == "READY"){ ?>
                                            <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?>&nbsp;<b>side:</b><?php echo $val->side;?><br><b>remarks:</b><?php echo $val->remarks;?></td>
                                        <?php }else if($val->product_type == "RX"){ ?>
                                            <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?>&nbsp;<b>side:</b><?php echo $val->side;?><br><b>spec:</b><?php echo $val->specification;?><br><b>remarks:</b><?php echo $val->remarks;?></td>
                                        <?php }else if($val->product_type == "GRINDING"){ ?>
                                            <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?><br><b>side:</b><?php echo $val->side;?>&nbsp;<b>dia:</b><?php echo $val->diameter;?>&nbsp;<b>base:</b><?php echo $val->base;?><br><?php $val->remarks; ?></td>
                                        <?php }else if($val->product_type == "CONTACT LENS"){ ?>
                                            <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?>&nbsp;<b>side:</b><?php echo $val->side;?><br><b>spec:</b><?php echo $val->specification;?><br><b>remarks:</b><?php echo $val->remarks;?></td>
                                        <?php }else if($val->product_type == "SOLUTION"){ ?>
                                            <td><b>remarks:</b><?php $val->remarks; ?></td>
                                        <?php }else if($val->product_type == "ACCESSORY"){ ?>
                                            <td><b>remarks:</b><?php $val->remarks; ?></td>
                                        <?php }else if($val->product_type == "FITTING"){ ?>
                                            <td><b>frame:</b><?php $val->frame; ?>&nbsp;<b>lens:</b><?php $val->lens; ?>&nbsp;<b>remarks:</b><?php $val->remarks; ?></td>
                                        <?php }else{ ?>
                                        <?php } ?>
                                        <td><?php echo $val->quantity?></td>


                                       <TD>DELIVERED</TD>

                                    </tr>

                                    <?php echo form_close();?>

                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('layouts/footer'); ?>
</section>

