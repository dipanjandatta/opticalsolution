<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>

<div class="content-wrapper">
    <div class="container">
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <h4 class="page-head-line">Billing Details <input type="button" onclick="printDiv('printableArea')" value="Print this Bill" class="btn btn-primary" style="float: right;" /></h4>-->
<!--            </div>-->
<!--        </div>-->
        <div class="row">
            <div class="col-md-12 listb">
                <div id="printableArea">
                    <div style="float: left; width: 20%;">
                        <img src="<?php echo base_url(); ?>images/logo1.png" style="max-width: 80%;margin-top: 11px;" />
                    </div>
                    <div style="float: left; width: 52%;font-size: 41px;margin-top: 21px;">
                        Edifice Healthcare Pvt. Ltd
                    </div>
                    <div style="float:left;margin-top: 17px;margin-left: 10px">




                    </div>
                    <div style="line-height: 40px;">
                        <?php
                        $ov = '';
                        foreach($bi as $val):
                            $newval = $val->name;
                            if($ov != $newval){
                                ?>
                                <strong style="font-size: 15px;">Invoice No:</strong> <?php echo $randNo = rand(1111111111,9999999999); ?>
                                <div class="pdname" style="font-size: 18px;">
                                <?php
                                echo $val->name;
                            }
                            $ov = $val->name;
                            ?>
                            </div>
                            <br />


                        <?php endforeach; ?>

                        <?php
                        //                            if($ab!=$val2){
                        //                                $birthDate = $val2;
                        //                                //explode the date to get month, day and year
                        //                                $birthDate = explode("/", $birthDate);
                        //                                //get age from date or birthdate
                        //                                $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
                        //                                    ? ((date("Y") - $birthDate[2]) - 1)
                        //                                    : (date("Y") - $birthDate[2]));
                        ////                                echo "Age is:" . $age;
                        //                            ?>
                        <!---->
                        <!--                            Age:  --><?php //echo $age; ?>
                        <!--                            --><?php //}
                        //                        else{
                        //                            echo $age;
                        //                        }?>






                    </div>

                    <div class="preshrview" style="border: 2px solid green; margin: 15px 0px; float: left; width: 100%;"></div>
                    <?php if($this->session->userdata('user_type') == 'Pathologist'){ ?>
                        <div style="float: left; width: 100%;">
                            <?php
                            $counter=1;
                            ?>
                            <table style="width: 100%;     border: 1px solid #D8D5D5;">
                                <tr>
                                    <th style="background: #CECDCD; text-align: center;">Sl No</th>
                                    <th style="background: #CECDCD; text-align: center;">Item Name</th>
                                    <th style="background: #CECDCD; text-align: center;">Item Price</th>
                                </tr>
                                <?php
                                $summ = 0;

                                foreach($cci as $val):
                                    $summ = $summ + $val->testprice;
                                    ?>
                                    <tr>
                                        <td style="text-align: center;">
                                            <?php echo $counter++; ?>
                                        </td>
                                        <td style="text-align: center;">
                                            <?php echo $val->testname; ?>
                                        </td>


                                        <!--                                <td style="text-align: center;">-->
                                        <!--                                    --><?php //echo $val->testdescription; ?>
                                        <!--                                </td>-->
                                        <td style="text-align: center;">
                                            <?php echo $val->testprice; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                            <div style="float: right; width: 80%; text-align: right;">
                                <div style="float: left; width: 59%;">
                                    <strong>Billing Amount: </strong>
                                </div>
                                <div style="float: left; width: 7%;">
                                    <?php echo $this->session->billing_amount;  ?>
                                </div>
                                <div style="float: left; width: 59%;">
                                    <strong>Discount(%): </strong>
                                </div>
                                <div style="float: left; width: 7%;">
                                    <?php echo $this->session->discount; ?>
                                </div>
                                <div style="float: left; width: 59%;">
                                    <strong>Discount Amount: </strong>
                                </div>
                                <div style="float: left; width: 7%;">
                                    <?php echo $this->session->discount_amount; ?>
                                </div>
                                <div style="float: left; width: 59%;">
                                    <strong>Pay Amount: </strong>
                                </div>
                                <div style="float: left; width: 7%;">
                                    <?php echo $this->session->recieved_amount; ?>
                                </div>
                                <div style="float: left; width: 59%;">
                                    <strong>Due Amount: </strong>
                                </div>
                                <div style="float: left; width: 7%;">
                                    <?php echo $this->session->due_amount; ?>
                                </div>
                                <br/>
                                <br/>
                                <div style="float: left; width: 60%;margin-top: 20px">
                                    <strong>Amount in words: </strong>&nbsp;<?php echo "Rupees ";?> <?php echo $billing; ?> <?php echo " "?><?php echo "Only"?>
                                </div>



                                <!--                            <div style="float: left; width: 23%;">-->
                                <!--                                <strong> </strong>-->
                                <!--                            </div>-->
                                <!---->
                                <!--                            <div style="float: left; width: 58%;">-->
                                <!--                                </div>-->



                                <div style="float: left; width: 20%;">
                                    <?php

                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div style="float: left; width: 100%;">
                            <table style="width: 100%;     border: 1px solid #D8D5D5;">
                                <tr>
                                    <th style="background: #CECDCD; text-align: center;">Item Name</th>
                                    <th style="background: #CECDCD; text-align: center;">Item Price / Qty</th>
                                </tr>
                                <?php
                                foreach($cci as $val):
                                    ?>
                                    <tr>
                                        <td style="text-align: center;">
                                            <?php echo $val->name; ?>
                                        </td>
                                        <td style="text-align: center;">
                                            <?php echo $val->price; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                            <div style="float: right; width: 50%; text-align: right;">
                                <div style="float: left; width: 59%;">
                                    <strong>Amount Payable: </strong>
                                </div>
                                <div style="float: left; width: 20%;">
                                    <?php echo $fixer; ?>
                                </div>
                                <div style="float: left; width: 59%;">
                                    <strong>VAT: </strong>
                                </div>
                                <div style="float: left; width: 20%;">
                                    14.5%
                                </div>
                                <div style="float: left; width: 59%;">
                                    <strong>Grand / Total Amount Payable (in Rs.): </strong>
                                </div>
                                <div style="float: left; width: 20%;">
                                    <?php
                                    $pervar = (14.5 / 100) * $fixer;
                                    $tot = $fixer + $pervar;
                                    echo round($tot);
                                    ?>
                                </div>



                            </div>
                        </div>
                    <?php } ?>
                    <div>
                        <div style="display:inline-block;margin-top: 10px;margin-left: 50px"><strong>Staff Id:</strong>  <?php echo $this->session->user_type_id; ?></div>
                        <div style="float:right;display:inline-block;margin-right: 200px;margin-top: 10px"><strong>Signature: </strong></div>

                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
