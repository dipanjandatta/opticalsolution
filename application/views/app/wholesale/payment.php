<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>

<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"><img id = "myImage" ></div>
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->


        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">

                        <div id="notification"  style="display: none;position: absolute;top: 3px;right: 1px;width: 28%;z-index: 105;text-align: center;font-size: 14px;font-weight: 700;color: white;background-color: #60b544;padding: 9px;">Data Saved Successfully </div>

                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">PAYMENT </a></li>
                        </ul>
                        <h4>PAYMENT
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>

            <div style="position: absolute;top: 1px;right: 46px;"><label>Due Amount : <?php echo $due_amt; ?> </label></div>
            <input type="hidden" id="tot_due_amnt" value="<?php echo $due_amt; ?> "/>

            <?php for($i=0;$i<count($itemdetails);$i++){?>

                <?php if($i>0 && $itemdetails[$i]->sales_order_id == $itemdetails[$i-1]->sales_order_id){?>

                    <?php continue;?>
                <?php }else{ ?>

                    <div class="col-md-12">

                        <div class="col-md-3"><label>Invoice No : <?php echo $inv_no; ?> </label></div>
                        <div class="col-md-3"><label>Order No : <?php echo $itemdetails[$i]->sales_order_id; ; ?> </label></div>
                        <div class="col-md-3"><label>Bill No : <?php echo $itemdetails[$i]->bill_no; ; ?> </label></div>
                        <div class="col-md-3"><label>Party Name : <?php echo $itemdetails[0]->party_name; ?> </label></div>
                        <div ><input type="hidden" name="hid_prty_name" id="hid_prty_name" value="<?php echo $itemdetails[0]->party_name; ?>"/></div>
                    </div>

                    <div class="col-md-12" style="border-bottom: dashed 1px black "></div>

                <?php } ?>

            <?php }?>

            <div class="col-md-12">
                <div class="col-md-9">
                    <div style="height: 250px;">
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="hath" style="font-size: 11px">SL NO</th>
                                <th class="hath" style="font-size: 11px">PRODUCT TYPE</th>
                                <th class="hath" style="font-size: 11px">PRODUCT NAME</th>
                                <th class="hath" style="font-size: 11px">ITEM DETAILS </th>
                                <th class="hath" style="font-size: 11px">QUANTITY</th>
                                <th class="hath" style="font-size: 11px">TOTAL COST</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $i=0;?>
                            <?php foreach($itemdetails as $val):?>
                                <tr style="font-size: 10px;">
                                    <td><?php echo $i + 1;?></td>
                                    <td><?php echo $val->product_type;?></td>
                                    <td><?php echo $val->product_name;?></td>
                                    <?php if($val->product_type == 'READY'){ ?>
                                        <td>
                                            sph:<?php echo $val->sph;?> &nbsp;cyl:<?php echo $val->cyl;?>&nbsp; axis:<?php echo $val->axis;?> &nbsp;addition:<?php echo $val->addition;?>
                                            <br>
                                            side:<?php echo $val->side;?>&nbsp;&nbsp;remarks:<?php echo $val->remarks;?>
                                        </td>
                                    <?php }elseif($val->product_type == 'RX'){ ?>
                                        <td>
                                            sph:<?php echo $val->sph;?> &nbsp;cyl:<?php echo $val->cyl;?>&nbsp; axis:<?php echo $val->axis;?> &nbsp;addition:<?php echo $val->addition;?>
                                            <br>
                                            side:<?php echo $val->side;?>&nbsp;&nbsp;remarks:<?php echo $val->remarks;?>
                                            <br>
                                            specification:<?php echo $val->specification;?>
                                        </td>
                                    <?php }elseif($val->product_type == 'GRINDING'){ ?>
                                        <td>
                                            sph:<?php echo $val->sph;?> &nbsp;cyl:<?php echo $val->cyl;?>&nbsp; axis:<?php echo $val->axis;?> &nbsp;addition:<?php echo $val->addition;?>
                                            <br>
                                            side:<?php echo $val->side;?>&nbsp;diameter:<?php echo $val->diameter;?>&nbsp;base:<?php echo $val->base;?>&nbsp;remarks:<?php echo $val->remarks;?>
                                            <br>
                                            specification:<?php echo $val->specification;?>
                                        </td>
                                    <?php }elseif($val->product_type == 'CONTACT LENS'){ ?>
                                        <td>
                                            sph:<?php echo $val->sph;?> &nbsp;cyl:<?php echo $val->cyl;?>&nbsp; axis:<?php echo $val->axis;?> &nbsp;addition:<?php echo $val->addition;?>
                                            <br>
                                            side:<?php echo $val->side;?>&nbsp;&nbsp;remarks:<?php echo $val->remarks;?>
                                            <br>
                                            specification:<?php echo $val->specification;?>
                                        </td>
                                    <?php }elseif($val->product_type == 'SOLUTION'){ ?>
                                        <td>
                                            remarks:<?php echo $val->remarks;?>
                                        </td>
                                    <?php }elseif($val->product_type == 'ACCESSORY'){ ?>
                                        <td>
                                            remarks:<?php echo $val->remarks;?>
                                        </td>
                                    <?php }else{ ?>
                                        <td>
                                            remarks:<?php echo $val->remarks;?>
                                        </td>
                                    <?php } ?>


                                    <td><?php echo $val->quantity;?></td>
                                    <input type="hidden" id="quantity<?php echo $val->id;?>" value="<?php echo $val->quantity;?>">
                                    <td><input type="number" name="total_cost" id="total_cost" class="totalprice" value="<?php echo set_value('total_cost')?>" style="width:50px" ></td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                        <div class="col-md-3"><label>Party Address : <?php echo $address; ?> </label></div>
                        <div ><input type="hidden" name="hid_prty_adrs" id="hid_prty_adrs" value="<?php echo $address; ?>"/></div>
                    </div>
                </div>
                <div class="col-md-3" style="margin-top: 40px">
                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            AMOUNT
                        </div>
                        <div class="col-md-4">
                            <input type="text" name="amount" id="amount" style="width: 60px" class="form-control eilmlitecontrols more" disabled>
                            <input type="hidden" name="amount_hid" id="amount_hid" />
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            SERVICE TAX
                        </div>
                        <div class="col-md-4">
                            <input type="text"  name="serv_tax" id="serv_tax" style="width: 60px"   onkeyup="service()" class="form-control eilmlitecontrols more">
                            <input type="hidden" name="serv_tax_hid" id="serv_tax_hid" />
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            TOTAL AMOUNT
                        </div>
                        <div class="col-md-4">
                            <input type="text" style="width: 60px" name="tot_amount" id="tot_amount" class="form-control eilmlitecontrols more" disabled />
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            PAID AMOUNT
                        </div>
                        <div class="col-md-4">
                            <input type="text" style="width: 60px" name="paid_amount" id="paid_amount" onkeyup="paid_amount()" class="form-control eilmlitecontrols more" />
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            DUE AMOUNT
                        </div>
                        <div class="col-md-4">
                            <input type="text" name="due_amount" id="due_amount" style="width: 60px" class="form-control eilmlitecontrols more" disabled/>
                            <input type="hidden"  name="flag_val" id="flag_val" />
                            <input type="hidden"  name="item_id" id="item_id" />
                            <input type="hidden"  name="item_ids_get" id="item_ids_get" value="<?php echo $item_ids; ?>"/>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-12">
                <div class="pull-right" style="margin-bottom: 68px;">
                    <input type="button" class="btn btn-primary" id="btnprint" onclick="printDiv('printableArea')" value="Print" disabled="disabled"/>
                </div>
                <div class="pull-right" style="margin-bottom: 68px;">
                    <input type="button" onclick="returnval()" class="btn btn-primary" id="btnsave" value="Save"/>
                </div>
            </div>
        </div>
    </div>

    <div id="printableArea" style="display: none">
        <div style="float: left; width: 20%;">
            <img src="<?php echo base_url(); ?>images/logo1.png" style="max-width: 80%;margin-top: 11px;" />
        </div>
        <div style="float: left; width: 52%;font-size: 41px;margin-top: 21px;">
            Optical Solution
        </div>
        <div style="float:left;margin-top: 17px;margin-left: 10px">

            <strong style="padding:0px 5px 0px 0px">Order No:</strong> <?php echo $val->sales_order_id; ?>&nbsp;&nbsp;

            <strong style="padding:0px 5px 0px 5px">DC No:</strong><?php echo $val->bill_no; ?>&nbsp;&nbsp;

            <strong style="padding:0px 5px 0px 5px">Party Name:</strong><?php echo $val->party_name; ?><br>

            <strong style="padding:0px 5px 0px 5px">Order Date:</strong><?php echo date('Y-m-d', strtotime($val->order_date)); ?>&nbsp;&nbsp;

            <strong style="padding:0px 5px 0px 5px">Delivery Date:</strong><?php echo date('Y-m-d'); ?>

        </div>
        <div style="line-height: 40px;">

            <strong style="font-size: 15px;">Invoice No:</strong> <?php echo $inv_no ?>
            <div class="pdname" style="font-size: 18px;">

            </div>
            <br />
        </div>

        <div class="preshrview" style="border: 2px solid green; margin: 15px 0px; float: left; width: 100%;"></div>

        <div style="float: left; width: 100%;">
            <?php
            $counter=1;
            ?>
            <table style="width: 100%;     border: 1px solid #D8D5D5;">
                <tr>
                    <th style="background: #CECDCD; text-align: center;">SL NO</th>
                    <th style="background: #CECDCD; text-align: center;">PRODUCT TYPE</th>
                    <th style="background: #CECDCD; text-align: center;">PRODUCT NAME</th>
                    <th style="background: #CECDCD; text-align: center;">ITEM DETAILS</th>
                    <th style="background: #CECDCD; text-align: center;">QUANTITY</th>
                </tr>
                <?php
                $summ = 0;

                foreach($itemdetails as $val):
                    ?>
                    <tr>
                        <td style="text-align: center;">
                            <?php echo $counter++; ?>
                        </td>
                        <td style="text-align: center;">
                            <?php echo $val->product_type; ?>
                        </td>

                        <td style="text-align: center;">
                            <?php echo $val->product_name; ?>
                        </td>

                        <?php if($val->product_type == 'READY'){ ?>
                            <td style="text-align: center;">
                                sph:<?php echo $val->sph;?> &nbsp;cyl:<?php echo $val->cyl;?>&nbsp; axis:<?php echo $val->axis;?> &nbsp;addition:<?php echo $val->addition;?>
                                <br>
                                side:<?php echo $val->side;?>&nbsp;&nbsp;remarks:<?php echo $val->remarks;?>
                            </td>
                        <?php }elseif($val->product_type == 'RX'){ ?>
                            <td style="text-align: center;">

                                sph:<?php echo $val->sph;?> &nbsp;cyl:<?php echo $val->cyl;?>&nbsp; axis:<?php echo $val->axis;?> &nbsp;addition:<?php echo $val->addition;?>
                                <br>
                                side:<?php echo $val->side;?>&nbsp;&nbsp;remarks:<?php echo $val->remarks;?>
                                <br>
                                specification:<?php echo $val->specification;?>
                            </td>
                        <?php }elseif($val->product_type == 'GRINDING'){ ?>
                            <td style="text-align: center;">
                                sph:<?php echo $val->sph;?> &nbsp;cyl:<?php echo $val->cyl;?>&nbsp; axis:<?php echo $val->axis;?> &nbsp;addition:<?php echo $val->addition;?>
                                <br>
                                side:<?php echo $val->side;?>&nbsp;diameter:<?php echo $val->diameter;?>&nbsp;base:<?php echo $val->base;?>&nbsp;remarks:<?php echo $val->remarks;?>
                                <br>
                                specification:<?php echo $val->specification;?>
                            </td>
                        <?php }elseif($val->product_type == 'CONTACT LENS'){ ?>
                            <td style="text-align: center;">
                                sph:<?php echo $val->sph;?> &nbsp;cyl:<?php echo $val->cyl;?>&nbsp; axis:<?php echo $val->axis;?> &nbsp;addition:<?php echo $val->addition;?>
                                <br>
                                side:<?php echo $val->side;?>&nbsp;&nbsp;remarks:<?php echo $val->remarks;?>
                                <br>
                                specification:<?php echo $val->specification;?>
                            </td>
                        <?php }elseif($val->product_type == 'SOLUTION'){ ?>
                            <td style="text-align: center;">
                                remarks:<?php echo $val->remarks;?>
                            </td>
                        <?php }elseif($val->product_type == 'ACCESSORY'){ ?>
                            <td style="text-align: center;">
                                remarks:<?php echo $val->remarks;?>
                            </td>
                        <?php }else{ ?>
                            remarks:<?php echo $val->remarks;?>
                        <?php } ?>

                        <td style="text-align: center;">
                            <?php echo $val->quantity; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <div style="float: right; width: 80%; text-align: right;">
                <div style="float: left; width: 59%;">
                    <strong>Billing Amount: </strong>
                </div>
                <div style="float: left; width: 7%;" id="amount_display">
                </div>
                <div style="float: left; width: 59%;">
                    <strong>Other Services: </strong>
                </div>
                <div style="float: left; width: 7%;" id="service_tax_display">
                </div>
                <div style="float: left; width: 59%;">
                    <strong>Total Amount: </strong>
                </div>
                <div style="float: left; width: 7%;" id="tot_amount_display">
                </div>
                <div style="float: left; width: 59%;">
                    <strong>Pay Amount: </strong>
                </div>
                <div style="float: left; width: 7%;" id="paid_amount_display">
                </div>
                <div style="float: left; width: 59%;">
                    <strong>Due Amount: </strong>
                </div>
                <div style="float: left; width: 7%;" id="due_amount_display">
                </div>
                <br/>
                <br/>
                <div style="float: left; width: 60%;margin-top: 20px">
                    <!--                        <strong>Amount in words: </strong>&nbsp;--><?php //echo "Rupees ";?><!-- --><?php //echo $billing; ?><!-- --><?php //echo " "?><!----><?php //echo "Only"?>
                </div>

                <div style="float: left; width: 20%;">
                    <?php

                    ?>
                </div>
            </div>
        </div>
        <div>
            <div style="float:right;display:inline-block;margin-right: 200px;margin-top: 10px"><strong>Signature: </strong></div>
        </div>
    </div>


</section>

<script type="text/javascript">
    function printDiv(divName) {

        var originalContents = document.body.innerHTML;
        var text = document.getElementById('amount').value;
        var text1 = document.getElementById('serv_tax').value;
        var text2 = document.getElementById('tot_amount').value;
        var text3 = document.getElementById('paid_amount').value;
        var text4 = document.getElementById('due_amount').value;
//        console.log("List of values : "+ text + text1 + text2 + text3);

        document.getElementById('amount_display').innerHTML = text;
        document.getElementById('service_tax_display').innerHTML = text1;
        document.getElementById('tot_amount_display').innerHTML = text2;
        document.getElementById('paid_amount_display').innerHTML = text3;
        document.getElementById('due_amount_display').innerHTML = text4;

        document.getElementById('printableArea').style.visibility = 'visible';

        var printContents = document.getElementById(divName).innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>