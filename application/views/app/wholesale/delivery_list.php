<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"><img id = "myImage" ></div>
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->

        <?php echo form_open('wholesale/payment');?>

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">DELIVERY LIST</a></li>
                        </ul>
                        <h4>DELIVERY LIST
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>
            <div class="col-md-12">
                <div class="col-md-1 ">
                    Invoice No.
                </div>
                <div class="col-md-2">
                    <input type="number" name="invoice_no" id="invoice_no" class="form-control eilmlitecontrols more">
                </div>

                <?php
                if($this->session->flashdata('messageError') != "")
                {
                    ?>

                    <div class="col-md-2">
                        <input type="checkbox" name="payment[]" onclick="paymentcheck(this)" value="1" <?php echo  set_radio('payment','1',TRUE); ?>/>&nbsp;only payment
                    </div>
                    <div class="col-md-2" id="prty_name_shw" style="display:block" >
                        <input type="text" placeholder="provide party name" name="party_name_prov"  />
                        <div><?php echo $this->session->flashdata('messageError'); ?></div>
                    </div>

                    <?php
                }else{ ?>

                    <div class="col-md-2">
                        <input type="checkbox" name="payment[]" onclick="paymentcheck(this)"/>&nbsp;only payment
                    </div>
                    <div class="col-md-2" id="prty_name_shw" style="display:none" >
                        <input type="text" placeholder="provide party name" name="party_name_prov"  />
                    </div>

                    <?php } ?>

                <div class="col-md-2">
                    <input type="submit" name="save_generate" id="save_generate" value="save & generate" />
                </div>
            </div>
            <br/>
            <div class="col-lg-12" style="height:400px;margin-bottom: 56px;overflow-y: scroll;">

                <table class="table" id="deliverylistdatatable">
                    <thead>
                    <tr>
                        <th class="hath" style="font-size: 11px"></th>
                        <th class="hath" style="font-size: 11px">ORDER NO</th>
                        <th class="hath" style="font-size: 11px">BILL NO</th>
                        <th class="hath" style="font-size: 11px">PARTY NAME</th>
                        <th class="hath" style="font-size: 11px">DATE</th>
                        <th class="hath" style="font-size: 11px">PRODUCT TYPE</th>
                        <th class="hath" style="font-size: 11px">PRODUCT NAME</th>
                        <th class="hath" style="font-size: 11px" >QUANTITY</th>
                        <th class="hath" style="font-size: 11px" >ORDER STATUS</th>
                        <th class="hath" style="font-size: 11px" ></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach($orderedlist as $val):?>

                        <tr style="font-size: 10px;">
                            <td><input type="checkbox" name="options[]" id="delivcheckboxs" value="<?php echo $val->item_id; ?>"></td>
                            <td><?php echo $val->sales_order_id;?></td>
                            <td><?php echo $val->bill_no;?></td>
                            <td><?php echo $val->party_name;?></td>
                            <td><?php echo date('Y-m-d', strtotime($val->order_date));?></td>
                            <td><?php echo $val->product_type?></td>
                            <td><?php echo $val->product_name?></td>
                            <td><?php echo $val->quantity?></td>
                            <td>
                                <select name="order_status" id="order_status">
                                    <option value="Delivered To Counter"<?php if($val->status == "Delivered To Counter"){?> selected="selected" <?php }?>>Delivered To Counter</option>
                                    <option value ="Delivered"<?php if($val->status == "Delivered"){?> selected="selected" <?php }?>>DELIVERED</option>
                                </select>
                            </td>
                            <td><input type="button" onclick="savedelivery(<?php echo $val->item_id;?>,'<?php echo $val->product_type;?>','<?php echo $val->product_name;?>','<?php echo $val->sph;?>','<?php echo $val->cyl;?>','<?php echo $val->axis;?>','<?php echo $val->addition;?>','<?php echo $val->quantity;?>')" value="save"></td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php form_close();?>
    </div>
</section>

<?php $this->load->view('layouts/footer'); ?>


<script>

</script>