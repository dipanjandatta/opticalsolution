<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"></div>
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">ORDERLIST</a></li>
                        </ul>
                        <h4>ORDERLIST
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>
            <?php
            if($this->session->flashdata('messageSuccess'))
            {
                ?>
                <!--            <div style="top:7px !important;">-->
                <script>
                    notif({
                        type: "success",
                        msg: '<?php echo $this->session->flashdata('messageSuccess'); ?>',
                        position: "right",
                        width: 520,
                        height: 60,
                        autohide: true,
                    });
                </script>
                <!--            </div>-->
                <?php
            }
            ?>

            <div class="col-lg-5">
                <div class="form-horizontal">

                    <?php echo form_open('wholesale/orderlist_save')?>

                    <?php if($this->uri->segment(3) != ""){ ?>

                        <div class="form-group ">
                            <label class="col-sm-4 control-label">ORDER NO</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="ord_no" value="<?php echo $sales_order_id?>" type="text" placeholder="ORDER NO" disabled>
                                <input type="hidden" class="form-control" value="<?php echo $id?>" name="order_no" />
                                <input type="hidden" class="form-control" value="<?php echo $item_id?>" name="item_id" />
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-sm-4 control-label">BILL NO</label>
                            <div class="col-sm-8">
                                <input class="form-control" value="<?php echo $bill_no?>"  type="text" placeholder="BILL NO" disabled>
                                <input type="hidden" class="form-control" name="bill_no" value="<?php echo $bill_no?>"/>
                            </div>
                        </div>
                        <input type="hidden" class="form-control" name="hidden_status" id="hidden_status" value="<?php echo $status?>"/>

                        <div class="form-group ">
                            <label class="col-sm-4 control-label">DATE</label>
                            <div class="col-sm-8">
                                <input name="date" id="date" value="<?php echo $date?>" class="form-control" type="date" placeholder="DATE" maxlength="30"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" >PARTY NAME</label>
                            <div class="col-sm-8">
                                <input name="party_name" id="party_name" class="form-control" value="<?php echo $party_name?>" type="text"  placeholder="PARTY NAME" maxlength="30"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" >SUPPLIER NAME</label>
                            <div class="col-sm-8">
                                <input name="supplier_name" id="supplier_name" class="form-control" value="<?php echo $supplier_name?>" type="text"  placeholder="SUPPLIER NAME" maxlength="30"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" >ORDER STATUS</label>
                            <div class="col-sm-8" >
                                <?php if($status == "Received"){ ?>
                                    <select class="form-control" name="order_status" id="order_status" >
                                        <option value="Received"<?php if($status == "Received"){?> selected="selected" <?php }?>>RECIEVED</option>
                                        <option value ="Delivered To Counter"<?php if($status == "Delivered To Counter"){?> selected="selected" <?php }?>>DELIVERED TO COUNTER</option>
                                    </select>
                                <?php }else{ ?>
                                    <select class="form-control" name="order_status" id="order_status">
                                        <option value="">SELECT</option>
                                        <option value="Not Ordered"<?php if($status == "Not Ordered"){?> selected="selected" <?php }?>>NOT ORDERED</option>
                                        <option value ="Ordered"<?php if($status == "Ordered"){?> selected="selected" <?php }?>>ORDERED</option>
                                        <option value ="Delivered To Counter"<?php if($status == "Delivered To Counter"){?> selected="selected" <?php }?>>Delivered To Counter</option>
                                        <option value ="Out Source"<?php if($status == "Out Source"){?> selected="selected" <?php }?>>OUT SOURCE</option>
                                    </select>
                               <?php } ?>
                            </div>
                        </div>


                        <div class="col-sm-12" id="ready" style="margin-left: 16px;">
                            <div class="panel panel-default panelFixer">
                                <div class="panel-heading panelBlue" style="text-align: center;">
                                    ------ITEM DETAILS------
                                </div>
                                <div class="panel-body formPadder">

                                    <div class="control-group form-group formFix">
                                        <div class="col-md-12 ">
                                            <div class="col-md-5">
                                                PRODUCT TYPE
                                            </div>
                                            <div class="col-md-5" >
                                                <input type="text" value="<?php echo $product_type?>" class="form-control eilmlitecontrols more" >
                                                <input type="hidden" name="prod_type" id="prod_type" value="<?php echo $product_type?>" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>

                                    </div>

                                    <?php if($product_type == "READY"){ ?>

                                        <div class="control-group form-group formFix">

                                            <div class="control-group form-group formFix" id="product_name" >
                                                <div class="col-md-12">
                                                    <div class="col-md-5">
                                                        PRODUCT NAME
                                                    </div>
                                                    <div class="col-md-7">
                                                        <input type="text" name="prod_name" id="prod_name" value="<?php echo $product_name?>" class="form-control eilmlitecontrols more">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="col-md-6">
                                                    SPH
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" value="<?php echo $sph?>" name="sph" id="sph" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    CYL
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" value="<?php echo $cyl?>" name="cyl" id="cyl" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix" style="display:block">
                                            <div class="col-md-6 ">
                                                <div class="col-md-6">
                                                    AXIS
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" value="<?php echo $axis?>" name="axis" id="axis" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    <select class="form-control" name="add_type" id="add_type" >
                                                        <option value="">SELECT </option>
                                                        <option value="NEAR ADD"<?php if($add_type == "NEAR ADD"){?> selected="selected" <?php }?>>NEAR ADD</option>
                                                        <option value="NEAR POWER"<?php if($add_type == "NEAR POWER"){?> selected="selected" <?php }?>>NEAR POWER</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" value="<?php echo $addition?>" name="addition" id="addition" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix" style="display:block">
                                            <div class="col-md-6 ">
                                                <div class="col-md-6">
                                                    SIDE
                                                </div>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="side" id="side" >
                                                        <option value="">SELECT SIDE</option>
                                                        <option value="RE"<?php if($side == "RE"){?> selected="selected" <?php }?>>RE</option>
                                                        <option value="LE"<?php if($side == "LE"){?> selected="selected" <?php }?>>LE</option>
                                                        <option value="BE"<?php if($side == "BE"){?> selected="selected" <?php }?>>BE</option>
                                                        <option value="NA"<?php if($side == "NA"){?> selected="selected" <?php }?>>N/A</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    QUANTITY
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="number" value="<?php echo $quantity?>" name="quantity" id="quantity" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>


                                    <?php }elseif($product_type == "RX"){ ?>


                                        <div class="control-group form-group formFix" id="product_name" >
                                            <div class="col-md-12">
                                                <div class="col-md-5">
                                                    PRODUCT NAME
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" name="prod_name" id="prod_name" value="<?php echo $product_name?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix" id="comp_name">
                                            <div class="col-md-12">
                                                <div class="col-md-5">
                                                    COMPANY NAME
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" name="comp_name" id="comp_name" value="<?php echo $company_name?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix" id="specification">
                                            <div class="col-md-12">
                                                <div class="col-md-5">
                                                    SPECIFICATION
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" name="specification" id="specification" value="<?php echo $specification?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix">
                                            <div class="col-md-6 ">
                                                <div class="col-md-6">
                                                    SPH
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="sph" id="sph" value="<?php echo $sph?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    CYL
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="cyl" id="cyl" value="<?php echo $cyl?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix" style="display:block">
                                            <div class="col-md-6 ">
                                                <div class="col-md-6">
                                                    AXIS
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="axis" id="axis" value="<?php echo $axis?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    <select class="form-control" name="add_type" id="add_type" >
                                                        <option value="">SELECT </option>
                                                        <option value="NEAR ADD"<?php if($add_type == "NEAR ADD"){?> selected="selected" <?php }?>>NEAR ADD</option>
                                                        <option value="NEAR POWER"<?php if($add_type == "NEAR POWER"){?> selected="selected" <?php }?>>NEAR POWER</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="addition" id="addition" value="<?php echo $addition?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="control-group form-group formFix"  style="display:block">
                                            <div class="col-md-6 ">

                                                <div class="col-md-6">
                                                    SIDE
                                                </div>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="side" id="side" >
                                                        <option value="">SELECT SIDE</option>
                                                        <option value="RE"<?php if($side == "RE"){?> selected="selected" <?php }?>>RE</option>
                                                        <option value="LE"<?php if($side == "LE"){?> selected="selected" <?php }?>>LE</option>
                                                        <option value="BOTH"<?php if($side == "BOTH"){?> selected="selected" <?php }?>>BOTH</option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    QUANTITY
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="number" name="quantity" id="quantity" value="<?php echo $quantity?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>

                                    <?php }elseif($product_type == "GRINDING"){ ?>

                                        <div class="control-group form-group formFix" id="product_name" >
                                            <div class="col-md-12">
                                                <div class="col-md-5">
                                                    PRODUCT NAME
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" name="prod_name" id="prod_name" value="<?php echo $product_name?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix">
                                            <div class="col-md-6 ">
                                                <div class="col-md-6">
                                                    SPH
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="sph" id="sph" value="<?php echo $sph?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    CYL
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="cyl" id="cyl" value="<?php echo $cyl?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="control-group form-group formFix" >
                                            <div class="col-md-6 ">
                                                <div class="col-md-6">
                                                    AXIS
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="axis" id="axis" value="<?php echo $axis?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    <select class="form-control" name="add_type" id="add_type" >
                                                        <option value="">SELECT </option>
                                                        <option value="NEAR ADD"<?php if($add_type == "NEAR ADD"){?> selected="selected" <?php }?>>NEAR ADD</option>
                                                        <option value="NEAR POWER"<?php if($add_type == "NEAR POWER"){?> selected="selected" <?php }?>>NEAR POWER</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="addition" id="addition" value="<?php echo $addition?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="control-group form-group formFix">
                                            <div class="col-md-6 ">
                                                <div class="col-md-6">
                                                    DIAMETER
                                                </div>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="dia" id="dia">
                                                        <option value="">SELECT</option>
                                                        <option value="60"<?php if($diameter == "60"){?> selected="selected" <?php }?>>60 X 3 TO 8</option>
                                                        <option value ="65"<?php if($diameter == "65"){?> selected="selected" <?php }?>>65 X 3 TO 8</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    BASE
                                                </div>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="base" id="base">
                                                        <option value="">SELECT</option>
                                                        <option value="3"<?php if($base == "3"){?> selected="selected" <?php }?>>3</option>
                                                        <option value ="4"<?php if($base == "4"){?> selected="selected" <?php }?>>4</option>
                                                        <option value ="5"<?php if($base == "5"){?> selected="selected" <?php }?>>5</option>
                                                        <option value ="6"<?php if($base == "6"){?> selected="selected" <?php }?>>6</option>
                                                        <option value ="8"<?php if($base == "8"){?> selected="selected" <?php }?>>8</option>
                                                        <option value ="10"<?php if($base == "10"){?> selected="selected" <?php }?>>10</option>
                                                        <option value ="12"<?php if($base == "12"){?> selected="selected" <?php }?>>12</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="control-group form-group formFix" id="side_qty">
                                            <div class="col-md-6 ">

                                                <div class="col-md-6">
                                                    SIDE
                                                </div>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="side" id="side" >
                                                        <option value="">SELECT SIDE</option>
                                                        <option value="RE"<?php if($side == "RE"){?> selected="selected" <?php }?>>RE</option>
                                                        <option value="LE"<?php if($side == "LE"){?> selected="selected" <?php }?>>LE</option>
                                                        <option value="BOTH"<?php if($side == "BOTH"){?> selected="selected" <?php }?>>BOTH</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    QUANTITY
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="quantity" id="quant1" value="<?php echo $quantity; ?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>


                                    <?php }elseif($product_type == "SOLUTION"){ ?>

                                        <div class="control-group form-group formFix" id="product_name" >
                                            <div class="col-md-12">
                                                <div class="col-md-5">
                                                    PRODUCT NAME
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" name="prod_name" value="<?php echo $product_name?>" id="prod_name" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix">
                                            <div class="col-md-12">
                                                <div class="col-md-5">
                                                    QUANTITY
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="number" name="quantity" id="quantity" value="<?php echo $quantity?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>

                                    <?php }elseif($product_type == "FITTING"){ ?>

                                        <div class="control-group form-group formFix" id="frame" >
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    FRAME
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" name="frame_dup" id="frame_dup" value="<?php echo $frame?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="control-group form-group formFix" id="lens" >
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    LENS
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" name="lens_dup" id="lens_dup" value="<?php echo $lens?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="control-group form-group formFix" id="quantity">
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    QUANTITY
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" name="quantity" id="quant2" value="<?php echo $quantity?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>


                                    <?php }elseif($product_type == "ACCESSORY"){ ?>

                                        <div class="control-group form-group formFix" id="product_name" >
                                            <div class="col-md-12">
                                                <div class="col-md-5">
                                                    PRODUCT NAME
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" name="prod_name" value="<?php echo $product_name?>" id="prod_name" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="control-group form-group formFix">
                                            <div class="col-md-12">
                                                <div class="col-md-5">
                                                    QUANTITY
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="number" name="quantity" id="quantity" value="<?php echo $quantity?>" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>


                                    <?php }else{ ?>

                                        <div class="control-group form-group formFix">
                                            <div class="col-md-6 ">
                                                <div class="col-md-6">
                                                    SPH
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" value="<?php echo $sph?>" name="sph" id="sph" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    CYL
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" value="<?php echo $cyl?>" name="cyl" id="cyl" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix" style="display:block">
                                            <div class="col-md-6 ">
                                                <div class="col-md-6">
                                                    AXIS
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" value="<?php echo $axis?>" name="axis" id="axis" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    <select class="form-control" name="add_type" id="add_type" >
                                                        <option value="">SELECT </option>
                                                        <option value="NEAR ADD"<?php if($add_type == "NEAR ADD"){?> selected="selected" <?php }?>>NEAR ADD</option>
                                                        <option value="NEAR POWER"<?php if($add_type == "NEAR POWER"){?> selected="selected" <?php }?>>NEAR POWER</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" value="<?php echo $addition?>" name="addition" id="addition" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group form-group formFix" style="display:block">
                                            <div class="col-md-6 ">
                                                <div class="col-md-6">
                                                    QUANTITY
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="number" value="<?php echo $quantity?>" name="quantity" id="quantity" class="form-control eilmlitecontrols more">
                                                </div>
                                            </div>
                                        </div>


                                    <?php } ?>

                                    <div class="control-group form-group formFix" style="display:block">
                                        <div class="col-md-12 ">
                                            <div class="col-md-4">
                                                PENDING QTY
                                            </div>
                                            <div class="col-md-3">
                                                <input type="number" value="<?php echo $pending_quantity?>" name="pending_quantity" id="pending_quantity" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix" id="remarks">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                REMARKS
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" name="remarks_dup" value="<?php echo $remarks; ?>" id="remarks_dup" class="form-control eilmlitecontrols more">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group form-group formFix" style="display:block">
                                        <div class="col-md-12 ">
                                            <div class="col-md-6">
                                                <input  type="button" value="ADD PRODUCT"  id="add_prod" name="add_prod" onclick="addprod('<?php echo $sales_order_id;?>','<?php echo $bill_no;?>','<?php echo $id;?>')" class="form-control eilmlitecontrols more" style="background: #286090;color: white;" disabled>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="pull-right" style="margin-bottom: 68px;">
                            <input type="submit" name="save" id="save_orderlist"class="btn btn-primary" id="btnsave" value="Save"/>
                            <input type="submit" class="btn btn-primary" id="btnCancelComp" value="Cancel"/>
                        </div>


                    <?php }else{ ?>


                        <?php echo form_close();?>


                        <div class="form-group ">
                            <label class="col-sm-4 control-label">ORDER NO</label>
                            <div class="col-sm-8">
                                <input class="form-control"  name="order_no" type="text" placeholder="ORDER NO" disabled>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-sm-4 control-label">D.C NO</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="bill_no"  type="text" placeholder="BILL NO" disabled>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label class="col-sm-4 control-label">DATE</label>
                            <div class="col-sm-8">
                                <input name="date" class="form-control" type="date" placeholder="DATE" maxlength="30" disabled/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label" >PARTY NAME</label>
                            <div class="col-sm-8">
                                <input name="party_name" class="form-control"  type="text" placeholder="PARTY NAME" maxlength="30" disabled/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label" >ORDER STATUS</label>
                            <div class="col-sm-8">
                                <select class="form-control" disabled>
                                    <option value="">SELECT</option>
                                </select>
                            </div>
                        </div>


                        <div class="col-sm-12" id="ready" style="margin-left: 16px;">
                            <div class="panel panel-default panelFixer">
                                <div class="panel-heading panelBlue" style="text-align: center;">
                                    ------ITEM DETAILS------
                                </div>
                                <div class="panel-body formPadder">

                                    <div class="control-group form-group formFix">
                                        <div class="col-md-12 ">
                                            <div class="col-md-3">
                                                PRODUCT TYPE
                                            </div>
                                            <div class="col-md-5">
                                                <select class="form-control" disabled>
                                                    <option value="">SELECT TYPE</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="control-group form-group formFix">
                                        <div class="col-md-6 ">
                                            <div class="col-md-6">
                                                SPH
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="sph" id="sph" class="form-control eilmlitecontrols more" disabled />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                                CYL
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="cyl" id="cyl" class="form-control eilmlitecontrols more" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-6 ">
                                            <div class="col-md-6">
                                                AXIS
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="axis" id="axis"  class="form-control eilmlitecontrols more" disabled />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                                ADDITION
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="addition" id="addition" class="form-control eilmlitecontrols more" disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group form-group formFix">
                                        <div class="col-md-6 ">
                                            <div class="col-md-6">
                                                QUANTITY
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="quantity" id="quantity" class="form-control eilmlitecontrols more" disabled />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pull-right" style="margin-bottom: 68px;">
                            <input type="submit" name="save" class="btn btn-primary" id="btnsave" value="Save" disabled/>
                            <input type="submit" class="btn btn-primary" id="btnCancelComp" value="Cancel"/>
                        </div>

                    <?php } ?>

                </div>
            </div>

            <div class="col-lg-7" >
                <div class="medium no-padding searchpositioning"  style="overflow-y: auto;">
                    <div >
                       <table class="table" id="orderlistdatatable">
                                <thead>
                                <tr>
                                    <th >&nbsp;</th>
                                    <th class="hath" style="font-size: 11px">ORDER NO</th>
                                    <th class="hath" style="font-size: 11px">DC NO</th>
                                    <th class="hath" style="font-size: 11px">DATE</th>
                                    <th class="hath" style="font-size: 11px">PARTY NAME</th>
                                    <th class="hath" style="font-size: 11px" >PRODUCT TYPE</th>
                                    <th class="hath" style="font-size: 11px" >ITEM DETAILS</th>

                                </tr>
                                </thead>
                                <tbody >
                                <?php foreach($orderlistdata as $val):?>

                                    <tr style="font-size: 10px;">

                                        <td><?php
                                            if($this->uri->segment(3) != $val->item_id){
                                                ?>
                                                <input type="radio" class="ordlist" name="ordlist" id="ordlist" onclick="getvalue(<?php echo $val->item_id;?>)">
                                                <?php
                                            }
                                            else{
                                                ?>
                                                <input type="radio" class="ordlist" name="ordlist" id="ordlist" onclick="getvalue(<?php echo $val->item_id;?>)" checked="checked">
                                                <?php
                                            }
                                            ?>
                                        </td>

                                        <td><?php echo $val->sales_order_id;?></td>
                                        <td><?php echo $val->bill_no;?></td>
                                        <td><?php echo date('Y-m-d', strtotime($val->order_date));?></td>
                                        <td><?php echo $val->party_name;?></td>
                                        <td><?php echo $val->product_type;?></td>
                                        <?php if($val->product_type == "READY"){?>
                                        <td>sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->cyl;?>&nbsp;add:<?php echo $val->cyl;?></td>
                                        <?php }elseif($val->product_type == "RX"){?>
                                            <td>sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->cyl;?>&nbsp;add:<?php echo $val->cyl;?></td>
                                        <?php }elseif($val->product_type == "FITTING"){?>
                                            <td>sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->cyl;?>&nbsp;add:<?php echo $val->cyl;?></td>
                                        <?php }else{?>
                                            <td>sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->cyl;?>&nbsp;add:<?php echo $val->cyl;?></td>
                                        <?php } ?>
                                    </tr>

                                <?php endforeach;?>

                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php $this->load->view('layouts/footer'); ?>

</section>

<script type="text/javascript">
    function printDiv(divName) {
        document.getElementById('printableArea').style.visibility = 'visible';
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>