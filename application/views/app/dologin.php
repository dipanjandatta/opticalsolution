<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php //$this->load->view('layouts/header'); ?>


<div class="container padTopBody">
    <div class="row">
        <div class="col-md-12 noPad">
            <div class="col-md-6 centerAlign noPad">
                <img src="./images/sublogo.png" class="subLogo" />
                <span><h3 style="color: white;margin-top: 0px;">EILM CLOUD</h3></span>
                <hr class="dhlhr">
                <span><div style="color: #f8980a;margin-top: 0px;">Transportation & Logistics</div></span>
            </div>
            <form method="post">
                <div class="col-md-4 col-sm-offset-1">
                    <div class="panel panel-default noBord">
                        <div class="panel-heading panelLogin remwhitebord">
                            Login Credentials
                        </div>
                        <div class="panel-body panCol" style="background: #D0D4D5 !important;     border: none;">
                            <div class="control-group form-group">
                                <div class="controls">
                                    <input type="text" class="form-control dhlControls" placeholder="Username" ng-model="user.username">
                                </div>
                            </div>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <input type="password" class="form-control dhlControls" placeholder="Password" ng-model="user.password">
                                </div>
                            </div>
                            <div class="control-group form-group buttonAlignment">
                                <button type="reset" class="btn btn-warning modwarn" ng-click="cancel();">CANCEL</button>

                                <button type="submit" class="btn btn-success modsucc" ng-click="submitlogin()">SUBMIT</button>
                            </div>
                            <div class="control-group form-group">&nbsp;</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
