<?php $this->load->view('assets/css'); ?>

<head>
    <style>
        a:hover {
            background-color: yellow;
        }
    </style>
</head>

<?php $this->load->view('layouts/header'); ?>
<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <?php $this->load->view('layouts/main'); ?>
        </div><!-- leftpanel -->
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">SALES STAFF</a></li>
                        </ul>
                        <h4>SALES STAFF
                        </h4>
                    </div>
                </div><!-- media -->
            </div>

            <br/>
            <div class="row ng-scope">

                <div class="col-lg-6">
                    <srd-widget>
                        <div class="widget" >
                            <srd-widget-header icon="fa-tasks" title="Servers" class="ng-scope ng-isolate-scope">
                            </srd-widget-header>
                            <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope">
                                <div ng-hide="loading" class="widget-content" >
                                    <div class=" ng-scope form-horizontal">

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">CUSTOMER ID</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="CMCompanyName" placeholder="CUSTOMAR ID" maxlength="30" ng-model="sales_data.customer_id" readonly/>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">REFERENCE ID</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="CMCompanyName" placeholder="REFERENCE ID" maxlength="30" ng-model="sales_data.reference_id" readonly />
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <!--ol-sm-4 control-label">POWER LENS</label>-->
                                            <div class="col-sm-8">
                                                <table class="tatble_style">
                                                    <tr class="row_style">
                                                        <td style="border: solid #000000 1px;padding: 0 !important;" colspan="2"><b>LENS MEASUREMENT</b></td>
                                                        <!--<td style="border: solid #000000 2px"></td>-->
                                                        <td class="column_style" style="padding: 0 !important;">SPH</td>
                                                        <td class="column_style" style="padding: 0 !important;">CYL</td>
                                                        <td class="column_style" style="padding: 0 !important;">AXIS</td>
                                                        <td class="column_style" style="padding: 0 !important;">V/A</td>
                                                    </tr>
                                                    <tr class="row_style">
                                                        <td class="column_style" rowspan="2">RIGHT EYE</td>
                                                        <td class="column_style">DISTANCE</td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.right_dis_ds"  readonly/></td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.right_dis_cyl" readonly/></td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.right_dis_axis" readonly/></td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.right_dis_va"  readonly/></td>
                                                    </tr>

                                                    <tr class="row_style">
                                                        <!--<td style="border: solid #000000 2px"></td>-->
                                                        <td class="column_style">NEAR ADD</td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.right_near_ds" readonly/></td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.right_near_cyl" readonly/></td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.right_near_axis" readonly/></td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.right_near_va" readonly/></td>
                                                    </tr>

                                                    <tr class="row_style">
                                                        <td class="column_style" rowspan="2">LEFT EYE</td>
                                                        <td class="column_style">DISTANCE</td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.left_dis_ds" readonly/></td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.left_dis_cyl" readonly/></td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.left_dis_axis" readonly/></td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.left_dis_va" readonly/></td>
                                                    </tr>
                                                    <tr class="row_style">
                                                        <td class="column_style">NEAR ADD</td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.left_near_ds" readonly/></td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.left_near_cyl" readonly/></td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.left_near_axis" readonly/></td>
                                                        <td class="column_style"><input type="text" class="input_column" ng-model="sales_data.left_near_va" readonly/></td>
                                                    </tr>

                                                </table>
                                            </div>
                                        </div>

                                        <hr/>

                                        <div>
                                            <p style="text-align: center;color: red;"><u>FRAME</u></p>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">TYPE OF FRAME</label>
                                            <div class="col-sm-8">
                                                <select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle" ng-model="sales_data.frame_type">
                                                    <option value="" selected="selected">SELECT TYPE OF FRAME</option>
                                                    <option value="FULL">FULL</option>
                                                    <option value="SUPRA">SUPRA</option>
                                                    <option value="3 PC">3 PC</option>
                                                    <option value="OWN FULL">OWN FULL</option>
                                                    <option value="OWN SUPRA">OWN SUPRA</option>
                                                    <option value="OWN 3 PC">OWN 3 PC</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">FRAME CODE</label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="text" id="CMDisplayName" placeholder="FRAME CODE" ng-model="sales_data.frame_code"/>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">COMPANY</label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="text" id="CMDisplayName" placeholder=" FRAME COMPANY" ng-model="sales_data.frame_company"/>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">AMOUNT</label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="text" id="CMDisplayName" placeholder="AMOUNT" ng-model="sales_data.frame_amount" ng-blur="total_amount_cal_frame()"/>
                                            </div>
                                        </div>
                                        <hr/>

                                        <!--<hr/>-->
                                        <!--<div class="form-group ">-->
                                        <!--<label class="col-sm-4 control-label">TYPE OF FRAME</label>-->
                                        <!--<div class="col-sm-8">-->
                                        <!--<select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle" ng-model="sales_data.frame_type">-->
                                        <!--<option value="" selected="selected">SELECT TYPE OF FRAME</option>-->
                                        <!--<option value="FULL">FULL</option>-->
                                        <!--<option value="SUPRA">SUPRA</option>-->
                                        <!--<option value="3 PC">3 PC</option>-->
                                        <!--<option value="OWN FULL">OWN FULL</option>-->
                                        <!--<option value="OWN SUPRA">OWN SUPRA</option>-->
                                        <!--<option value="OWN 3 PC">OWN 3 PC</option>-->
                                        <!--</select>-->
                                        <!--</div>-->
                                        <!--</div>-->

                                        <!--<div class="form-group ">-->
                                        <!--<label class="col-sm-4 control-label">FRAME CODE</label>-->
                                        <!--<div class="col-sm-8">-->
                                        <!--<input  class="form-control" type="text" id="CMDisplayName" placeholder="FRAME CODE" ng-model="sales_data.frame_code"/>-->
                                        <!--</div>-->
                                        <!--</div>-->

                                        <div>
                                            <p style="text-align: center;color: red;"><u>LENS</u></p>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">TYPE OF LENS</label>
                                            <div class="col-sm-8">
                                                <select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle" ng-model="sales_data.lens_type">
                                                    <option value="" selected="selected">SELECT TYPE OF LENS</option>
                                                    <option value='SINGLE VISION'>SINGLE VISION</option>
                                                    <option value="SINGLE VISION ARC">SINGLE VISION ARC</option>
                                                    <option value="SINGLE VISION ARC PG">SINGLE VISION ARC PG</option>
                                                    <option value="BIFOD">BIFOD</option>
                                                    <option value="BIFOD ARC">BIFOD ARC</option>
                                                    <option value="BIFOD ARC PG">BIFOD ARC PG</option>
                                                    <option value="PROGRESSIVE">PROGRESSIVE</option>
                                                    <option value="DIGITAL PROGRESSIVE">DIGITAL PROGRESSIVE</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">LENS COMPANY</label>
                                            <div class="col-sm-8">
                                                <select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle" ng-model="sales_data.lens_company"  ng-change="stockchange()">
                                                    <option value="" selected="selected">SELECT COMPANY</option>
                                                    <option value='GKB'>GKB</option>
                                                    <option value="SLR">SLR</option>
                                                    <option value="OTHER">OTHER</option>
                                                    <option value="OWN STOCK">OWN STOCK</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">SPECIFICATION</label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="text" id="CMDisplayName" placeholder="LENS SPECIFICATION" ng-model="sales_data.lens_specification" ng-disabled="stockmaintain"/>
                                            </div>
                                        </div>

                                        <!--<div class="form-group ">-->
                                        <!--<label class="col-sm-4 control-label">SPECIFICATION</label>-->
                                        <!--<div class="col-sm-8">-->
                                        <!--<select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle" ng-model="sales_data.lens_specification">-->
                                        <!--<option value="" selected="selected">SELECT SPECIFICATION</option>-->
                                        <!--<option value='TRANSITION CRIZAL A2 BROWN' >TRANSITION CRIZAL A2 BROWN</option>-->
                                        <!--<option value='TRANSITION CRIZAL A2 GRAY'>TRANSITION CRIZAL A2 GRAY</option>-->
                                        <!--<option value="XTERLITE SATIN">XTERLITE SATIN</option>-->
                                        <!--<option value="XTERLITE SHMC">XTERLITE SHMC</option>-->
                                        <!--<option value="XTERLITE TC TITENUM COAT">XTERLITE TC TITENUM COAT</option>-->
                                        <!--<option value="RGLR SV HC">RGLR SV HC</option>-->
                                        <!--<option value="RGLR SV ARC">RGLR SV ARC</option>-->
                                        <!--<option value="RGLR SV UC">RGLR SV UC</option>-->
                                        <!--</select>-->
                                        <!--</div>-->
                                        <!--</div>-->


                                        <div class="form-group">
                                            <div class="col-sm-8">
                                                <label style="margin-left: 100px"><a href="" data-toggle="modal" data-target="#exampleModal1" data-whatever="@mdo" ><input type="button" ng-disabled="stockmaintain" value="Lens Avaibility" /></a>
                                                    <!--<input type="button" value="Get Lens Price" ng-click="get_lens_price()" style="margin-left: 20px"/>-->
                                                </label>

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">LENS PRICE</label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="text" id="CMDisplayName" placeholder="LENS PRICE" ng-model="sales_data.lens_amount" ng-blur="total_amount_cal_lens()"/>
                                            </div>
                                        </div>
                                        <hr/>

                                        <div>
                                            <p style="text-align: center;color: red;"><u>CONTACT LENS</u></p>
                                        </div>
                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">COMPANY</label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="text" id="CMDisplayName" placeholder=" FRAME COMPANY" ng-model="sales_data.contact_lens_company"/>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">AMOUNT</label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="text" id="CMDisplayName" placeholder="AMOUNT" ng-model="sales_data.contact_lens_amount" ng-blur="total_amount_cal_contact_lens()"/>
                                            </div>
                                        </div>
                                        <hr/>






                                        <div>
                                            <p style="text-align: center;color: red;"><u>OTHER ITEM</u></p>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">ITEM NAME</label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="text" id="CMDisplayName" placeholder="ITEM NAME" ng-model="sales_data.other_item_name"/>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">SPECIFICATION</label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="text" id="CMDisplayName" placeholder="SPECIFICATION" ng-model="sales_data.other_item_specification"/>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">AMOUNT</label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="text" id="CMDisplayName" placeholder="AMOUNT" ng-model="sales_data.other_item_amount" ng-change="other_item_cal()"/>
                                            </div>
                                        </div>



                                        <hr/>


                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">OTHER SERVICES</label>
                                            <div class="col-sm-8">
                                                <select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle" ng-model="sales_data.other_services" ng-change="other_services(sales_data.other_services)">
                                                    <option value="" selected="selected">Other Services</option>

                                                    <option value="yes">YES</option>
                                                    <option value="no">NO</option>

                                                </select>
                                            </div>
                                        </div>

                                        <div ng-show="data_show">
                                            <div class="form-group ">
                                                <label class="col-sm-4 control-label">FITTING&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;<input  type="checkbox" ng-model="sales_data.fitting_charge"/></label>
                                                <label class="col-sm-4 control-label">COLOURING&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;<input type="checkbox" ng-model="sales_data.colouring_charge"/></label>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">TOTAL AMOUNT</i></label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="text" id="CMDisplayName" placeholder="TOTAL AMOUNT" ng-model="sales_data.total_amount"/>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">VAT %</label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="text" id="CMDisplayName" placeholder="VAT %" ng-model="sales_data.vat_percentage" ng-blur="vat_cal(sales_data.vat_percentage)"/>
                                            </div>
                                        </div>

                                        <!--<div class="form-group ">-->
                                        <!--<form>-->
                                        <!--<label class="col-sm-4 control-label"><input type="radio" ng-model="sales_data.vat_status" value="yes" ng-change="vat(sales_data.vat_status)"> WITH 4% VAT</label>-->

                                        <!--<label class="col-sm-4 control-label"><input type="radio" ng-model="sales_data.vat_status" value="no" ng-change="vat(sales_data.vat_status)"> WITHOUT VAT</label>-->
                                        <!--</form>-->
                                        <!--</div>-->

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">DISCOUNT</label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="text" id="CMDisplayName" placeholder="DISCOUNT" ng-model="sales_data.discount" ng-blur="discount_cal(sales_data.discount)"/>
                                            </div>
                                        </div>



                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">TOTAL CHARGE</label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="text" id="CMDisplayName" placeholder="TOTAL CHARGE" ng-model="sales_data.total_charge"/>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">DELIVERY DATE</label>
                                            <div class="col-sm-8">
                                                <input  class="form-control" type="date" id="CMDisplayName" placeholder="DELIVERY DATE" ng-model="sales_data.delivery_date"/>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" >DELIVERY STATUS</label>
                                            <div class="col-sm-8">
                                                <select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle" ng-model="sales_data.delivery_status" ng-disabled="delivery_status_disable">
                                                    <option value="" selected="selected">SELECT STATUS</option>
                                                    <option value="NOT DELIVERED">NOT DELIVERED</option>
                                                    <option value="PARTIAL DELIVERED">PARTIAL DELIVERED</option>
                                                    <option value="DELIVERED">DELIVERED</option>
                                                    <option value="CANCELLED">CANCELLED</option>
                                                </select>
                                            </div>

                                            <br/>
                                            <div>
                                                <input type = 'button' class="btn btn-primary" value="Update Delivery Status" style='margin-left: 66%' ng-click="update_delivery(sales_data)"  ng-disabled="delivery_status_disable">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" >BACK OFFICE STATUS</label>
                                            <div class="col-sm-8">
                                                <select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle" ng-model="sales_data.backup_status" ng-disabled="true">
                                                    <option value="" selected="selected">SELECT STATUS</option>
                                                    <option value="IN PROGRESS">IN PROGRESS</option>
                                                    <option value="PARTIAL DELIVERED IN COUNTER">PARTIAL DELIVERED IN COUNTER</option>
                                                    <option value="DELIVERED IN COUNTER">DELIVERED IN COUNTER</option>
                                                    <option value="OUT STATION ORDER">OUT STATION ORDER</option>
                                                    <option value="CANCELLED">CANCELLED</option>
                                                </select>
                                            </div>


                                        </div>

                                        <div class="pull-right" >
                                            <input type="button" class="btn btn-primary" id="btnsave1" ng-click="save_sales_data()" value="Save" ng-disabled="pay_save_disable"/>
                                            <!--<input type="button" class="btn btn-primary" id="btnsave1" ng-click="save_sales_data(sales_data)" value="Save" ng-disabled="pay_save_disable"/>-->
                                            <input type="button" class="btn btn-primary" id="btnCancelComp" value="Cancel" ng-click="cancel_sales_data()"/>
                                            <a href="#/main/payment/{{sales_data.customer_id}}"><input type="button" class="btn btn-primary" id="btnsave1" value="Payment Page" ng-disabled="payment_disable"/></a>
                                        </div>
                                    </div>
                                </div>
                            </srd-widget-body>

                        </div>
                    </srd-widget>
                </div>

                <div class="col-lg-6" >
                    <srd-widget>
                        <div class="widget" >


                            <div class="widget" >
                                <srd-widget-header icon="fa-tasks" title="Servers" class="ng-scope ng-isolate-scope">

                                </srd-widget-header>
                            </div>

                            <srd-widget-header icon="fa-tasks" title="Striped Servers" class="ng-scope ng-isolate-scope">
                                <div class="widget-header ng-binding">
                                    <div class="input-group">
                                        <input type="text" class="form-control" ng-model="searchBy" placeholder="Search" >
                                        <span class="input-group-addon" style="cursor: pointer">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </srd-widget-header>
                            <hr/>

                            <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope" >
                                <div class="widget-body medium no-padding" id="gridscroll" style="height: 425px" ng-class="classes">

                                    <div  class="widget-content" style="height: 450px; overflow: scroll;">
                                        <div>
                                            <table class="table fontsizestyle"><thead>
                                                <tr>
                                                    <th >&nbsp;</th>
                                                    <th class="hath" ng-click="predicate = 'customer_id'; reverse=!reverse">ID</th>
                                                    <th class="hath" ng-click="predicate = 'customer_name'; reverse=!reverse">NAME</th>
                                                    <th class="hath" ng-click="predicate = 'reference_id'; reverse=!reverse">REFERENCE ID</th>
                                                    <th class="hath" ng-click="predicate = 'patient_status'; reverse=!reverse">PATIENT STATUS</th>
                                                    <th class="hath" ng-click="predicate = 'payment_status'; reverse=!reverse">PAYMENT STATUS</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr ng-repeat="sal in sales_list | filter: searchBy | orderBy:predicate:reverse">
                                                    <td class="text-center"><input type="radio" name="id" ng-click="pop_sales_data(sal);"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </srd-widget-body>
                        </div>
                    </srd-widget>
                </div>
            </div>
        </div>
        <?php $this->load->view('layouts/footer'); ?>

    </div>
</section>


<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="close_pop_up();"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel1">Lens Avaibility Chart</h4>
            </div>
            <div class="modal-body row">

                <div class="widget" >
                    <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope">
                        <div class="widget-body medium no-padding" style="height: 300px" ng-class="classes">
                            <rd-loading ng-show="loading" class="ng-hide">
                                <div class="loading">
                                    <div class="double-bounce1"></div>
                                    <div class="double-bounce2"></div>
                                </div>
                            </rd-loading>

                            <table cellspacing="0 !impotant" border="0">
                                <colgroup span="17" width="86"></colgroup>

                                <tr>
                                    <td class="lensaviblty" height="21" align="center"><b><font face="Calibri" color="#000000">MINUS SPH/MINUS CYL</font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle><b><font face="Calibri" color="#000000">0 CYL</font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)0.25 CYL </font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)0.50 CYL </font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)0.75 CYL </font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)1.00 CYL </font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)1.25 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)1.50 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)1.75 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)2.00 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)2.25 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)2.50 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)3.00 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)3.50 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)4.00 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)4.50 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)5.00 CYL </font></b></td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="21" align="center"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="21" align="center" sdval="0.25" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><font face="Calibri" color="#000000"> 0.25 </font></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><font face="Calibri" color="#000000"><br></font></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="17.5" sdnum="1033;">17.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="0" sdnum="1033;">0</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="16.5" sdnum="1033;">16.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="20" sdnum="1033;">20</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="4" sdnum="1033;">4</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="5" sdnum="1033;">5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="9" sdnum="1033;">9</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="8" sdnum="1033;">8</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="3" sdnum="1033;">3</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="2.5" sdnum="1033;">2.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="3.5" sdnum="1033;">3.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1" sdnum="1033;">1</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1" sdnum="1033;">1</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="0" sdnum="1033;">0</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="0" sdnum="1033;">0</td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="21" align="center" sdval="0.5" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><font face="Calibri" color="#000000"> 0.50 </font></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><font face="Calibri" color="#000000"><br></font></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="8.5" sdnum="1033;">8.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="18" sdnum="1033;">18</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="14" sdnum="1033;">14</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="20" sdnum="1033;">20</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="3" sdnum="1033;">3</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="0" sdnum="1033;">0</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="9.5" sdnum="1033;">9.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="6.5" sdnum="1033;">6.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1.5" sdnum="1033;">1.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1" sdnum="1033;">1</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="2.5" sdnum="1033;">2.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1.5" sdnum="1033;">1.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1" sdnum="1033;">1</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="2.5" sdnum="1033;">2.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="2" sdnum="1033;">2</td>
                                </tr>
                                <tr>
                                    <td class="lensaviblty" height="21" align="center"><b><font face="Calibri" color="#000000">MINUS SPH/MINUS CYL</font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle><b><font face="Calibri" color="#000000">0 CYL</font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)0.25 CYL </font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)0.50 CYL </font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)0.75 CYL </font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)1.00 CYL </font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)1.25 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)1.50 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)1.75 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)2.00 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)2.25 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)2.50 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)3.00 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)3.50 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)4.00 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)4.50 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)5.00 CYL </font></b></td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="21" align="center"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    iiiii                                <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="21" align="center" sdval="0.25" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><font face="Calibri" color="#000000"> 0.25 </font></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><font face="Calibri" color="#000000"><br></font></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="17.5" sdnum="1033;">17.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="0" sdnum="1033;">0</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="16.5" sdnum="1033;">16.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="20" sdnum="1033;">20</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="4" sdnum="1033;">4</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="5" sdnum="1033;">5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="9" sdnum="1033;">9</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="8" sdnum="1033;">8</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="3" sdnum="1033;">3</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="2.5" sdnum="1033;">2.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="3.5" sdnum="1033;">3.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1" sdnum="1033;">1</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1" sdnum="1033;">1</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="0" sdnum="1033;">0</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="0" sdnum="1033;">0</td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="21" align="center" sdval="0.5" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><font face="Calibri" color="#000000"> 0.50 </font></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><font face="Calibri" color="#000000"><br></font></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="8.5" sdnum="1033;">8.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="18" sdnum="1033;">18</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="14" sdnum="1033;">14</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="20" sdnum="1033;">20</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="3" sdnum="1033;">3</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="0" sdnum="1033;">0</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="9.5" sdnum="1033;">9.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="6.5" sdnum="1033;">6.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1.5" sdnum="1033;">1.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1" sdnum="1033;">1</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="2.5" sdnum="1033;">2.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1.5" sdnum="1033;">1.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1" sdnum="1033;">1</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="2.5" sdnum="1033;">2.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="2" sdnum="1033;">2</td>
                                </tr>
                                <tr>
                                    <td class="lensaviblty" height="21" align="center"><b><font face="Calibri" color="#000000">MINUS SPH/MINUS CYL</font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle><b><font face="Calibri" color="#000000">0 CYL</font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)0.25 CYL </font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)0.50 CYL </font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)0.75 CYL </font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)1.00 CYL </font></b></td>
                                    <td class="lensaviblty" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)1.25 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)1.50 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)1.75 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)2.00 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)2.25 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)2.50 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)3.00 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)3.50 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)4.00 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)4.50 CYL </font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"> (-)5.00 CYL </font></b></td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="21" align="center"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><b><font face="Calibri" color="#000000"><br></font></b></td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="21" align="center" sdval="0.25" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><font face="Calibri" color="#000000"> 0.25 </font></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><font face="Calibri" color="#000000"><br></font></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="17.5" sdnum="1033;">17.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="0" sdnum="1033;">0</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="16.5" sdnum="1033;">16.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="20" sdnum="1033;">20</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="4" sdnum="1033;">4</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="5" sdnum="1033;">5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="9" sdnum="1033;">9</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="8" sdnum="1033;">8</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="3" sdnum="1033;">3</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="2.5" sdnum="1033;">2.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="3.5" sdnum="1033;">3.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1" sdnum="1033;">1</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1" sdnum="1033;">1</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="0" sdnum="1033;">0</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="0" sdnum="1033;">0</td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="21" align="center" sdval="0.5" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><font face="Calibri" color="#000000"> 0.50 </font></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)"><font face="Calibri" color="#000000"><br></font></td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="8.5" sdnum="1033;">8.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="18" sdnum="1033;">18</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="14" sdnum="1033;">14</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="20" sdnum="1033;">20</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="3" sdnum="1033;">3</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="0" sdnum="1033;">0</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="9.5" sdnum="1033;">9.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="6.5" sdnum="1033;">6.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1.5" sdnum="1033;">1.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1" sdnum="1033;">1</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="2.5" sdnum="1033;">2.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1.5" sdnum="1033;">1.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="1" sdnum="1033;">1</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="2.5" sdnum="1033;">2.5</td>
                                    <td style="padding: 0 !important;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" bgcolor="#FFCC00" sdval="2" sdnum="1033;">2</td>
                                </tr>

                            </table>
                        </div>
                    </srd-widget-body>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="close_pop_up();">Cancel</button>
                <!--<button type="button" class="btn btn-primary" ng-click="">Submit</button>-->

            </div>
        </div>
    </div>
</div>
