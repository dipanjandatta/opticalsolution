<html>
<body onload="myFunction()" style="margin:0;">

<div id="loader"></div>

<div style="display:none;" id="myDiv" class="animate-bottom">


    }
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script>
    var myr;

    function myFunction() {
        myVar = setTimeout(showPage, 3000);
    }

    function showPage() {
        document.getElementById("loader").style.display = "none";
        document.getElementById("myDiv").style.display = "block";
    }

</script>

</body>
</html>
<?php $this->load->view('assets/css'); ?>
<?php $this->load->view('assets/js'); ?>
<head>
    <style>
        a:hover {
            background-color: yellow;
        }
    </style>
</head>

<?php $this->load->view('layouts/header'); ?>
<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <?php $this->load->view('layouts/main'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">COUNTER</a></li>
                        </ul>
                        <h4>COUNTER</h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>
            <div class="row">
                <?php
                echo form_open('dashboard/countersave');?>

                <div class="col-lg-6">

                    <?php if($this->uri->segment(3) != ''){?>
                        <div class="form-horizontal">

                            <div class="form-group ">
                                <label class="col-sm-4 control-label">PATIENT ID</label>
                                <div class="col-sm-8">
                                    <input class="form-control" value="<?php echo $patient_id?>" type="text" disabled>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-4 control-label">PATIENT NAME</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="patient_name" value="<?php echo $full_name?>" type="text">

                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-4 control-label">DATE OF BIRTH</label>
                                <div class="col-sm-8">

                                    <input name="dob" class="form-control" type="date" id="CMCompanyName" value="<?php echo $dob?>" placeholder="Company Name" maxlength="30"  />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label" >CURRENT AGE</label>
                                <div class="col-sm-8">
                                    <input name="cur_age" class="form-control" type="number" id="CMCompanyName" value="<?php echo $age?>" placeholder="Current Age" maxlength="30"  />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label" >GENDER</label>
                                <div class="col-sm-8">
                                    <!--                                --><?php // $gender=$gender; ?>
                                    <select name="sex" class="form-control" id="ddlTrackCycle" title="Select Track Cycle" ">
                                    <option value="" selected="selected">GENDER</option>
                                    <option value="M" <?php if($gender == "M"){?> selected="selected" <?php }?>>MALE</option>
                                    <option value="F" <?php if($gender == "F"){?> selected="selected" <?php }?>>FEMALE</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">MOBILE NUMBER</label>
                                <div class="col-sm-8">
                                    <input name="mobile_numb" class="form-control" type="text" value="<?php echo $mobile_no?>" id="emailid" placeholder="MOBILE NUMBER"   />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">ALTERNATIVE MOBILE NUMBER</label>
                                <div class="col-sm-8">
                                    <input name="alt_mobile_numb" class="form-control" type="text" value="<?php echo $mobile_no_alt?>" id="emailid" placeholder="ALTERNATIVE MOBILE NUMBER"   />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">ADDRESS</label>
                                <div class="col-sm-8">
                                    <textarea name="address" class="form-control" style="height: 100PX;" value="<?php echo $address?>" placeholder="CONTACT ADDRESS"  id="CMContactAddress" name="CMContactAddress" ></textarea>
                                </div>
                            </div>

                            <!--<div class="form-group">-->
                            <!--<label class="col-sm-4 control-label" >EXISTING STATUS</label>-->
                            <!--<div class="col-sm-8">-->
                            <!--<select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle" >-->
                            <!--<option value="" selected="selected">EXISTING STATUS</option>-->
                            <!--<option value="YES">YES</option>-->
                            <!--<option value="NO">NO</option>-->
                            <!--</select>-->
                            <!--</div>-->
                            <!--</div>-->

                            <div class="form-group">
                                <label class="col-sm-4 control-label">PATIENT STATUS:</label>
                                <div class="col-sm-8">
                                    <b>INNER PATIENT</b>&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;<input name="patient_status"  type="radio" value="I" />&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;
                                    <b>OUTER PATIENT</b>&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;<input name="patient_status" type="radio" value="O"  />

                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-4 control-label">UPLOAD IMAGE:</label>
                                <div class="col-sm-8">
                                    <input type="file"  name="photo" id="photo" accept="image/*"
                                           style="background: #164D79;color:#fff;line-height: normal;height: auto;" >
                                </div>
                            </div>


                            <div class="form-group " >

                                <div class="col-sm-8">
                                    <table class="tatble_style">
                                        <tr class="row_style">
                                            <td style="border: solid #000000 2px;padding: 0 !important;" colspan="2"><b>LENS MEASUREMENT</b></td>
                                            <!--<td style="border: solid #000000 2px"></td>-->
                                            <td class="column_style">SPH</td>
                                            <td class="column_style">CYL</td>
                                            <td class="column_style">AXIS</td>
                                            <td class="column_style">V/A</td>
                                        </tr>
                                        <tr class="row_style">

                                            <td class="column_style" rowspan="2">RIGHT EYE</td>
                                            <td class="column_style">DISTANCE</td>
                                            <td class="column_style"><input type="text"  name="r_d_sph" class="input_column" disabled/></td>
                                            <td class="column_style"><input type="text" name="r_d_cyl" class="input_column" disabled/></td>
                                            <td class="column_style"><input type="text" name="r_d_axis" class="input_column" disabled/></td>
                                            <td class="column_style"><input type="text" name="r_d_va" class="input_column" disabled/></td>
                                            <!--                                            --><?php //endforeach;?>
                                        </tr>

                                        <tr class="row_style">
                                            <!--<td style="border: solid #000000 2px"></td>-->
                                            <td class="column_style">NEAR</td>
                                            <td class="column_style"><input type="text" name="r_n_sph" class="input_column" disabled/></td>
                                            <td class="column_style"><input type="text" name="r_n_cyl" class="input_column" disabled/></td>
                                            <td class="column_style"><input type="text" name="r_n_axis" class="input_column" disabled/></td>
                                            <td class="column_style"><input type="text" name="r_n_va" class="input_column" disabled/></td>
                                        </tr>

                                        <tr class="row_style">
                                            <td class="column_style" rowspan="2">LEFT EYE</td>
                                            <td class="column_style">DISTANCE</td>
                                            <td class="column_style"><input type="text" name="l_d_sph" class="input_column" disabled/></td>
                                            <td class="column_style"><input type="text" name="l_d_cyl" class="input_column" disabled/></td>
                                            <td class="column_style"><input type="text" name="l_d_axis" class="input_column" disabled/></td>
                                            <td class="column_style"><input type="text" name="l_d_va" class="input_column" disabled/></td>
                                        </tr>
                                        <tr class="row_style">
                                            <td class="column_style">NEAR</td>
                                            <td class="column_style"><input type="text" name="l_n_sph" class="input_column" disabled/></td>
                                            <td class="column_style"><input type="text" name="l_n_cyl" class="input_column" disabled/></td>
                                            <td class="column_style"><input type="text" name="l_n_axis" class="input_column" disabled/></td>
                                            <td class="column_style"><input type="text" name="l_n_va" class="input_column" disabled/></td>
                                        </tr>

                                    </table>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label" >DELIVERY STATUS</label>
                                <div class="col-sm-8">
                                    <select  name="deliv_status" class="form-control"  id="ddlTrackCycle" title="Select Track Cycle" disabled>
                                        <option value="" selected="selected">SELECT STATUS</option>
                                        <option value="NOT DELIVERED">NOT DELIVERED</option>
                                        <option value="PARTIAL DELIVERED">PARTIAL DELIVERED</option>
                                        <option value="DELIVERED">DELIVERED</option>
                                        <option value="CANCELLED">CANCELLED</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label" >BACK OFFICE STATUS</label>
                                <div  class="col-sm-8">
                                    <select name="back_ofc_status" class="form-control"  id="ddlTrackCycle" title="Select Track Cycle" disabled>
                                        <option value="" selected="selected">SELECT STATUS</option>
                                        <option value="IN PROGRESS">IN PROGRESS</option>
                                        <option value="PARTIAL DELIVERED IN COUNTER">PARTIAL DELIVERED IN COUNTER</option>
                                        <option value="DELIVERED IN COUNTER">DELIVERED IN COUNTER</option>
                                        <option value="OUT STATION ORDER">OUT STATION ORDER</option>
                                        <option value="CANCELLED">CANCELLED</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group" >
                                <div class="col-sm-8">
                                    <label style="margin-left: 100px"><a href="" data-toggle="modal" data-target="#exampleModal11" data-whatever="@mdo" ><input type="button" value="Dr Avaibility"/></a></label>
                                </div>
                            </div>

                            <div class="form-group" >
                                <label class="col-sm-4 control-label" >DR APPOINMENT</label>
                                <div class="col-sm-8">
                                    <select name="dr_appointmnt" class="form-control"  id="ddlTrackCycle" title="Select Track Cycle">
                                        <option value="" selected="selected">SELECT DR</option>
                                        <?php foreach($drlist as $val):?>
                                            <option value="<?php echo $val->doctor_id?>"><?php echo $val->full_name?></option>
                                        <?php endforeach?>
                                    </select>
                                </div>
                            </div>

                            <div class="pull-right" >
                                <input type="submit" name="save" class="btn btn-primary" id="btnsave1" value="Update"/>
<!--                                <input type="button" class="btn btn-primary" id="btnCancelComp" value="Reset"/>-->
                                <a style="padding: 8px 10px 12px 10px;margin-right: 4px;background: #337ab7;color: white; text-decoration:none;border-radius: 5px;" href="<?php echo base_url();?>dashboard/counter">Reset</a>
                                <input type="button" class="btn btn-primary" id="btnCancelComp" value="Print Identity Card"/>

                            </div>
                        </div>
                    <?php }
                    else { ?>
                        <div class="form-horizontal">

                            <div class="form-group ">
                                <label class="col-sm-4 control-label">PATIENT ID</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" disabled>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-4 control-label">PATIENT NAME</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="patient_name"  type="text">

                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-4 control-label">DATE OF BIRTH</label>
                                <div class="col-sm-8">

                                    <input name="dob" class="form-control" type="date" id="CMCompanyName"  placeholder="Company Name" maxlength="30"  />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label" >CURRENT AGE</label>
                                <div class="col-sm-8">
                                    <input name="cur_age" class="form-control" type="number" id="CMCompanyName"  placeholder="Current Age" maxlength="30"  />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label" >GENDER</label>
                                <div class="col-sm-8">
                                    <select name="sex" class="form-control" id="ddlTrackCycle" title="Select Track Cycle" ">
                                    <option value="" selected="selected">GENDER</option>
                                    <option value="M">MALE</option>
                                    <option value="F">FEMALE</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">MOBILE NUMBER</label>
                                <div class="col-sm-8">
                                    <input name="mobile_numb" class="form-control" type="text" id="emailid" placeholder="MOBILE NUMBER"   />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">ALTERNATIVE MOBILE NUMBER</label>
                                <div class="col-sm-8">
                                    <input name="alt_mobile_numb" class="form-control" type="text" id="emailid" placeholder="ALTERNATIVE MOBILE NUMBER"   />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">ADDRESS</label>
                                <div class="col-sm-8">
                                    <textarea name="address" class="form-control" style="height: 100PX;" placeholder="CONTACT ADDRESS"  id="CMContactAddress" name="CMContactAddress" ></textarea>
                                </div>
                            </div>

                            <!--<div class="form-group">-->
                            <!--<label class="col-sm-4 control-label" >EXISTING STATUS</label>-->
                            <!--<div class="col-sm-8">-->
                            <!--<select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle" >-->
                            <!--<option value="" selected="selected">EXISTING STATUS</option>-->
                            <!--<option value="YES">YES</option>-->
                            <!--<option value="NO">NO</option>-->
                            <!--</select>-->
                            <!--</div>-->
                            <!--</div>-->

                            <div class="form-group">
                                <label class="col-sm-4 control-label">PATIENT STATUS:</label>
                                <div class="col-sm-8">
                                    <b>INNER PATIENT</b>&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;<input name="patient_status"  type="radio" value="I" />&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;
                                    <b>OUTER PATIENT</b>&NonBreakingSpace;&NonBreakingSpace;&NonBreakingSpace;<input name="patient_status" type="radio" value="O"  />

                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-4 control-label">UPLOAD IMAGE:</label>
                                <div class="col-sm-8">
                                    <input type="file"  name="photo" id="photo" accept="image/*"
                                           style="background: #164D79;color:#fff;line-height: normal;height: auto;" >
                                </div>
                            </div>


                            <div class="form-group " >

                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label" >DELIVERY STATUS</label>
                                <div class="col-sm-8">
                                    <select  name="deliv_status" class="form-control"  id="ddlTrackCycle" title="Select Track Cycle" disabled>
                                        <option value="" selected="selected">SELECT STATUS</option>
                                        <option value="NOT DELIVERED">NOT DELIVERED</option>
                                        <option value="PARTIAL DELIVERED">PARTIAL DELIVERED</option>
                                        <option value="DELIVERED">DELIVERED</option>
                                        <option value="CANCELLED">CANCELLED</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label" >BACK OFFICE STATUS</label>
                                <div  class="col-sm-8">
                                    <select name="back_ofc_status" class="form-control"  id="ddlTrackCycle" title="Select Track Cycle" disabled>
                                        <option value="" selected="selected">SELECT STATUS</option>
                                        <option value="IN PROGRESS">IN PROGRESS</option>
                                        <option value="PARTIAL DELIVERED IN COUNTER">PARTIAL DELIVERED IN COUNTER</option>
                                        <option value="DELIVERED IN COUNTER">DELIVERED IN COUNTER</option>
                                        <option value="OUT STATION ORDER">OUT STATION ORDER</option>
                                        <option value="CANCELLED">CANCELLED</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group" >
                                <div class="col-sm-8">
                                    <label style="margin-left: 100px"><a href="" data-toggle="modal" data-target="#exampleModal11" data-whatever="@mdo" ><input type="button" value="Dr Avaibility"/></a></label>
                                </div>
                            </div>

                            <div class="form-group" >
                                <label class="col-sm-4 control-label" >DR APPOINMENT</label>
                                <div class="col-sm-8">
                                    <select name="dr_appointmnt" class="form-control"  id="ddlTrackCycle" title="Select Track Cycle">
                                        <option value="" selected="selected">SELECT DR</option>
                                        <?php foreach($drlist as $val):?>
                                            <option value="<?php echo $val->doctor_id?>"><?php echo $val->full_name?></option>
                                        <?php endforeach?>
                                    </select>
                                </div>
                            </div>

                            <div class="pull-right" >
                                <input type="submit" name="save" class="btn btn-primary" id="btnsave1" value="Save"/>
                                <a href="<?php echo base_url();?>dashboard/counter"><input type="submit" name="reset" class="btn btn-primary" id="btnCancelComp" value="Reset"/></a>
                                <input type="button" class="btn btn-primary" id="btnCancelComp" value="Print Identity Card" disabled="disabled"/>

                            </div>
                        </div>
                    <?php } ?>

                </div>

                <div class="col-lg-6" >
                    <div class="input-group">
                        <input type="text" class="form-control"  placeholder="Search" >
                        <span class="input-group-addon" style="cursor: pointer">
                            <i class="fa fa-search" title="Search"></i>
                        </span>
                    </div>
                    <div class="medium no-padding" id="gridscroll" style="height: 425px" >
                        <div style="height: 450px; overflow: scroll;">


                            <div class="table-responsive ">
                                <table class="table fontsizestyle" id="mytable"><thead>
                                    <tr>
                                        <th >&nbsp;</th>
                                        <th class="hath" >ID</th>
                                        <th class="hath"  reverse=!reverse">NAME</th>
                                        <th class="hath" >MOBILE</th>
                                    </tr>
                                    </thead>
                                    <?php
                                    foreach($getinfo1 as $val):?>
                                        <tbody>
                                        <tr>
                                            <td class="text-center"><a class="glyphicon glyphicon-edit" href="<?php echo base_url();?>dashboard/counter/<?php echo $val->patient_id?>"></a></td>
                                            <td><?php echo $val->patient_id?></td>
                                            <td><?php echo $val->full_name?></td>
                                            <td><?php echo $val->mobile_no?></td>
                                        </tr>
                                        </tbody>
                                        <?php
                                    endforeach;
                                    ?>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
                <?php form_close();?>

            </div>

            <div class="modal fade" id="exampleModal11" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="width: 133%">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" "><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel1">Dr. Avaibility Chart</h4>
                        </div>
                        <div class="modal-body row">

                            <div class="widget" >
                                <srd-widget-body classes="medium no-padding">
                                    <div class="widget-body medium no-padding" style="height: 200px" ">

                                    <table cellspacing="0" border="0" width="96%" style="margin-left:2%;border-left: 1px solid;">

                                        <tr style="font-weight:bold; font-size:10px">
                                            <th height="20" class="dravitbl" align="center">DR NAME</th>
                                            <th class="dravitbl" align="center">SPECIALIZATION</th>
                                            <th class="dravitbl" align="center">AVAILABLE</th>
                                            <th class="dravitbl" align="center">FEES</th>
                                        </tr>

                                        <tr >

                                            <td height="10" class="dravitbl" align="center"></td>
                                            <td class="dravitbl" align="center"></td>
                                            <td class="dravitbl" align="center"></td>
                                            <td class="dravitbl" align="center"></td>

                                        </tr>
                                    </table>

                                    <!--<table cellspacing="0" border="0">-->
                                    <!--<colgroup width="235"></colgroup>-->
                                    <!--<colgroup width="202"></colgroup>-->
                                    <!--<colgroup width="159"></colgroup>-->
                                    <!--<colgroup width="157"></colgroup>-->
                                    <!--<colgroup width="129"></colgroup>-->
                                    <!--<colgroup width="155"></colgroup>-->
                                    <!--<colgroup width="130"></colgroup>-->
                                    <!--<colgroup width="164"></colgroup>-->
                                    <!--<tr>-->
                                    <!--<td class="dravitbl" height="20" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center">Monday</td>-->
                                    <!--<td class="dravitbl" align="center">Tuesday</td>-->
                                    <!--<td class="dravitbl" align="center">Wednesday</td>-->
                                    <!--<td class="dravitbl" align="center">Thursday</td>-->
                                    <!--<td class="dravitbl" align="center">Friday</td>-->
                                    <!--<td class="dravitbl" align="center">Saturday</td>-->
                                    <!--<td class="dravitbl" align="center">Sunday</td>-->
                                    <!--</tr>-->
                                    <!--<tr>-->
                                    <!--<td class="dravitbl" height="20" align="center">DR ISTA RANJAN SAMANTA(MS)</td>-->
                                    <!--<td class="dravitbl" align="center">10.00 AM-12.30PM</td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center">4.00PM-6.00PM</td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--</tr>-->
                                    <!--<tr>-->
                                    <!--<td class="dravitbl" height="20" align="center">DR HASSAN AHAMED(MS)</td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center">10.00 AM-12.30PM</td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--</tr>-->
                                    <!--<tr>-->
                                    <!--<td class="dravitbl" height="20" align="center">DR KONIKA GHOSH(MS)</td>-->
                                    <!--<td class="dravitbl" align="center">10.00 AM-12.30PM</td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center">10.00 AM-12.30PM</td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center">10.00 AM-12.30PM</td>-->
                                    <!--</tr>-->
                                    <!--<tr>-->
                                    <!--<td class="dravitbl" height="20" align="center">DR SUDIP SAMANTA(MS)</td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center">10.00 AM-12.30PM</td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center">10.00 AM-12.30PM</td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--</tr>-->
                                    <!--<tr>-->
                                    <!--<td class="dravitbl" height="20" align="center">DE DIPANJAN DUTTA(MS)</td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center">10.00 AM-12.30PM</td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--</tr>-->
                                    <!--<tr>-->
                                    <!--<td class="dravitbl" height="20" align="center">DR SRIJAN GHOSH(MS)</td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center"><br></td>-->
                                    <!--<td class="dravitbl" align="center">4.00PM-6.00PM</td>-->
                                    <!--<td class="dravitbl" align="center">10.00 AM-12.30PM</td>-->
                                    <!--</tr>-->
                                    <!--</table>-->

                            </div>
                            </srd-widget-body>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                        <!--<button type="button" class="btn btn-primary" </button>-->

                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php $this->load->view('layouts/footer'); ?>
    </div>
</section>

<input type = "button" value = "Show image for 5 seconds" onclick = "show()"><br><br>
<div id = "myDiv" style="display:none"><img id = "myImage" src = "<?php echo base_url();?>images/load-indicator.gif"></div><br>

<script type = "text/javascript">

    function show() {
        document.getElementById("myDiv").style.display="block";
        setTimeout("hide()", 5000);  // 5 seconds
    }

    function hide() {
        document.getElementById("myDiv").style.display="none";
    }

</script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".update_button_block").click(function() {


            var radioButtons = $("#mytable input:radio[name='id']").val();
            console.log("Value content");
            console.log(radioButtons);
            var dataString = 'radiocheck='+ radioButtons;

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>dashboard/counter",
                data: dataString,
                cache: false,
                success: function(html){
                    alert("sucess")
                }
            });
        });
    });
</script>
