<?php $this->load->view('assets/css'); ?>

<head>
    <style>
        a:hover {
            background-color: yellow;
        }
    </style>
</head>

<?php $this->load->view('layouts/header'); ?>
<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <?php $this->load->view('layouts/main'); ?>
        </div><!-- leftpanel -->
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">DR'S PRESCRIPTION</a></li>
                        </ul>
                        <h4>PRESCRIPTION</h4>
                    </div>
                </div><!-- media -->
            </div>

            <br/>

            <?php
            echo form_open('dashboard/drdetailssave');?>
            <div class="row ">
                <div class="col-lg-6">

                        <div class="form-horizontal">



                            <div class="form-group ">
                                <label class="col-sm-4 control-label">PATIENT ID</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text"  name="patient_id" value="" placeholder="PATIENT ID" maxlength="30" disabled/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-4 control-label">PATIENT NAME</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" id="" name="patient_name" value="" placeholder="PATIENT NAME" maxlength="30" disabled/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-4 control-label" >CURRENT AGE</label>
                                <div class="col-sm-8">
                                    <input  class="form-control" type="text" id="" value="" placeholder="AGE" disabled/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-4 control-label" >GENDER</label>
                                <div class="col-sm-8">
                                    <input  class="form-control" type="text" id="" value="" placeholder="GENDER" disabled/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-4 control-label">MOBILE NUMBER</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" id="" value="" placeholder="MOBILE NUMBER" name="emailid" disabled/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-4 control-label"> ALTERNATIVE MOBILE NUMBER</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" id="" value="" placeholder="ALTERNATIVE MOBILE NUMBER" name="emailid" disabled/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-4 control-label">ADDRESS</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" style="height: 100PX;" placeholder="ADDRESS"  value="" id="CMContactAddress" name="CMContactAddress" disabled></textarea>
                                </div>
                            </div>

                            <div class="form-group ">
                                <!--ol-sm-4 control-label">POWER LENS</label>-->
                                <div class="col-sm-8">
                                    <table class="tatble_style">
                                        <tr class="row_style">
                                            <td style="border: solid #000000 2px;padding: 0 !important;" colspan="2"><b>LENS MEASUREMENT</b></td>
                                            <!--<td style="border: solid #000000 2px"></td>-->
                                            <td class="column_style">SPH</td>
                                            <td class="column_style">CYL</td>
                                            <td class="column_style">AXIS</td>
                                            <td class="column_style">V/A</td>
                                        </tr>
                                        <tr class="row_style">

                                            <td class="column_style" rowspan="2">RIGHT EYE</td>
                                            <td class="column_style">DISTANCE</td>
                                            <td class="column_style"><input type="text"  name="r_d_sph" class="input_column" /></td>
                                            <td class="column_style"><input type="text" name="r_d_cyl" class="input_column" /></td>
                                            <td class="column_style"><input type="text" name="r_d_axis" class="input_column"  /></td>
                                            <td class="column_style"><input type="text" name="r_d_va" class="input_column"  /></td>
<!--                                            --><?php //endforeach;?>
                                        </tr>

                                        <tr class="row_style">
                                            <!--<td style="border: solid #000000 2px"></td>-->
                                            <td class="column_style">NEAR</td>
                                            <td class="column_style"><input type="text" name="r_n_sph" class="input_column" /></td>
                                            <td class="column_style"><input type="text" name="r_n_cyl" class="input_column" /></td>
                                            <td class="column_style"><input type="text" name="r_n_axis" class="input_column" /></td>
                                            <td class="column_style"><input type="text" name="r_n_va" class="input_column" /></td>
                                        </tr>

                                        <tr class="row_style">
                                            <td class="column_style" rowspan="2">LEFT EYE</td>
                                            <td class="column_style">DISTANCE</td>
                                            <td class="column_style"><input type="text" name="l_d_sph" class="input_column" /></td>
                                            <td class="column_style"><input type="text" name="l_d_cyl" class="input_column" /></td>
                                            <td class="column_style"><input type="text" name="l_d_axis" class="input_column" /></td>
                                            <td class="column_style"><input type="text" name="l_d_va" class="input_column" /></td>
                                        </tr>
                                        <tr class="row_style">
                                            <td class="column_style">NEAR</td>
                                            <td class="column_style"><input type="text" name="l_n_sph" class="input_column" /></td>
                                            <td class="column_style"><input type="text" name="l_n_cyl" class="input_column" /></td>
                                            <td class="column_style"><input type="text" name="l_n_axis" class="input_column" /></td>
                                            <td class="column_style"><input type="text" name="l_n_va" class="input_column" /></td>
                                        </tr>

                                    </table>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="col-sm-4 control-label">OTHER INFORMATION</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control"  style="height: 100PX;" placeholder="OTHER INFORMATION"></textarea>
                                </div>
                            </div>

                            <div class="pull-right" >
                                <input type="submit" class="btn btn-primary" id="btnsave1" value="Save" />
                                <input type="button" class="btn btn-primary" id="btnsave1" value="Update" />
                                <input type="button" class="btn btn-primary" id="btnCancelComp" value="Cancel" />
                            </div>

                        </div>
                </div>

                <div class="col-lg-6" >
                        <div class="widget" >
                            <br/>
                                    <div class="input-group">
                                        <input type="text" class="form-control"placeholder="Search" >
                                        <span class="input-group-addon" style="cursor: pointer">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                            <hr/>

                                <div class="widget-body medium no-padding" id="gridscroll" style="height: 425px" >
                                    <div  class="widget-content" style="height: 450px; overflow: scroll;">
                                        <div class="table-responsive ">
                                            <table class="table fontsizestyle"><thead>
                                                <tr>
                                                    <th >&nbsp;</th>
                                                    <th class="hath" >PATIENT ID</th>
                                                    <th class="hath" >NAME</th>
                                                    <th class="hath" >MOBILE</th>
                                                    <th class="hath" >APPOINMENT</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach($allpatientlist as $val):?>
                                                    <tr>
                                                    <td class="text-center"><a class="glyphicon glyphicon-edit" href="<?php echo base_url();?>dashboard/dr_details/<?php echo $val->patient_id?>/<?php echo $val->prescription_id?>"></a></td>
                                                    <td><?php echo $val->patient_id?></td>
                                                    <td><?php echo $val->full_name?></td>
                                                    <td><?php echo $val->mobile_no?></td>
                                                    <td><?php echo $val->doctor_name?></td>

                                                </tr>
                                                <?php endforeach?>

                                                </tbody>
                                            </table>
                                        </div>


                                    </div>
                                </div>
                        </div>
                </div>
            </div>
            <?php form_close();?>
        </div>

        <?php $this->load->view('layouts/footer'); ?>
    </div>
</section>