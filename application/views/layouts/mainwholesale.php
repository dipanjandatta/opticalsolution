<h5 class="leftpanel-title">Navigation</h5>
<ul class="nav nav-pills nav-stacked">
    <li class="parent active"><a href=""><span  class="glyphicon  glyphicon- glyphicon-list">  Menu</span></a>
        <ul class="children">

            <!--ADDED BY MAHIRUDDIN SEIKH FOR OPTICAL SOLUTIONS-->
            <!--            <li><a href="#/main/counter">Counter</a></li>-->
            <li><a href="<?php echo base_url(); ?>wholesale/purchase">Bulk Purchase</a></li>
            <li><a href="<?php echo base_url(); ?>wholesale/sale">Sales Order</a></li>
            <li><a href="<?php echo base_url(); ?>wholesale/stocklist">Stock List</a></li>
            <li><a href="<?php echo base_url(); ?>wholesale/orderlist">Order List</a></li>
            <li><a href="<?php echo base_url(); ?>wholesale/purchase_orderlist">Purchase Order List</a></li>
            <li><a href="<?php echo base_url(); ?>wholesale/receivedlist">Received Order List</a></li>
            <li><a href="<?php echo base_url(); ?>wholesale/deliverylist">Delivery List</a></li>
            <li><a href="<?php echo base_url(); ?>/wholesale/delivery_history">Delivery History</a></li>
            <li><a href="<?php echo base_url(); ?>wholesale/outsource_list">Out Source</a></li>
        </ul>
    </li>
</ul>