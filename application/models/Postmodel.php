
<?php
/**
 * @author Subhajit
 * @copyright 2016
 */
class Postmodel extends CI_Model
{

    function Model()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_patient_lens_info($data){

        $p0=$data['prescription_id'];
//        print_r($p0);exit();

        $this->db->query("SET @p0 := '$p0';");

        $success = $this->db->query("CALL get_patient_lens_info(@p0);");

        $result=$success->result();
//        print_r($success);exit();

        return $result;

    }

    function save_appointment_info($data){
        $p0=$data['appointment_id'];
        $p1=$data['doctor_in'];
        $p2=$data['patient_id'];
        $p3=$data['created_by'];
        $p4=$data['appointment_on'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $this->db->query("SET @p4 := '$p4';");

        $success = $this->db->query("CALL save_appointment_info(@p0,@p1,@p2,@p3,@p4);");

//        $result = $success->result();

//        print_r($result);exit();

        return 0;
    }

    function save_update_prescription($data){

//        print_r($data);exit();
        $p0 = $data['prescription_id'];
        $p1 = $data['patient_id'];
        $p2 = $data['prescribed_by'];
        $p3 = $data['other_info'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");

        $success = $this->db->query("CALL save_update_prescription(@p0,@p1,@p2,@p3);");

        $out_param_query = $this->db->query('SELECT @p0 AS  `prescription_id_in`');
        $result = $out_param_query->result();

//        print_r($result);exit();
        return $result;

    }

    function save_update_patient_lense_info1($data){
//        print_r($data);exit();
        $p0 = $data['prescription_lens_id'];
        $p1 = $data['prescription_id'];
        $p2 = $data['eye_type'];
        $p3 = $data['measurement_type'];
        $p4 = $data['r_d_sph'];
        $p5 = $data['r_d_cyl'];
        $p6 = $data['r_d_axis'];
        $p7 = $data['r_d_va'];
        $p8 = $data['created_by'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $this->db->query("SET @p4 := '$p4';");
        $this->db->query("SET @p5 := '$p5';");
        $this->db->query("SET @p6 := '$p6';");
        $this->db->query("SET @p7 := '$p7';");
        $this->db->query("SET @p8 := '$p8';");

//        print_r($this->db->query);exit();

        $success = $this->db->query("CALL save_update_patient_lense_info(@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8);");

        $out_param_query = $this->db->query('SELECT @p0 AS  `prescribed_lense_id_in`');

        $result = $out_param_query->result();

//        print_r($result);exit();

        return $result;
    }

    function save_update_patient_lense_info2($data){
//        print_r($data);exit();

        $p0 = $data['prescription_lens_id'];
        $p1 = $data['prescription_id'];
        $p2 = $data['eye_type'];
        $p3 = $data['measurement_type'];
        $p4 = $data['r_n_sph'];
        $p5 = $data['r_n_cyl'];
        $p6 = $data['r_n_axis'];
        $p7 = $data['r_n_va'];
        $p8 = $data['created_by'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $this->db->query("SET @p4 := '$p4';");
        $this->db->query("SET @p5 := '$p5';");
        $this->db->query("SET @p6 := '$p6';");
        $this->db->query("SET @p7 := '$p7';");
        $this->db->query("SET @p8 := '$p8';");

//        print_r($this->db->query);exit();

        $success = $this->db->query("CALL save_update_patient_lense_info(@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8);");

        $out_param_query = $this->db->query('SELECT @p0 AS  `prescribed_lense_id_in`');

        $result = $out_param_query->result();

//        print_r($result);exit();

        return $result;
    }

    function save_update_patient_lense_info3($data){
//        print_r($data);exit();

        $p0 = $data['prescription_lens_id'];
        $p1 = $data['prescription_id'];
        $p2 = $data['eye_type'];
        $p3 = $data['measurement_type'];
        $p4 = $data['l_d_sph'];
        $p5 = $data['l_d_cyl'];
        $p6 = $data['l_d_axis'];
        $p7 = $data['l_d_va'];
        $p8 = $data['created_by'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $this->db->query("SET @p4 := '$p4';");
        $this->db->query("SET @p5 := '$p5';");
        $this->db->query("SET @p6 := '$p6';");
        $this->db->query("SET @p7 := '$p7';");
        $this->db->query("SET @p8 := '$p8';");

//        print_r($this->db->query);exit();

        $success = $this->db->query("CALL save_update_patient_lense_info(@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8);");

        $out_param_query = $this->db->query('SELECT @p0 AS  `prescribed_lense_id_in`');

        $result = $out_param_query->result();

//        print_r($result);exit();

        return $result;
    }

    function save_update_patient_lense_info4($data){
//        print_r($data);exit();

        $p0 = $data['prescription_lens_id'];
        $p1 = $data['prescription_id'];
        $p2 = $data['eye_type'];
        $p3 = $data['measurement_type'];
        $p4 = $data['l_n_sph'];
        $p5 = $data['l_n_cyl'];
        $p6 = $data['l_n_axis'];
        $p7 = $data['l_n_va'];
        $p8 = $data['created_by'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $this->db->query("SET @p4 := '$p4';");
        $this->db->query("SET @p5 := '$p5';");
        $this->db->query("SET @p6 := '$p6';");
        $this->db->query("SET @p7 := '$p7';");
        $this->db->query("SET @p8 := '$p8';");

//        print_r($this->db->query);exit();

        $success = $this->db->query("CALL save_update_patient_lense_info(@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8);");

        $out_param_query = $this->db->query('SELECT @p0 AS  `prescribed_lense_id_in`');

        $result = $out_param_query->result();

//        print_r($result);exit();

        return $result;
    }

    function save_update_patient_info($data)
    {
        $p0 = $data['patient_id'];
        $p1 = $data['patient_name'];
        $p2 = $data['dob'];
        $p3 = $data['cur_age'];
        $p4 = $data['sex'];
        $p5 = $data['mobile_numb'];
        $p6 = $data['alt_mobile_numb'];
        $p7 = $data['address'];
        $p8 = $data['patient_status'];
        $p9 = $data['deliv_status'];
        $p10 = $data['back_ofc_status'];
        $p11 = $data['pic_url'];
        $p12 = $data['created_by'];
        $p13 = $data['doctor_in'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $this->db->query("SET @p4 := '$p4';");
        $this->db->query("SET @p5 := '$p5';");
        $this->db->query("SET @p6 := '$p6';");
        $this->db->query("SET @p7 := '$p7';");
        $this->db->query("SET @p8 := '$p8';");
        $this->db->query("SET @p9 := '$p9';");
        $this->db->query("SET @p10 := '$p10';");
        $this->db->query("SET @p11 := '$p11';");
        $this->db->query("SET @p12 := '$p12';");
        $this->db->query("SET @p13 := '$p13';");
//        print_r($this->db->query);exit();


        $success = $this->db->query("CALL save_update_patient_info(@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8,@p9,@p10,@p11,@p12,@p13);");

        $out_param_query = $this->db->query('SELECT @p0 AS  `patient_id_in`');

        $result = $out_param_query->result();

        return $result;
    }
    function get_all_doctor_name()
    {
        $success = $this->db->query("CALL get_all_doctor_name();");
        $result=$success->result();
//        print_r($result);exit();
        return $result;
    }

    //specific patient list
    function get_patient_details($data)
    {
//        print_r($data);exit();
        $p0 = $data['patient_id'];
        $p1 = $data['off_set'];
        $p2 = $data['limit'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $success = $this->db->query("CALL get_patient_details(@p0,@p1,@p2,@p3);");

//        $out_param_query = $this->db->query('select @p3 AS  `totalRowCnt`;');
        $result=$success->result();
//        print_r($result);exit();
        return $result;
    }

    //all patient list
    function get_patient_details1(){
        $p0 ="";
        $p1 =0;
        $p2 = 20;

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $success = $this->db->query("CALL get_patient_details(@p0,@p1,@p2,@p3);");

//        $out_param_query = $this->db->query('select @p3 AS  `totalRowCnt`;');
        $result=$success->result();
//        print_r($result);exit();
        return $result;
    }

    //For Dr Details page

    function get_prescription_info($data){
//        print_r($data);exit();

        $p0 = $data['patient_id'];
        $p1 = $data['prescription_id'];
        $p2 = $data['offset'];
        $p3 = $data['limit'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $success = $this->db->query("CALL get_prescription_info(@p0,@p1,@p2,@p3,@p4);");

        $result=$success->result();
//        print_r($result);exit();
        return $result;
    }

    function get_prescription_info1(){
        $p0 ="";
        $p1 = "";
        $p2 = 0;
        $p3 = 100;

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $success = $this->db->query("CALL get_prescription_info(@p0,@p1,@p2,@p3,@p4);");

        $result=$success->result();
//        print_r($result);exit();
        return $result;
    }

    function get_country_list(){

        $success = $this->db->query("CALL get_country_list();");
        print_r($success->result());exit();

    }


    //Wholesale part
    function save_update_sales_order($data){

        $p0=$data['order_id'];
        $p1=$data['bill_no'];
        $p2=$data['party_name'];
        $p3=$data['date'];
        $p4=$data['order_status'];

        $this->db->query("SET @p0 := '$p0';");

        $this->db->query("CALL save_update_sales_order(@p0 ,'$p1','$p2','$p3','$p4');");


        $out_param_query = $this->db->query('SELECT @p0 AS  `order_id`');
        $result = $out_param_query->result();

//        print_r($result);exit();
        return $result;
    }

    function save_update_sales_order_item($data){
//        print_r("Hello");exit();
        $p0=$data['item_id'];
        $p1=$data['order_id'];
        $p2=$data['prod_type'];
        $p3=$data['prod_name'];
        $p4=$data['comp_name'];
        $p5=$data['add_type'];
        $p6=$data['specification'];
        $p7=$data['sph'];
        $p8=$data['cyl'];
        $p9=$data['axis'];
        $p10=$data['addition'];
        $p11=$data['dia'];
        $p12=$data['base'];
        $p13=$data['side'];
        $p14=$data['quant'];
        $p15=$data['remarks'];
        $p16=$data['order_status'];
        $p17=$data['frame'];
        $p18=$data['lens'];
        $p19=$data['pending_quant'];
        $p20=$data['supplier_name'];

        $this->db->query("SET @p0 := '$p0';");

        $this->db->query("CALL save_update_sales_order_item(@p0,'$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9','$p10','$p11','$p12','$p13','$p14','$p15','$p16','$p17','$p18','$p19','$p20');");

        $out_param_query = $this->db->query('SELECT @p0 AS  `item_id`');
        $result = $out_param_query->result();

//        print_r($result);exit();
        return $result;

    }

    function get_sales_order_item($data){

        $p0=$data['sales_order_id'];
        $p1=$data['order_status'];
        $p2=$data['offset'];
            $p3=$data['limit'];

        $success=$this->db->query("CALL get_sales_order_item('$p0','$p1','$p2','$p3','@p4');");

        $result=$success->result();
//        print_r($result);exit();
        return $result;


    }

    function get_sales_order_item1($data){

//        print_r($data);exit();
        $p0=$data['sales_order_id'];
        $p1=$data['order_status'];
        $p2=$data['offset'];
        $p3=$data['limit'];

        $success=$this->db->query("CALL get_sales_order_item('$p0','$p1','$p2','$p3','@p4');");

        $result=$success->result();
//        print_r($result);exit();
        return $result;

    }


    function get_sales_order_item_payment($data){
        $query = $this->db->query("select o.id, o.sales_order_id,o.bill_no, o.party_name,i.item_id,o.order_date,i.product_type,i.product_name,i.company_name,i.specification,i.sph,i.cyl,i.axis,i.addition,i.diameter,i.base,i.side,i.quantity,i.remarks,i.status from sales_order o left join sales_order_items i on o.id = i.sales_order_id where i.item_id in($data)");
        $result = $query->result();
        return $result;
    }

    function add_to_stock($data){
        $p0=$data['prod_name'];
        $p1=$data['sph'];
        $p2=$data['cyl'];
        $p3=$data['axis'];
        $p4=$data['addition'];
        $p5=$data['quant'];

        $success=$this->db->query("CALL add_to_stock('$p0','$p1','$p2','$p3','$p4','$p5');");
        return 0;
    }

    function remove_from_stock($data){

//        print_r($data);exit();

        $p0=$data['prod_name'];
        $p1=$data['sph'];
        $p2=$data['cyl'];
        $p3=$data['axis'];
        $p4=$data['addition'];
        $p5=$data['quant'];

        $success=$this->db->query("CALL remove_from_stock('$p0','$p1','$p2','$p3','$p4','$p5');");
        return 0;
    }

    function get_stock_list(){

        $p0=0;
        $p1=10000;

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $success = $this->db->query("CALL get_stock_list(@p0,@p1,@p2);");

        $result=$success->result();
//        print_r($result);exit();
        return $result;
    }

function update_stat($id,$stat){
    $query = $this->db->query("update sales_order_items set status = ? where item_id = ? ", array('status'=>$stat,'item_id'=>$id));
    return true;
}

    function save_payment($data){

        $p0=$data['paymnt_wholesale_id'];
        $p1=$data['prty_name'];
        $p2=$data['item_id'];
        $p3=$data['amount'];
        $p4=$data['ser_tax'];
        $p5=$data['tot_amnt'];
        $p6=$data['payed'];
        $p7=$data['due_amount'];
        $p8=$data['updated_by'];

        $this->db->query("SET @p0 := '$p0';");

        $success=$this->db->query("CALL save_update_payment_wholesale(@p0,'$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8');");
        $out_param_query = $this->db->query('select @p0 AS `paymnt_id`');

        $result = $out_param_query->result();

        return true;
    }

    function get_due_amnt($prty_nme){

        $query = $this->db->query("select due_amount from payment_wholesale where party_name=?",array('party_name'=>$prty_nme));
        $result = $query->result();
        return $result;
    }

    function login_cred($usr,$pass){

        $query = $this->db->query("select user_id,password,user_type from users where user_id=? && password=?",array('username'=>$usr,'pasword'=>$pass));
        $result = $query->result();
        return $result;
    }

    function check_name($prtyname){
        $query = $this->db->query("SELECT a.payment_wholesale_id,a.total_amount,a.paid_amount,b.due_amount,b.name,b.address FROM  opticaldb.party_details b LEFT JOIN opticaldb.payment_wholesale a ON a.party_name=b.name where b.name=?",array('party_name'=>$prtyname));
        $result = $query->result();
        return $result;
    }

    function update_prty_due_blnce($prtyname,$updatedblnce){
        $query = $this->db->query("update party_details set due_amount = ? where name = ? ", array('due_amnt'=>$updatedblnce,'prty_name'=>$prtyname));
        return true;
    }
}
